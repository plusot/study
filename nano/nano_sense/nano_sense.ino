/* 
  Modify U=Object(o.useState)(50) to U=Object(o.useState)(1440) in main.35ae02cb.chunk.js of 
  Arduino IDE.app/Contents/Resources/app/lib/backend/resources/arduino-serial-plotter-webapp/static/js
  to get large buffer on serial plotter.
*/

#include <ArduinoBLE.h>
/* For APDS9960 Gesture, light, and proximity sensor */
#include <Arduino_APDS9960.h>
/* For HTS221 Temperature and humidity sensor */
#include <Arduino_HTS221.h>
/* For LPS22HB barometric barometricPressure sensor */
#include <Arduino_LPS22HB.h>


#define INTERVAL 1000
#define MAX_INDEX 1440

REDIRECT_STDOUT_TO(Serial)

uint8_t data[] = { 
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C //0x0D, 0x0E, 0x0F, 
  //0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F
};

struct color {
  int red;
  int blue;
  int green;
  int ambient;
  unsigned long time;
};
color colors[MAX_INDEX];
int colorIndex = -1;
bool indexLooped = false;
bool autoPrint = true;
unsigned long timeResolution = 1000L;

char buffer[180] = "";

const uint16_t manufacturer = 0xBEEB;
const float CALIBRATION_FACTOR = -7.0; // Temperature calibration factor (Celsius)

int proximity = 0;
int temperature = 0;
unsigned int humidity = 0;
unsigned long pressure = 0;
unsigned long currentTime = 0L;

BLEService nanoService("19D0EA00-E8F2-537E-4F6C-D104768A1214");
BLECharacteristic colorCharacteristic("19D0EA01-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify /*| BLEBroadcast*/, 12);   
BLECharacteristic weatherCharacteristic("19D0EA02-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify /*| BLEBroadcast*/, 8);               // 1234,5678,
BLEDescriptor colorLabelDescriptor("2901", "16-bit ints: r, g, b, a");

void setup() {
  Serial.begin(115200); // Initialize serial communication
  //while (!Serial);
  // put your setup code here, to run once:
  if (!APDS.begin()) { // Initialize APDS9960 sensor
    Serial.println("Failed to initialize color, gesture sensor!");
    while (1);
  }
  /* As per Arduino_APDS9960.h, 0=100%, 1=150%, 2=200%, 3=300%. Obviously more
   * boost results in more power consumption. */
  APDS.setLEDBoost(0);

  if (!HTS.begin()) { // Initialize HTS221 temperature, humidity sensor
    Serial.println("Failed to initialize humidity temperature sensor!");
    while (1);
  }
  
  if (!BARO.begin()) { // Initialize LPS22HB sensor
    Serial.println("Failed to initialize pressure sensor!");
    while (1);
  }
  // Avoid bad readings to start bug
  // https://forum.arduino.cc/index.php?topic=660360.0
  BARO.readPressure();
  delay(1000);

  if (!BLE.begin()) { // Initialize NINA B306 BLE
    Serial.println("Faild to initialize BLE!");
    while (1);
  }
  
  BLE.setLocalName("NanoSense");
  BLE.setDeviceName("Nano33Sense");
   
  nanoService.addCharacteristic(colorCharacteristic);
  colorCharacteristic.addDescriptor(colorLabelDescriptor);
  nanoService.addCharacteristic(weatherCharacteristic);
  BLE.addService(nanoService);
  
  //Serial.println("blue,red,green,ambient,proximity,˚C,mBar,%humidity");
  colorCharacteristic.writeValue(data, 10);
  BLE.setAdvertisedServiceUuid(NULL); // To allow manufacturer data to be written!!!!
  BLE.setManufacturerData(manufacturer, data, 10);
  BLE.advertise(); 
}

void getWeather() {
  

  int t = (int) (HTS.readTemperature() * 100) + (int) (CALIBRATION_FACTOR * 100);
  unsigned int h = (unsigned int) (HTS.readHumidity() * 100);
  unsigned long p = (unsigned long) (BARO.readPressure() * 1000L);
  if ((temperature != t) ||
    (humidity != h) ||
    (pressure != p)) {
    temperature = t;
    pressure = p;
    humidity = h;
    memcpy(&data[0], &temperature, 2); 
    memcpy(&data[2], &humidity, 2); 
    memcpy(&data[4], &pressure, 4); 
    
    weatherCharacteristic.writeValue(data, 8); // Update characteristic

    // Serial.print("Temperature: ");
    // Serial.println(temperature);
    
    // Serial.print("Humidity: ");
    // Serial.println(humidity);
    // Serial.print("Pressure: ");
    // Serial.println(pressure);
    
  }
}
 
void showColor(int hasCentral) {
  //irradiance = (float) ambient / 2360 W/m2
  static long prevMillis = 0;
  long now = millis();
  if (now - prevMillis > INTERVAL) {
    int i = 0, red, green, blue, ambient;
    while (!APDS.colorAvailable() && i++ < 100) {
      delay(50);
    }
    APDS.readColor(red, green, blue, ambient);
    if (APDS.proximityAvailable()) proximity = APDS.readProximity();
    
    memcpy(&data[0], &red, 2); 
    memcpy(&data[2], &green, 2); 
    memcpy(&data[4], &blue, 2); 
    memcpy(&data[6], &ambient, 2); 
    memcpy(&data[8], &proximity, 2); 
    if (hasCentral) {
      colorCharacteristic.writeValue(data, 10);
    } else {
      BLE.stopAdvertise();
//      delay(100);
      BLE.setManufacturerData(manufacturer, data, 10);
//      delay(100);
      BLE.advertise();
    }
    
    //Serial.println("blue,red,green,ambient,proximity,˚C,mBar,%humidity");
    currentTime = millis(); 
    //sprintf(buffer, "blue:%i,red:%i,green:%i,ambient:%i,proximity:%i,˚C:%i,mBar:%i,humidity:%i,time:%i", blue, red, green, ambient, proximity, temperature / 100, pressure / 100, humidity/ 100, currentTime/1000);
    if (autoPrint) {
      sprintf(buffer, "blue:%i,red:%i,green:%i,ambient:%i,time:%i", blue, red, green, ambient, currentTime/1000);
      Serial.println(buffer);
    }
    
    if (colorIndex < 0 || abs(currentTime - colors[colorIndex].time) > timeResolution) {
      colorIndex++;
      if (colorIndex >= MAX_INDEX) {
        colorIndex %= MAX_INDEX;
        indexLooped = true;
        if (timeResolution <= 5000) {
          timeResolution = 10000;
        } else /*if (timeResolution < 30000) {
          timeResolution += 10000;
        } else */ {
          timeResolution = 60000;
        } 
      }
      colors[colorIndex].red = red;
      colors[colorIndex].blue = blue;
      colors[colorIndex].green = green;
      colors[colorIndex].ambient = ambient;
      colors[colorIndex].time = currentTime;
    }
    
    prevMillis = now;
    toggleLed();
  }
}

void toggleLed() {
  static int ledToggle = 0;
  switch (ledToggle) {
    case 0:
      digitalWrite(LEDG, LOW);
      break;
    case 1:
      digitalWrite(LEDG, HIGH);
      break;
  }
  ledToggle++;
  ledToggle %= 2;
}

void checkInput() {
  if (Serial.available()) {
    String read = Serial.readString();
    // Serial.print("read: ");
    // Serial.print(read);
    if (read.startsWith("show")) {
      reprint();
    }
    if (read.startsWith("auto")) {
      autoPrint = true;
    }
    if (read.startsWith("manual")) {
      autoPrint = false;
    }
    if (read.startsWith("seconds")) {
      timeResolution = 1000L;
      Serial.println("timeResolution = seconds");
    }
    if (read.startsWith("minutes")) {
      timeResolution = 60000L;
      Serial.println("timeResolution = minutes");   
    }
    if (read.startsWith("resolution")) {
      Serial.print("timeResolution:");   
      Serial.println(timeResolution);   
    }
  }
}

void reprint() {
  if (colorIndex < 0) return;
  if (indexLooped) {
    for (int i=1; i <= MAX_INDEX; i++) {
      int index = (colorIndex + i) % MAX_INDEX;
      sprintf(buffer, "blue:%i,red:%i,green:%i,ambient:%i,time:%i,index:%i", 
        colors[index].blue, 
        colors[index].red, 
        colors[index].green, 
        colors[index].ambient, /*, proximity, temperature / 100, pressure / 100, humidity/ 100, */
        colors[index].time/1000,
        index
      );
      Serial.println(buffer);
    }
  } else {
    for (int i=0; i <= colorIndex; i++) {
      sprintf(buffer, "blue:%i,red:%i,green:%i,ambient:%i,time:%i,index:%i", 
        colors[i].blue, 
        colors[i].red, 
        colors[i].green, 
        colors[i].ambient, /*, proximity, temperature / 100, pressure / 100, humidity/ 100, */
        colors[i].time/1000,
        i
      );
      Serial.println(buffer);
    }
  } 
}

void loop() {
  BLEDevice central = BLE.central();
  if (central) {
    while  (central.connected()) {
      showColor(true);
      getWeather();
    }
  } else {
      showColor(false);
      getWeather();
  }
  checkInput();
}

