/*
  Description: Transmits Arduino Nano 33 BLE Sense sensor readings over BLE,
               including temperature, humidity, barometric pressure, and color,
               using the Bluetooth Generic Attribute Profile (GATT) Specification
  Author: Gary Stafford
  Reference: Source code adapted from `Nano 33 BLE Sense Getting Started`
  Adapted from Arduino BatteryMonitor example by Peter Milne
*/

/*
  Generic Attribute Profile (GATT) Specifications
  GATT Service: Environmental Sensing Service (ESS) Characteristics
  Temperature
  sint16 (decimalexponent -2)
  Unit is in degrees Celsius with a resolution of 0.01 degrees Celsius
  https://www.bluetooth.com/xml-viewer/?src=https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.temperature.xml
  Humidity
  uint16 (decimalexponent -2)
  Unit is in percent with a resolution of 0.01 percent
  https://www.bluetooth.com/xml-viewer/?src=https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.humidity.xml
  Barometric Pressure
  uint32 (decimalexponent -1)
  Unit is in pascals with a resolution of 0.1 Pa
  https://www.bluetooth.com/xml-viewer/?src=https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.pressure.xml
*/
#include <stdio.h>
#include <ArduinoBLE.h>
/* For MP34DT05 microphone */
#include <PDM.h>
/* For LSM9DS1 9-axis IMU sensor */
#include <Arduino_LSM9DS1.h>
/* For HTS221 Temperature and humidity sensor */
#include <Arduino_HTS221.h>
/* For LPS22HB barometric barometricPressure sensor */
#include <Arduino_LPS22HB.h>
/* For APDS9960 Gesture, light, and proximity sensor */
#include <Arduino_APDS9960.h>

#define BLE_BUFFER_SIZE 20

const int UPDATE_FREQUENCY = 1000;     // Update frequency in ms
const float CALIBRATION_FACTOR = -4.0; // Temperature calibration factor (Celsius)

int temperature = 0;
unsigned int humidity = 0;
unsigned int pressure = 0;
int proximity, gesture;
int red, green, blue, alpha;
char text[120];
long previousMillis = 0; // last time readings were checked, in ms

BLEService environmentService("181A"); // Standard Environmental Sensing service
BLEIntCharacteristic tempCharacteristic("2A6E", BLERead | BLENotify); 
BLEUnsignedIntCharacteristic humidCharacteristic("2A6F", BLERead | BLENotify);
BLEUnsignedIntCharacteristic pressureCharacteristic("2A6D", BLERead | BLENotify); 
BLEDescriptor colorLabelDescriptor("2901", "16-bit ints: r, g, b, a");
BLEService colorService("19D00000-E8F2-537E-4F6C-D104768A1214");
BLECharacteristic colorCharacteristic("19D00001-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify /*| BLEBroadcast*/, 12);               // 1234,5678,

BLEService soundService("19C00000-E8F2-537E-4F6C-D104768A1214");
BLECharacteristic soundCharacteristic("19C00001-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify, 6);
BLEService imuService("19A00000-E8F2-537E-4F6C-D104768A1214");
BLECharacteristic imuCharacteristic("19A00001-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify, 18);
BLEService ledService("19B10000-E8F2-537E-4F6C-D104768A1214"); 
BLEByteCharacteristic ledSwitchCharacteristic("19B10001-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite | BLENotify);

uint8_t bleBuffer[BLE_BUFFER_SIZE];
short micSampleBuffer[256];
volatile int micSamplesRead;
volatile float micLevel, sampleRate = 0, micMax = 0;
unsigned long micMillis = 0;

void setup() {
  Serial.begin(9600); // Initialize serial communication
  // while (!Serial); // only when connected to laptop

  pinMode(LEDR, OUTPUT);  
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  if (!HTS.begin()) { // Initialize HTS221 temperature, humidity sensor
    Serial.println("Failed to initialize humidity temperature sensor!");
    while (1);
  }

  if (!BARO.begin()) { // Initialize LPS22HB sensor
    Serial.println("Failed to initialize pressure sensor!");
    while (1);
  }

  // Avoid bad readings to start bug
  // https://forum.arduino.cc/index.php?topic=660360.0
  BARO.readPressure();
  delay(1000);

  APDS.setGestureSensitivity(50);
  if (!APDS.begin()) { // Initialize APDS9960 sensor
    Serial.println("Failed to initialize color, gesture sensor!");
    while (1);
  }
  /* As per Arduino_APDS9960.h, 0=100%, 1=150%, 2=200%, 3=300%. Obviously more
   * boost results in more power consumption. */
  APDS.setLEDBoost(0);

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    /* Hacky way of stopping program executation in event of failure. */
    while(1);
  }

  pinMode(LED_BUILTIN, OUTPUT); // Initialize the built-in LED pin

  if (!BLE.begin()) { // Initialize NINA B306 BLE
    Serial.println("Faild to initialize BLE!");
    while (1);
  }

  PDM.onReceive(onPDMdata);
  // optionally set the gain, defaults to 20
  // PDM.setGain(30);

  if (!PDM.begin(1, 16000)) {
    Serial.println("Failed to start PDM!");
    while (1);
  }

  BLE.setLocalName("Senses");    
  BLE.setAdvertisedService(environmentService); 
  environmentService.addCharacteristic(tempCharacteristic);
  environmentService.addCharacteristic(humidCharacteristic);    
  environmentService.addCharacteristic(pressureCharacteristic); 
  
  colorService.addCharacteristic(colorCharacteristic);   
  colorCharacteristic.addDescriptor(colorLabelDescriptor); // Add color characteristic descriptor

  imuService.addCharacteristic(imuCharacteristic);      
  ledService.addCharacteristic(ledSwitchCharacteristic);
  soundService.addCharacteristic(soundCharacteristic);

  BLE.addService(environmentService); 
  BLE.addService(colorService);
  BLE.addService(imuService);
  BLE.addService(ledService);
  BLE.addService(soundService);

  tempCharacteristic.setValue(0);     
  humidCharacteristic.setValue(0);    
  pressureCharacteristic.setValue(0); 
  memset(bleBuffer, 0, BLE_BUFFER_SIZE);
  colorCharacteristic.writeValue(bleBuffer, 12);
  ledSwitchCharacteristic.setValue(0);
  soundCharacteristic.writeValue(bleBuffer, 6);

  digitalWrite(LEDB, HIGH);

  BLE.advertise(); // Start advertising
  Serial.print("Peripheral device MAC: ");
  Serial.println(BLE.address());
  Serial.println("Waiting for connections...");
}

void loop() {
  
  BLEDevice central = BLE.central(); // Wait for a BLE central to connect

  // If central is connected to peripheral
  if (central) {
    Serial.print("Connected to central MAC: ");
    Serial.println(central.address()); // Central's BT address:

    digitalWrite(LED_BUILTIN, HIGH); // Turn on the LED to indicate the connection

    while (central.connected()) {
      long currentMillis = millis();
      // After UPDATE_FREQUENCY ms have passed, check temperature & humidity
      if (currentMillis - previousMillis >= UPDATE_FREQUENCY) {
        previousMillis = currentMillis;
        getWeather(CALIBRATION_FACTOR);
        getColor();
        getImu();
        getSound();
      }
      if (ledSwitchCharacteristic.written()) {
        if (ledSwitchCharacteristic.value()) {   // any value other than 0
          Serial.println("LED on");
          digitalWrite(LEDB, LOW);         // will turn the LED on
        } else {                              // a 0 value
          Serial.println(F("LED off"));
          digitalWrite(LEDB, HIGH);          // will turn the LED off
        }
      }
      showLeds();
    }

    digitalWrite(LED_BUILTIN, LOW); // When the central disconnects, turn off the LED
    Serial.print("Disconnected from central MAC: ");
    Serial.println(central.address());
    
  } else {
    getMicrophoneData();
    showLeds();
  }
}

void showLeds() {
  static unsigned long milliCount = 0;
  static int ledToggle = 0;

  unsigned long mil = millis();

  if (mil - milliCount >= 1000) {
    milliCount = mil;
    switch (ledToggle) {
      case 0:
        digitalWrite(LEDR, HIGH); // For RGB led HIGH = off and LOW = on!!!
        digitalWrite(LEDG, LOW);
        break;
      case 1:
        digitalWrite(LEDG, HIGH);
        digitalWrite(LEDR, LOW);
        break;
    }
    ledToggle++;
    ledToggle %= 2;
  }
}

void getSound() {
  int value =  (int)sqrt(micLevel);
  memcpy(&bleBuffer[0],&value, 2);
  value =  (int)sqrt(micMax);
  memcpy(&bleBuffer[2],&value, 2);
  value =  (int)sampleRate;
  memcpy(&bleBuffer[4],&value, 2);
  soundCharacteristic.writeValue(bleBuffer, 6);
  //Serial.printf("Mic: %f, %f, %f Hz", micLevel, micMax, sampleRate);
  Serial.println();
  micMax = 0;
}

void getMicrophoneData() {
  if (micSamplesRead) {
    // print samples to the serial monitor or plotter
    for (int i = 0; i < micSamplesRead; i++) {
      Serial.print(micSampleBuffer[i]);
      Serial.print(",");
      Serial.print(sqrt(micLevel));
      Serial.print(",");
      Serial.print(sqrt(micMax));
      Serial.print(",");
      Serial.println(sampleRate / 1000);
    }
    // clear the read count
    micSamplesRead = 0;
  }
}

void getWeather(float calibration) {
  int t = (int) (HTS.readTemperature() * 100) + (int) (calibration * 100);
  unsigned int h = (unsigned int) (HTS.readHumidity() * 100);
  unsigned int p = (unsigned int) (BARO.readPressure() * 1000 * 10);
  if ((temperature != t) ||
    (humidity != h) ||
    (pressure != p)) {
    temperature = t;
    pressure = p;
    humidity = h;
    Serial.print("Temperature: ");
    Serial.println(temperature);
    tempCharacteristic.writeValue(temperature); // Update characteristic

    Serial.print("Humidity: ");
    Serial.println(humidity);
    humidCharacteristic.writeValue(humidity);
  
    Serial.print("Pressure: ");
    Serial.println(pressure);
    pressureCharacteristic.writeValue(pressure);
  }
}

void getColor() {
  // check if a color reading is available
  int i = 0;
  while (!APDS.colorAvailable() && i++ < 100) {
    delay(50);
  }
  
//  int tmp_r, tmp_g, tmp_b, tmp_a;
//  APDS.readColor(tmp_r, tmp_g, tmp_b, tmp_a);
//    red = tmp_r;
//    green = tmp_g;
//    blue = tmp_b;
//    alpha = tmp_a;

  // Get color as (4) unsigned 16-bit ints
  APDS.readColor(red, green, blue, alpha);
  if (APDS.proximityAvailable()) proximity = APDS.readProximity();
  //if (APDS.gestureAvailable()) gesture = APDS.readGesture();
  String stringColor = "";
  stringColor += red;
  stringColor += ",";
  stringColor += green;
  stringColor += ",";
  stringColor += blue;
  stringColor += ",";
  stringColor += alpha;
  stringColor += ",";
  stringColor += proximity;
  stringColor += ",";
  stringColor += gesture;

  memcpy(&bleBuffer[0], &red, 2); 
  memcpy(&bleBuffer[2], &green, 2); 
  memcpy(&bleBuffer[4], &blue, 2); 
  memcpy(&bleBuffer[6], &alpha, 2); 
  memcpy(&bleBuffer[8], &proximity, 2); 
  memcpy(&bleBuffer[10], &gesture, 2); 
  
  Serial.print("red, green, blue, alpha, proximity, gesture: ");
  Serial.println(stringColor);

  colorCharacteristic.writeValue(bleBuffer, 12);
}

void getImu() {
  bool imuRead = false;
  float accelerometerX, accelerometerY, accelerometerZ;
  float gyroscopeX, gyroscopeY, gyroscopeZ;
  float magneticX, magneticY, magneticZ;
  
  if(IMU.accelerationAvailable()) {
    IMU.readAcceleration(accelerometerX, accelerometerY, accelerometerZ);;
    imuRead = true;
  }
  /* If new gyroscope data is available on the LSM9DS1 get the data.*/
  if(IMU.gyroscopeAvailable()) {
    IMU.readGyroscope(gyroscopeX, gyroscopeY, gyroscopeZ);
    imuRead = true;
  }
  /* If new magnetic data is available on the LSM9DS1 get the data.*/
  if (IMU.magneticFieldAvailable()) {
    IMU.readMagneticField(magneticX, magneticY, magneticZ);
    imuRead = true;
  }
  if (!imuRead) return;
  // Serial.printf("acc: %f,%f,%f gyr: %f,%f,%f mag: %f,%f,%f",
  //   accelerometerX, accelerometerY, accelerometerZ,
  //   gyroscopeX, gyroscopeY, gyroscopeZ,
  //   magneticX,magneticY, magneticZ
  // );
  Serial.println();
  int accX = (int) (1000 * accelerometerX);
  int accY = (int) (1000 * accelerometerY);
  int accZ = (int) (1000 * accelerometerZ);
  int gyrX = (int) (1000 * gyroscopeX);
  int gyrY = (int) (1000 * gyroscopeY);
  int gyrZ = (int) (1000 * gyroscopeZ);
  int magX = (int) (1000 * magneticX);
  int magY = (int) (1000 * magneticY);
  int magZ = (int) (1000 * magneticZ);
  memcpy(&bleBuffer[0], &accX, 2); 
  memcpy(&bleBuffer[2], &accY, 2); 
  memcpy(&bleBuffer[4], &accZ, 2); 
  memcpy(&bleBuffer[6], &gyrX, 2); 
  memcpy(&bleBuffer[8], &gyrY, 2); 
  memcpy(&bleBuffer[10], &gyrZ, 2); 
  memcpy(&bleBuffer[12], &magX, 2); 
  memcpy(&bleBuffer[14], &magY, 2); 
  memcpy(&bleBuffer[16], &magZ, 2); 
  
  imuCharacteristic.writeValue(bleBuffer, 18);
}

void onPDMdata() {
  // query the number of bytes available
  int bytesAvailable = PDM.available();

  // read into the sample buffer
  // 16-bit, 2 bytes per sample
  int bytesRead = PDM.read(micSampleBuffer, bytesAvailable);
  micSamplesRead = bytesRead / 2;
  unsigned long now = micros();
  for (int i = 0; i < micSamplesRead / 2; i++) {
    micLevel = 0.01 * micSampleBuffer[i] * micSampleBuffer[i] + 0.99 * micLevel;
    if (micLevel > micMax) micMax = micLevel;
  }
  if (micMillis > 0) {
    sampleRate = 0.9 * sampleRate + 100000.0 * micSamplesRead / (now - micMillis);
  }
  micMillis = now;
}
