package com.plusot.bluelib.util

import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object TimeUtil {
    const val HOUR: Long = 3600000L
    const val DAY = 24 * HOUR
    const val MINUTE = 60000L
    const val YEAR = 365 * DAY

    val timeInt: Int
        get() {
            val cal = Calendar.getInstance()
            return cal.get(Calendar.YEAR) % 2000 * 100000000 +
                    (cal.get(Calendar.MONTH) + 1) * 1000000 +
                    cal.get(Calendar.DAY_OF_MONTH) * 10000 +
                    cal.get(Calendar.HOUR_OF_DAY) * 100 +
                    cal.get(Calendar.MINUTE)
        }

    val currentYear: Int
        get() {
            val cal = Calendar.getInstance()
            return cal.get(Calendar.YEAR)
        }

    val offset: Int
        get() = TimeZone.getDefault().getOffset(System.currentTimeMillis())

    fun getTime(year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int): Long {
        val cal = GregorianCalendar(TimeZone.getTimeZone("GMT"))
        cal.set(year, month, day, hour, minute, second)
        return cal.time.time
    }

    fun formatFileTime(): String {
        return formatTime(System.currentTimeMillis(), "yyyyMMdd-HHmmss")
    }

    fun formatDateShort(value: Long): String {
        return formatTime(value, "yyyyMMdd")
    }

    fun formatTime(
        value: Long = System.currentTimeMillis(),
        pattern: String = "yyyy-MM-dd HH:mm:ss",
        locale: Locale = Locale.getDefault(),
        decimals: Int = 0,
        utc: Boolean = false
    ): String {
        var v = value
        if (v == -1L) v = System.currentTimeMillis()
        val format = SimpleDateFormat(pattern, locale)
        if (utc) format.timeZone = TimeZone.getTimeZone("UTC")
        val calendar= Calendar.getInstance()
        calendar.timeInMillis = v
        val timeStr =  format.format(Date(v)).replace("∂", calendar.get(Calendar.DAY_OF_MONTH).toOrdinalOrNot())
        return when (decimals) {
            0 -> timeStr
            1 -> "$timeStr.${String.format(locale, "%01d", v % 1000 / 100)}"
            2 -> "$timeStr.${String.format(locale, "%02d", v % 1000 / 10)}"
            else -> "$timeStr.${String.format(locale, "%03d", v % 1000)}"
        }
    }

    fun parseTime(value: String, pattern: String): Long {
        val format = SimpleDateFormat(pattern, Locale.US)
        val date: Date?
        try {
            date = format.parse(value)
        } catch (e: ParseException) {
            return 0
        }

        //longDateFormat.setTimeZone(UTC_TIMEZONE);
        return date.time
    }

    fun parseTimeUTC(value: String, pattern: String): Long {
        var patt = pattern
        var milli = 0
        if (patt.contains(".S")) {
            val valueParts = value.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val patternParts = patt.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (i in 0 until Math.min(patternParts.size, valueParts.size)) {
                if (patternParts[i].startsWith("S")) {
                    val milliPart = (valueParts[i] + "000").substring(0, 3)
                    try {
                        milli = Integer.parseInt(milliPart)
                    } catch (e: NumberFormatException) {
                        milli = 0
                    }

                    when (patternParts[i].length) {
                        3 -> patt = patt.replace(".SSS", "")
                        2 -> patt = patt.replace(".SS", "")
                        1 -> patt = patt.replace(".S", "")
                        else -> patt = patt.replace(".S", "")
                    }

                }
            }
        }
        val format = SimpleDateFormat(patt, Locale.US)
        format.timeZone = TimeZone.getTimeZone("UTC")
        val date: Date?
        try {
            date = format.parse(value)
        } catch (e: ParseException) {
            return 0
        }

        return date.time + milli
    }

    fun parseTimeUTC(value: String): Long {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        format.timeZone = TimeZone.getTimeZone("UTC")
        val date: Date?
        try {
            date = format.parse(value)
        } catch (e: ParseException) {
            return 0
        }

        return date.time
    }

    fun parseTime(value: String): Long {
        return parseTime(value, "yyyy-MM-dd HH:mm:ss")
    }

    fun formatElapsedMilli(value: Long, countDays: Boolean): String {
        var v = value
        val appContext = Globals.appContext ?: return ""

        if (countDays && v > DAY) return (v / DAY).toString() + " " + appContext.getString(R.string.days)
        val hours = (v / HOUR).toInt()
        if (hours > 99)
            return hours.toString() + " " + appContext.getString(R.string.hour)
        v %= HOUR
        val min = (v / 60000).toInt()
        v %= 60000
        val seconds = (v / 1000).toInt()
        v %= 1000
        if (hours == 0 && min == 0 && seconds == 0) return String.format(Locale.US, ".%d", v)
        if (hours == 0 && min == 0) return String.format(Locale.US, "%d.%03d", seconds, v)
        return if (hours == 0) String.format(
            Locale.US,
            "%d:%02d.%03d",
            min,
            seconds,
            v
        ) else String.format(Locale.US, "%d:%02d:%02d.%03d", hours, min, seconds, v)
    }

    fun formatElapsedMilliSpoken(value: Long, countDays: Boolean): String {
        var v = value
        var result = formatElapsedHourSecondsSpoken(v, countDays)
        val appContext = Globals.appContext

        if (v < DAY && appContext != null) {
            v %= 1000
            result += " " + String.format(Locale.US, "%d", v) + appContext.getString(R.string.ms_voice)
        }
        return result
    }

    fun formatElapsedTenths(value: Long, countDays: Boolean): String {
        var v = value
        val appContext = Globals.appContext ?: return ""

        if (countDays && v > DAY) return (v / DAY).toString() + " " + appContext.getString(R.string.days)
        val hours = (v / HOUR).toInt()
        if (hours > 99)
            return hours.toString() + " " + appContext.getString(R.string.hour)

        v %= HOUR
        val min = (v / 60000).toInt()
        v %= 60000
        val seconds = (v / 1000).toInt()
        v %= 1000
        v /= 100
        if (hours == 0 && min == 0 && seconds == 0) return String.format(Locale.US, ".%d", v)
        if (hours == 0 && min == 0) return String.format(Locale.US, "%d.%01d", seconds, v)
        return if (hours == 0) String.format(Locale.US, "%d:%02d.%01d", min, seconds, v) else String.format(
            Locale.US,
            "%d:%02d:%02d.%01d",
            hours,
            min,
            seconds,
            v
        )
    }

    fun formatElapsedTenthsSpoken(value: Long, countDays: Boolean): String {
        var v = value
        var result = formatElapsedHourSecondsSpoken(v, countDays)
        val appContext = Globals.appContext

        if (v < DAY && appContext != null) {
            v %= 1000
            v /= 100
            result += " " + String.format(Locale.US, "%d", v) + appContext.getString(R.string.tenths_voice)
        }
        return result
    }

    fun formatElapsedSeconds(value: Long, countDays: Boolean): String {
        var v = value
        val appContext = Globals.appContext ?: return ""

        if (countDays && v > DAY) return (v / DAY).toString() + " " + appContext.getString(R.string.days)
        val hours = (v / HOUR).toInt()
        if (hours > 99)
            return hours.toString() + " " + appContext.getString(R.string.hour)
        v %= HOUR
        val min = (v / 60000).toInt()
        v %= 60000
        val seconds = (v / 1000).toInt()
        //value %= 1000;
        if (hours == 0 && min == 0 && seconds == 0) return "0"
        if (hours == 0 && min == 0) return String.format(Locale.US, "%d", seconds)
        return if (hours == 0) String.format(Locale.US, "%d:%02d", min, seconds) else String.format(
            Locale.US,
            "%d:%02d:%02d",
            hours,
            min,
            seconds
        )
    }

    fun formatElapsedSecondsSpoken(value: Long, countDays: Boolean): String {
        var v = value
        val appContext = Globals.appContext ?: return ""

        if (countDays && v > DAY) return (v / DAY).toString() + " " + appContext.getString(R.string.days) + " " + v % DAY / HOUR + " " + appContext.getString(
            R.string.hours_voice
        )
        val hours = (v / HOUR).toInt()
        if (hours > 99) return hours.toString() + " " + appContext.getString(R.string.hours_voice)
        v %= HOUR
        val min = (v / 60000).toInt()
        v %= 60000
        val seconds = (v / 1000).toInt()
        //value %= 1000;
        if (hours == 0 && min == 0 && seconds == 0)
            return "0 " + appContext.getString(R.string.sec_voice)
        if (hours == 0 && min == 0)
            return String.format(Locale.US, "%d", seconds) + " " +
                    appContext.getString(R.string.sec_voice)
        if (hours == 0)
            return String.format(Locale.US, "%d", min) + "  " +
                    appContext.getString(R.string.minutes_voice) +
                    String.format(Locale.US, "%d", seconds) + " " +
                    appContext.getString(R.string.sec_voice)
        return if (hours == 1) String.format(Locale.US, "%d", hours) + "  " +
                appContext.getString(R.string.hour_voice) +
                String.format(Locale.US, "%d", min) + "  " +
                appContext.getString(R.string.minutes_voice) +
                String.format(Locale.US, "%d", seconds) + " " +
                appContext.getString(R.string.sec_voice) else String.format(Locale.US, "%d", hours) + "  " +
                appContext.getString(R.string.hours_voice) +
                String.format(Locale.US, "%d", min) + "  " +
                appContext.getString(R.string.minutes_voice) +
                String.format(Locale.US, "%d", seconds) + " " +
                appContext.getString(R.string.sec_voice)
    }

    fun formatElapsedHourSeconds(value: Long, countDays: Boolean): String {
        val appContext = Globals.appContext ?: return ""

        if (countDays && value > DAY) return (value / DAY).toString() + " " + appContext.getString(R.string.days)
        val hours = (value / HOUR).toInt()
        return if (hours >= 1) hours.toString() + " " + appContext.getString(R.string.hour) else formatElapsedSeconds(
            value,
            countDays
        )
    }

    fun formatElapsedHourSecondsSpoken(value: Long, countDays: Boolean): String {
        val appContext = Globals.appContext ?: return ""

        if (countDays && value > DAY) return (value / DAY).toString() + " " + appContext.getString(R.string.days) + " " + value % DAY / HOUR + " " + appContext.getString(
            R.string.hours_voice
        )
        val hours = (value / HOUR).toInt()
        if (hours == 1) return hours.toString() + " " + appContext.getString(R.string.hour_voice)
        return if (hours > 1) hours.toString() + " " + appContext.getString(R.string.hours_voice) else formatElapsedSecondsSpoken(
            value,
            countDays
        )
    }

    fun fileNameTime(): String {
        return formatTime(System.currentTimeMillis(), "yyyyMMdd-HHmmss")
    }

    fun fileNameDate(): String {
        return formatTime(System.currentTimeMillis(), "yyyyMMdd")
    }

    fun isLeapYear(year: Long): Boolean {
        return year % 4 == 0L && (year % 400 == 0L || year % 100 != 0L)
    }

    fun daysUpToYear(year: Long): Long {
        return if (year == 0L) 0 else (year - 1) / 4 + year * 365 - (year - 1) / 100 + (year - 1) / 400
    }

    fun daysUpTo(unixTime: Long = System.currentTimeMillis()): Long {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = unixTime
        return daysUpToYear(calendar.get(Calendar.YEAR).toLong()) + dayInYear(unixTime)
    }

    fun dayInYear(unixTime: Long): Int {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = unixTime
        return calendar.get(Calendar.DAY_OF_YEAR)
    }

    fun toSecondsSinceYear0(unixTime: Long = System.currentTimeMillis()): Long {
        val days1970 = daysUpToYear(1970)
        val seconds = unixTime / 1000
        val totalSeconds = seconds + days1970 * 86400
        println("Days until 1970 = $days1970, seconds since 1970 = $seconds: Seconds since year 0 = $totalSeconds ${Calc.pow2(38)}" )
        return totalSeconds
    }

    fun fromSecondsSinceYear0(zeroTime: Long): Long {
        val seconds1970 = daysUpToYear(1970) * 86400
        if (zeroTime < seconds1970) return 0L
        val time1970 = (zeroTime - seconds1970) * 1000L
        println("Time calculated ${Date(time1970)}" )
        return time1970
    }

}

fun Long.toSecondsSinceYear0(): Long = TimeUtil.toSecondsSinceYear0(this)
fun Long.toUnixTime(): Long = TimeUtil.fromSecondsSinceYear0(this)
fun Long.toTimeStr(
    pattern: String = "yyyy-MM-dd HH:mm:ss",
    locale: Locale = Locale.US,
    decimals: Int = 3,
    utc: Boolean = false
) = TimeUtil.formatTime(
    value = this,
    pattern = pattern,
    locale = locale,
    decimals = decimals,
    utc = utc
    )
