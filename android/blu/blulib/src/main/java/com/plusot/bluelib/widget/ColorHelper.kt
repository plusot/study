package com.plusot.bluelib.widget.graph

fun argbToInt(a: Int, r: Int, g: Int, b: Int) = (a shl 24) + (r shl 16) + (g shl 8) + b
