package com.plusot.bluelib.preferences


enum class PreferenceKey private constructor(label: String, defaultValue: Any, valueClass: Class<*>, flags: Long = 0) {
    PERMISSIONS_ACCEPTED("permissions_accepted", false, Boolean::class.java),
    WHEEL_CIRCUMFERENCE("wheel_circumference", 2.07, Double::class.java),
    STATS("stats_preference", true, Boolean::class.java),
    TOTAL_TIME("total_time", 0L, Long::class.java),
    TOTAL_DISTANCE("total_distance", 0.0, Double::class.java),
    PREVIOUS_PID("previous_pid", 0, Int::class.java),

    ;

    private val elements: PreferenceKeyElements = PreferenceKeyElements(label, defaultValue, valueClass, flags)

    var int: Int
        get() = elements.int
        set(value) {
            elements.set(value)
        }

    var double: Double
        get() = elements.double
        set(value) {
            elements.double = value
        }

    var long: Long
        get() = elements.long
        set(value) {
            elements.long = value
        }

    var string: String
        get() = elements.string
        set(value) {
            elements.string = value
        }

    var isTrue: Boolean
        get() = elements.boolean
        set(value) {
            elements.boolean = value
        }


    companion object {

        fun fromString(label: String): PreferenceKey? {
            for (key in PreferenceKey.values()) {
                if (key.elements.label == label) return key
            }
            return null
        }
    }

}