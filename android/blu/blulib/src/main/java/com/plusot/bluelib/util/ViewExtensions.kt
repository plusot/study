package com.plusot.bluelib.util

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView

enum class ButtonAction {
    CLICK,
    DELETE
}

inline fun <reified T : Activity> Activity.startActivity() = this.startActivity(Intent(this, T::class.java))

inline fun <reified T : Activity> Activity.startActivity(requestCode: Int) = this.startActivityForResult(Intent(this, T::class.java), requestCode)


inline fun <reified T : Activity> Activity.startActivity(func: (Intent) -> Unit) {
    val intent = Intent(this, T::class.java)
    func(intent)
    this.startActivity(intent)
}

inline fun <reified T : Activity> Activity.startActivity(requestCode: Int, func: (Intent) -> Unit) {
    val intent = Intent(this, T::class.java)
    func(intent)
    this.startActivityForResult(intent, requestCode)
}


var View.visible: Boolean
    get() = this.visibility == View.VISIBLE
    set(value) { if (value) this.visibility = View.VISIBLE else this.visibility = View.GONE }

var View.invisible: Boolean
    get() = this.visibility == View.INVISIBLE
    set(value) { if (value) this.visibility = View.INVISIBLE else this.visibility = View.VISIBLE }

var List<View>.visibleIndex: Int
    get() {
        this.forEachIndexed { index, view ->
            if (view.visible) return index
        }
        return -1
    }
    set(value) {
        this.forEachIndexed { index, view ->
            view.visible = (index  == value)
        }
    }

fun <T: View> List<T>.setVisible(first: Boolean)  {
    if (first) {
        this.first().visible = true
        this.forEachIndexed { index, view ->
            if (index > 0) view.visible = false
        }
    } else {
        this.last().visible = true
        this.forEachIndexed { index, view ->
            if (index < this.size - 1) view.visible = false
        }
    }
}

fun <T: View> List<T>.setVisible(value: Int) {
    this.forEachIndexed { index, view ->
        view.visible = (index  == value)
    }
}

fun <T: TextView> List<T>.setBold(value: Int) {
    this.forEachIndexed { index, view ->
        if (index == value)
            view.setTypeface(null, Typeface.BOLD)
        else
            view.setTypeface(null, Typeface.NORMAL)

    }
}

fun <T: View> List<T>.getVisible(): Int {
    this.forEachIndexed { index, view ->
        if (view.visible) return index
    }
    return -1
}

val View.xy: Pair<Int, Int>
    get() {
        val array = IntArray(2)
        this.getLocationInWindow(array)
        return array[0] to array[1]
    }

fun EditText.toDouble(defaultValue: Double = 0.0): Double = try {
    this.text.toString().toDouble()
} catch (e: NumberFormatException) {
    defaultValue
}

fun EditText.toInt(defaultValue: Int = 0): Int = try {
    this.text.toString().toInt()
} catch (e: NumberFormatException) {
    defaultValue
}

fun Activity.hideSoftKeyboard(v: View) {
    val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //val view: View? = this.currentFocus
    inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
}

fun View.setNewHeight(value: Int) {
    val layout = this.layoutParams
    layout.height = value
    this.layoutParams = layout
}

