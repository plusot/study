package com.plusot.bluelib.util

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.text.Html
import androidx.core.app.NotificationCompat
import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog

object Notifications {
    const val alarmChannelId = "alarmmanager_channel"
    private val channelCreated = mutableMapOf<String, Boolean>()

    private const val NOTIFICATION_TITLE = "NOTIFICATION_TITLE"
    private const val NOTIFICATION_MSG = "NOTIFICATION_MSG"

    private fun createChannel(context: Context,
                              channelId: String,
                              vibration: LongArray
    ) {
        if (channelCreated[channelId] == true) return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(
                channelId,
                R.string.channel_name.getString(R.string.app_name.getString()),
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.description =  R.string.channel_description.getString(R.string.app_name.getString())
            channel.enableLights(true)
            channel.lightColor = Color.GREEN
            channel.enableVibration(true)
            channel.vibrationPattern = vibration
            notificationManager.createNotificationChannel(channel)
            channelCreated[channelId] = true
        } else {
            LLog.i("Could not create notification audioChannel as SDK version is below Oreo")
        }
    }

    private fun createNotification(
        context: Context?,
        clazz: Class<out Activity>,
        title: String,
        msg: String,
        icon: Int,
        autoCancel: Boolean = true,
        channelId: String = alarmChannelId
    ): Notification? {
        if (context == null) return null
        val resultIntent = Intent(context, clazz)
        resultIntent.putExtra(NOTIFICATION_TITLE, title)
        resultIntent.putExtra(NOTIFICATION_MSG, msg)

        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(clazz)
        stackBuilder.addNextIntent(resultIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT) //PendingIntent.FLAG_ONE_SHOT)

        val html = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(msg, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(msg)
        }

        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(
                context,
                channelId,
                if (channelId == alarmChannelId)
                    longArrayOf(500, 500, 500, 500, 500, 500, 500, 500, 500, 500)
                else
                    longArrayOf(250, 250, 250, 250)
            )
            NotificationCompat.Builder(context, channelId)
        } else {
            @Suppress("DEPRECATION")
            NotificationCompat.Builder(context)
        }
            .setContentTitle(title)
            .setContentText(html)
            .setAutoCancel(autoCancel)
        if (icon != -1) {
            builder.setSmallIcon(icon)
            builder.setBadgeIconType(icon)
        }
        builder.setContentIntent(resultPendingIntent)

        LLog.i("title $title, text $html")
        return builder.build()
    }

    fun sendNotification(
        clazz: Class<out Activity>,
        title: String,
        msg: String,
        icon: Int

    ) {
        val context : Context = Globals.appContext ?: return
        val id = ++notificationId
        SleepAndWake.runInMain {
            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?)?.
                notify(id, createNotification(context, clazz, title, msg, icon))
        }
    }

    private var notificationId = 0

}