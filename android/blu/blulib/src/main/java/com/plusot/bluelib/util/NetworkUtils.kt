package com.plusot.bluelib.util

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.wifi.WifiManager
import com.plusot.bluelib.log.LLog
import java.net.InetAddress
import java.net.UnknownHostException


/**
 * Package: com.plusot.bluelib.util
 * Project: helloWatch
 *
 * Created by Peter Bruinink on 2019-05-12.
 * Copyright © 2019 Plusot. All rights reserved.
 */

fun intToInetAddress(hostAddress: Int): InetAddress? {
    val addressBytes = byteArrayOf(
        (0xff and hostAddress).toByte(),
        (0xff and (hostAddress shr 8)).toByte(),
        (0xff and (hostAddress shr 16)).toByte(),
        (0xff and (hostAddress shr 24)).toByte()
    )

    try {
        return InetAddress.getByAddress(addressBytes)
    } catch (e: UnknownHostException) {
        LLog.e("Could not convert inet address: $e")
    }
    return null;
}

fun Context.getIpAddress(): String {
    val wm: WifiManager = this.getSystemService(WIFI_SERVICE) as WifiManager? ?: return "0.0.0.0"
    return intToInetAddress(wm.connectionInfo.ipAddress)?.hostAddress ?: "0.0.0.0"

}