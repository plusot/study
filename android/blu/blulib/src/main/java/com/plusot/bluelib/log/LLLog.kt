package com.plusot.bluelib.log


import android.util.Log
import java.util.*

/**
 * A class to show the methods calling Log. Makes it easy to find what method higher up
 * in the stack calls this method you want logging for.<br>
 * Example: Calling LLog.d("Loading image $tempUrl", classDepth = 2) from ImageLoader.loadFromUrl could
 * tell you that ImageLoader.loadFromUrl was called by ProductTileViewHolder.setItem
 *
 * <p>
 * MethodLevel 3 is the method in LLog itself
 * MethodLevel 4 is the method calling LLog
 * Higher MethodLevels are methods calling the methods that call LLog. Are the methods calling the
 * methods calling the methods that call LLog. Etc
 * </p>
 */

object LLLog {
    private val history = mutableMapOf<String, Long>()
    private val mode = Mode.NORMAL
    private const val TAG = "applog"
    private const val defaultMethodLevel = 4

    enum class Mode {
        NORMAL,
        VERBOSE
    }

    private fun getMethod(
        minLevel: Int = defaultMethodLevel,
        classDepth: Int = 1,
        showAllMethodsWithinClass: Boolean = false,
        separator: String = ""): String {
        val stack = Thread.currentThread().stackTrace
        val sb = StringBuilder()

        var prevMethodName: String? = null
        var prevClassName: String? = null
        var classChangeCount = 0
        run loop@{
            stack.forEachIndexed { index, element ->
                if (    !element.isNativeMethod &&
                    index >= minLevel &&
                    element.className != null &&
                    element.className != this.javaClass.canonicalName &&
                    element.methodName != null) {
                    val className = when (mode) {
                        Mode.VERBOSE -> element.className
                        Mode.NORMAL -> element.className.split(".").last()
                    }
                    val lineNumber = element.lineNumber
                    val methodName = element.methodName.split("$").first()
                    if (methodName != prevMethodName && (showAllMethodsWithinClass || prevClassName != element.className)) {
                        if (sb.isNotEmpty()) sb.append("/$separator")
                        sb.append("$className.$methodName($lineNumber)")
                    }
                    prevMethodName = methodName
                    if (prevClassName != element.className) classChangeCount++
                    if (classChangeCount >= classDepth) {
                        return@loop
                    }
                    prevClassName = element.className
                }
            }
        }
        return sb.toString() + separator
    }

    private fun shouldShow(msg: String, historyTimeOut: Long): Boolean {
        if (historyTimeOut >= 0) {
            val time = history[msg]
            val now = System.currentTimeMillis()
            if (time != null && (historyTimeOut == 0L || now - time < historyTimeOut)) return false
            if (history.size > 1000) {
                var oldestMsg: String? = null
                var oldest = java.lang.Long.MAX_VALUE
                history.forEach { (key, value) ->
                    if (value < oldest) {
                        oldest = value
                        oldestMsg = key
                    }
                }
                oldestMsg?.let { history.remove(it) }
            }
            history[msg] = now
        }

        return true
    }

    private fun v(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (shouldShow(msg, checkHistory)) Log.v(tag, msg, tw)
    }

    fun v(msg: String, tw: Throwable? = null, checkHistory: Long = -1) {
        v(TAG, getMethod(defaultMethodLevel) + ": " + msg, tw, checkHistory)
    }

    private fun w(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (shouldShow(msg, checkHistory)) Log.w(tag, msg, tw)
    }

    fun w(msg: String, tw: Throwable? = null, checkHistory: Long = -1) {
        w(TAG, getMethod(defaultMethodLevel) + ": " + msg, tw, checkHistory)
    }

    private fun d(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (shouldShow(msg, checkHistory)) Log.d(tag, msg, tw)
    }

    fun d(msg: String, tw: Throwable? = null, checkHistory: Long = -1, classDepth: Int = 1, separator: String = "") {
        d(TAG, getMethod(defaultMethodLevel, classDepth, separator = separator) + ": " + msg, tw, checkHistory)
    }

    private fun i(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (shouldShow(msg, checkHistory)) Log.i(tag, msg, tw)
    }

    fun i(msg: String, tw: Throwable? = null, checkHistory: Long = -1) {
        i(TAG, getMethod(defaultMethodLevel) + ": " + msg, tw, checkHistory)
    }

    private fun e(tag: String, msg: String, tw: Throwable?, checkHistory: Long) {
        if (shouldShow(msg, checkHistory)) Log.e(tag, msg, tw)
    }

    fun e(msg: String, tw: Throwable? = null, checkHistory: Long = -1) {
        e(TAG, getMethod(defaultMethodLevel) + ": " + msg, tw, checkHistory)
    }
}

