package com.plusot.bluelib.sensor.internal

import com.plusot.bluelib.BlueLib
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.Device
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.SensorAccuracy
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.proper

class ScannedInternalDevice (type: DeviceType) : ScannedDevice(type),  Device.Listener {
    private var internalDevice: Device? = null

    override var isSelected: Boolean
        get() = super.isSelected
        set(selected) {
            super.isSelected = selected
            if (selected) {
                launchConnection()
            } else {
                internalDevice?.close("isSelected method")
                internalDevice = null
            }
        }

    override val name: String
        get() = type.toString()

    override val address: String
        get() = type.toString()

    override val iconId: Int
        get() = when (type) {
            DeviceType.INTERNAL_GPS -> R.drawable.ic_gps
            DeviceType.INTERNAL_CLOCK -> R.drawable.clock
            DeviceType.INTERNAL_ACCELEROMETER,
            DeviceType.INTERNAL_ACCELEROMETER_UNCALIBRATED -> R.drawable.ic_accelerometer
            DeviceType.INTERNAL_GEOMAGNETIC_FIELD -> R.drawable.ic_magnet
            DeviceType.INTERNAL_PROXIMITY -> R.drawable.ic_sensor
            DeviceType.INTERNAL_BATTERY_SENSOR -> R.drawable.ic_battery
            DeviceType.INTERNAL_MEMORY_SENSOR -> R.drawable.ic_memory
            DeviceType.INTERNAL_STEP_COUNTER,
            DeviceType.INTERNAL_STEP_DETECTOR-> R.drawable.ic_walking
            else -> R.drawable.ic_sensor
        }

    override val manufacturer: String
        get() = android.os.Build.MANUFACTURER

    init {
        if (isSelected) launchConnection()
    }

    private fun launchConnection() {
        if (internalDevice == null) internalDevice = when (type) {
            DeviceType.INTERNAL_GPS -> GpsDevice(this, BlueLib.useNmea)
            DeviceType.INTERNAL_CLOCK -> ClockDevice(this)
            DeviceType.INTERNAL_BATTERY_SENSOR -> BatteryDevice(this)
            DeviceType.INTERNAL_MEMORY_SENSOR -> MemoryDevice(this)

            DeviceType.INTERNAL_ACCELEROMETER,
            DeviceType.INTERNAL_GEOMAGNETIC_FIELD,
            DeviceType.INTERNAL_ORIENTATION,
            DeviceType.INTERNAL_GYROSCOPE,
            DeviceType.INTERNAL_LIGHT,
            DeviceType.INTERNAL_PRESSURE,
            DeviceType.INTERNAL_TEMPERATURE,
            DeviceType.INTERNAL_PROXIMITY,
            DeviceType.INTERNAL_GRAVITY,
            DeviceType.INTERNAL_LINEAR_ACCELERATION,
            DeviceType.INTERNAL_ROTATION_VECTOR,
            DeviceType.INTERNAL_RELATIVE_HUMIDITY,
            DeviceType.INTERNAL_AMBIENT_TEMPERATURE,
            DeviceType.INTERNAL_MAGNETIC_FIELD_UNCALIBRATED,
            DeviceType.INTERNAL_GAME_ROTATION_VECTOR,
            DeviceType.INTERNAL_GYROSCOPE_UNCALIBRATED,
            DeviceType.INTERNAL_SIGNIFICANT_MOTION,
            DeviceType.INTERNAL_STEP_DETECTOR,
            DeviceType.INTERNAL_STEP_COUNTER,
            DeviceType.INTERNAL_GEOMAGNETIC_ROTATION_VECTOR,
            DeviceType.INTERNAL_HEART_RATE_MONITOR,
            DeviceType.INTERNAL_WAKE_UP_TILT_DETECTOR,
            DeviceType.INTERNAL_WAKE_GESTURE,
            DeviceType.INTERNAL_GLANCE_GESTURE,
            DeviceType.INTERNAL_PICK_UP_GESTURE,
            DeviceType.INTERNAL_WRIST_TILT_GESTURE,
            DeviceType.INTERNAL_DEVICE_ORIENTATION,
            DeviceType.INTERNAL_POSE_6DOF,
            DeviceType.INTERNAL_STATIONARY_DETECT,
            DeviceType.INTERNAL_MOTION_DETECT,
            DeviceType.INTERNAL_HEART_BEAT,
            DeviceType.INTERNAL_DYNAMIC_SENSOR_META,
            DeviceType.INTERNAL_LOW_LATENCY_OFFBODY_DETECT,
            DeviceType.INTERNAL_ACCELEROMETER_UNCALIBRATED -> SensorsDevice(type, this)
            else -> null
        }
    }

    override fun onDeviceState(device: Device, state: Device.StateInfo, data: Data?) {
        val d = data ?: Data()
        d.add(address, DataType.STATE, state.toString())
        this.deviceData.merge(d)
        BlueLib.fireData(this, d, true)
        BlueLib.fireState(this, state, d)
    }

    override fun onDeviceData(device: Device, data: Data) {
        //            LLog.i(deviceData.toString());
        this.deviceData.merge(data)
        BlueLib.fireData(this, data, true)
    }

    override fun onCommandDone(
        device: Device,
        commandId: String,
        data: ByteArray,
        success: Boolean,
        closeOnDone: Boolean
    ) {
        if (closeOnDone) close("onCommandDone method")
    }

    override fun resumeDevice() { internalDevice?.resumeDevice() }

    override fun pauseDevice() { internalDevice?.pauseDevice() }

    override fun close(source: String) {
        if (internalDevice != null) {
            LLog.i("Closing: $name")
            internalDevice?.close(source)
        }
        internalDevice = null
    }
}
