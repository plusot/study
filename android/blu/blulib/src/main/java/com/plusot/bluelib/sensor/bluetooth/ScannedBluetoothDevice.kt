package com.plusot.bluelib.sensor.bluetooth

import android.bluetooth.BluetoothDevice
import com.plusot.bluelib.BlueLib
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.Device
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.bluetooth.ble.*
import com.plusot.bluelib.sensor.bluetooth.util.BTHelper
import com.plusot.bluelib.sensor.bluetooth.util.BTTypeMajor
import com.plusot.bluelib.sensor.bluetooth.util.BleManufacturer
import com.plusot.bluelib.sensor.bluetooth.util.BleState
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.*
import java.util.*

class ScannedBluetoothDevice(
    private val bluetoothDevice: BluetoothDevice,
    timeScanned: Long,
    isLowEnergy: Boolean
) : ScannedDevice(DeviceType.fromBluetoothDeviceTypeInt(bluetoothDevice.type)), Device.Listener {

    var deviceName: String? = null
    var bleDevice: BleDevice? = null
    override var iconId = R.drawable.ic_bt
        private set

    private var manufacturerId = 0
    private var manufacturerName: String? = null
    val lastRead: Long
        get() = bleDevice?.lastRead ?: -1L

    override val name: String
        get() {
            deviceName?.let { return it }
            var name = deviceData.getStringWithoutLabel(address, DataType.RADBEACON_DEVICE_NAME)
            if (name != null) return name
            name = deviceData.getStringWithoutLabel(address, DataType.DEVICE_NAME)
            return name ?: BTHelper.getName(bluetoothDevice)
        }

    override val address: String
        get() = bluetoothDevice.address

    override var isSelected: Boolean
        get() = super.isSelected
        set(selected) {
            val wasSelected = super.isSelected
            super.isSelected = selected
            if (selected) {
                if (bleDevice == null)
                    bleDevice = BleDevice(this, bluetoothDevice)
                else if (!wasSelected)
                    bleDevice?.reconnect(false)
            } else {
                close("isSelected method")
            }
        }

    init {
        timeRead = timeScanned
        val deviceClassInt = bluetoothDevice.bluetoothClass.deviceClass
        val typeMajor = BTTypeMajor.fromInt(bluetoothDevice.bluetoothClass.majorDeviceClass)
        val deviceClass = BTHelper.BTTypeMinor.fromInt(deviceClassInt)
        if (DEBUG) LLog.i("Device class is " + deviceClass.toString() + " (" + StringUtil.toHexString(deviceClassInt) + ")")
        when (typeMajor) {
            BTTypeMajor.COMPUTER -> {
                iconId = R.drawable.ic_computer
                iconId = R.drawable.ic_phone
                val name = BTHelper.getName(bluetoothDevice).toLowerCase(Locale.getDefault())
                if (name.contains("vívosmart"))
                    iconId = R.drawable.ic_vivosmart
                else if (name.contains("estimote"))
                    iconId = R.drawable.ic_estimote
                else if (name.contains("keyboard"))
                    iconId = R.drawable.ic_keyboard
                else if (name.startsWith("mio"))
                    iconId = R.drawable.ic_mio
                else if (name.startsWith("alpha"))
                    iconId = R.drawable.ic_alpha
                else if (name.startsWith("ib300"))
                    iconId = R.drawable.ic_ib300
                else if (name.startsWith("k4"))
                    iconId = R.drawable.ic_kestrel
                else if (name.contains("radbeacon"))
                    iconId = R.drawable.ic_radbeacon
                else if (isLowEnergy)
                    iconId = R.drawable.ic_ble
            }
            BTTypeMajor.PHONE -> {
                iconId = R.drawable.ic_phone
                val name = BTHelper.getName(bluetoothDevice).toLowerCase(Locale.getDefault())
                if (name.contains("vívosmart"))
                    iconId = R.drawable.ic_vivosmart
                else if (name.contains("estimote"))
                    iconId = R.drawable.ic_estimote
                else if (name.contains("keyboard"))
                    iconId = R.drawable.ic_keyboard
                else if (name.startsWith("mio"))
                    iconId = R.drawable.ic_mio
                else if (name.startsWith("alpha"))
                    iconId = R.drawable.ic_alpha
                else if (name.startsWith("ib300"))
                    iconId = R.drawable.ic_ib300
                else if (name.startsWith("k4"))
                    iconId = R.drawable.ic_kestrel
                else if (name.contains("radbeacon"))
                    iconId = R.drawable.ic_radbeacon
                else if (isLowEnergy)
                    iconId = R.drawable.ic_ble
            }
            else -> {
                val name = BTHelper.getName(bluetoothDevice).toLowerCase(Locale.getDefault())
                if (name.contains("vívosmart"))
                    iconId = R.drawable.ic_vivosmart
                else if (name.contains("estimote"))
                    iconId = R.drawable.ic_estimote
                else if (name.contains("keyboard"))
                    iconId = R.drawable.ic_keyboard
                else if (name.startsWith("mio"))
                    iconId = R.drawable.ic_mio
                else if (name.startsWith("alpha"))
                    iconId = R.drawable.ic_alpha
                else if (name.startsWith("ib300"))
                    iconId = R.drawable.ic_ib300
                else if (name.startsWith("k4"))
                    iconId = R.drawable.ic_kestrel
                else if (name.contains("radbeacon"))
                    iconId = R.drawable.ic_radbeacon
                else if (isLowEnergy)
                    iconId = R.drawable.ic_ble
            }
        }
        deviceData.add(address, DataType.TYPE_MAJOR, typeMajor.intType)
    }

    fun write(attribute: GattAttribute, data: Data, listener: GattCallListener) {
        if (bleDevice != null && bleDevice?.state == BleState.CONNECTED) {
            bleDevice?.write(attribute, data, listener)
        } else {
            return listener(BleRWSuccess.FAILURE, "Not connected")
        }
    }

    fun read(attribute: GattAttribute, listener: GattCallListener) {
        if (bleDevice != null && bleDevice?.state == BleState.CONNECTED) {
            bleDevice?.read(attribute, listener)
        } else {
            return listener(BleRWSuccess.FAILURE, "Not connected")
        }
    }

    override fun resumeDevice() { bleDevice?.resumeDevice() }

    override fun pauseDevice() { bleDevice?.pauseDevice() }


    override fun close(source: String) {
        bleDevice?.let { LLog.d("Closing ${it.name}") }
        bleDevice?.close(source)
        bleDevice = null
    }

    fun disconnect() {
        bleDevice?.disconnect()
    }

    /*
            * 0	4C - Byte 1 (LSB) of Company identifier code
            * 1	00 - Byte 0 (MSB) of Company identifier code (0x004C == Apple)
            * 2	02 - Byte 0 of iBeacon advertisement indicator
            * 3	15 - Byte 1 of iBeacon advertisement indicator
            * 4	e2 |\
            * 5	c5 |\\
            * 6	6d |#\\
            * 7	b5 |##\\
            * 8	df |###\\
            * 9	fb |####\\
            * 10	48 |#####\\
            * 11	d2 |#####|| iBeacon proximity UUID
                    * 12	b0 |#####||
            * 13	60 |#####//
            * 14	d0 |####//
            * 15	f5 |###//
            * 16	a7 |##//
            * 17	10 |#//
            * 18	96 |//
            * 19	e0 |/
            * 20	00 - major
            * 21	00
            * 22	00 - minor
            * 23	00
            * 24	c5 - The 2's complement of the calibrated Tx Power
    */
    internal fun parseScanRecord(scanRecord: ByteArray) {
        deviceData.setTime(System.currentTimeMillis())
        var iCount = 0
        var manuLoop = 0;
        while (iCount < scanRecord.size) {
            val dataLen = scanRecord[iCount].toInt() and 0xFF
            if (dataLen == 0) break
            if (iCount + dataLen >= scanRecord.size) break
            val type = ScanRecordType.tupleFromId(scanRecord[iCount + 1])

            val offset = iCount + 2
            when (type.t1()) {
                ScanRecordType.DATA_TYPE_LOCAL_NAME_COMPLETE -> {
                    val name = scanRecord.getString(offset, dataLen).trim('\u0000')
                    if (manufacturerName == "Kien.io") LLog.d("Local name = $name")
                }
                ScanRecordType.DATA_TYPE_FLAGS -> {
//                    for (i in 0 until dataLen) LLog.d("Flag = ${scanRecord.getUInt8(offset + i)}")
                }
                ScanRecordType.DATA_TYPE_MANUFACTURER_SPECIFIC_DATA -> {
                    manuLoop++;
                    if (manuLoop == 1) {


                        manufacturerId = Numbers.getUInt16(scanRecord, offset)
                        manufacturerName = BleManufacturer.getManufacturerName(manufacturerId)
//                        LLog.d("Manufacturer Id = $manufacturerId $manufacturerName")
                        deviceData.add(address, DataType.MANUFACTURER_ID, manufacturerId)
                        manufacturerName?.let {
                            deviceData.add(
                                address,
                                DataType.MANUFACTURER_NAME_STRING,
                                it
                            )
                        }
                        when (manufacturerName) {
                            "Texas Instruments Inc." -> {
                                iconId = R.drawable.ic_sensortag
                                //sensorTagFound = System.currentTimeMillis();
                                val keys = Numbers.getByte(scanRecord, 13)
                                deviceData.add(address, DataType.TI_LEFT_KEY, keys and 0x1)
                                deviceData.add(address, DataType.TI_RIGHT_KEY, keys and 0x2 shr 1)
                                deviceData.add(address, DataType.TI_REED_RELAY, keys and 0x4 shr 1)
                            }
                            "Kien.io" -> {
                                iconId = R.drawable.kien
                                val beaconCode = scanRecord.getUInt16BE(offset + 2)
                                val uuid = scanRecord.getBytes(offset + 4, 16).toHex()
                                val reserved = scanRecord.getUInt8(offset + 20)
                                val roomId = scanRecord.getUInt8(offset + 21)
                                val timeOfFlight = scanRecord.getUInt16(offset + 22)
                                val txRssi = scanRecord.getUInt8(offset + 24)
                                val batteryStatus = scanRecord.getUInt8(offset + 25)
                                LLog.d(
                                    "Scan Record for Kien: " +
                                            "\n          DataLen: $dataLen" +
                                            "\n           Record: ${scanRecord.toHex()}" +
                                            "\n           Record: ${scanRecord.toReadable()}" +
                                            "\n      Beacon code: ${beaconCode.toHex()}" +
                                            "\n             UUID: $uuid" +
                                            "\n         Reserved: ${reserved.toHex()}" +
                                            "\n          Room id: ${roomId.toHex()}" +
                                            "\n   Time of flight: ${timeOfFlight.toHex()}" +
                                            "\n          TX rssi: ${txRssi.toHex()}" +
                                            "\n   Battery status: ${batteryStatus.toHex()}"
                                )

                            }
                            "Radius Networks Inc." -> {
                                iconId = R.drawable.ic_radbeacon
                                deviceName = "RadBeacon"
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_ADVERTISEMENT,
                                    String.format("%X", Numbers.getUInt16(scanRecord, offset + 2))
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_MAJOR,
                                    Numbers.getUInt16(scanRecord, offset + 20)
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_UUID,
                                    StringUtil.toHex(scanRecord, offset + 4, 16)
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_MINOR,
                                    Numbers.getUInt16(scanRecord, offset + 22)
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_POWER,
                                    Numbers.getUInt8(scanRecord, offset + 24)
                                )
                            }
                            else -> {
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_ADVERTISEMENT,
                                    String.format("%X", Numbers.getUInt16(scanRecord, offset + 2))
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_MAJOR,
                                    Numbers.getUInt16(scanRecord, offset + 20)
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_UUID,
                                    StringUtil.toHex(scanRecord, offset + 4, 16)
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_MINOR,
                                    Numbers.getUInt16(scanRecord, offset + 22)
                                )
                                deviceData.add(
                                    address,
                                    DataType.IBEACON_POWER,
                                    Numbers.getUInt8(scanRecord, offset + 24)
                                )
                            }
                        }
                        if (isSelected || manufacturerId == 0x023D)
                            BlueLib.fireData(this, deviceData, true)
                    } else if (manuLoop == 2 && manufacturerName == "Kien.io") {
                        val picUpdate = scanRecord.getString(offset, 4)
                        val espUpdate = scanRecord.getString(offset + 4, 4)
                        val coco = scanRecord.getBytes(offset + 8, 3)
                        LLog.d(
                            "Scan Record for Kien response: " +
                                    "\n          Pic update: $picUpdate" +
                                    "\n          Esp update: $espUpdate" +
                                    "\n                Coco: ${coco.toHex()}"
                        )

                    }
                }
                ScanRecordType.DATA_TYPE_PLUSOT_MTAG -> {
                    manufacturerName = "Plusot"
                    iconId = R.drawable.ic_sensortag
                    val rawObject = Numbers.getUInt16(scanRecord, BleConst.MTAG_IR_TEMPERATURE_LOCAL_OFFSET)
                    val rawAmbient = Numbers.getUInt16(scanRecord, BleConst.MTAG_IR_TEMPERATURE_TARGET_OFFSET)
                    deviceData.add(address, DataType.TEMPERATURE_OBJECT, 1.0 * rawObject / 128)
                    deviceData.add(address, DataType.TEMPERATURE_IR, 1.0 * rawAmbient / 128)

                    val rawTemp = Numbers.getUInt16(scanRecord, BleConst.MTAG_TEMPERATURE_OFFSET)
                    val rawHum = Numbers.getUInt16(scanRecord, BleConst.MTAG_HUMIDITY_OFFSET)
                    var temperature = 165.0 * rawTemp / 65536 - 40
                    val humidity = 1.0 * rawHum / 65536
                    deviceData.add(address, DataType.HUMIDITY, humidity)
                    deviceData.add(address, DataType.TEMPERATURE, temperature)

                    val rawLight = Numbers.getUInt16(scanRecord, BleConst.MTAG_LUMINENCE_OFFSET)
                    val mantissa = rawLight and 0x0FFF
                    val exponent = rawLight and 0xF000 shr 12
                    val light = 0.01 * mantissa.toDouble() * Math.pow(2.0, exponent.toDouble())
                    deviceData.add(address, DataType.LIGHT, light)

                    val rawTemperatureValue = Numbers.getUInt24(scanRecord, BleConst.MTAG_TEMPERATURE2_OFFSET)
                    val rawPressureValue = Numbers.getUInt24(scanRecord, BleConst.MTAG_PRESSURE_OFFSET)
                    val pressure: Double = 1.0 * rawPressureValue
                    temperature = 0.01 * rawTemperatureValue
                    deviceData.add(address, DataType.AIR_PRESSURE, pressure)
                    deviceData.add(address, DataType.TEMPERATURE_2, temperature)

                    val tKelvin = temperature + 273.15
                    val pSaturation = 611.21 * Math.exp((18.678 - temperature / 234.5) * (temperature / (temperature + 257.14))) // Buck equation, Pa
                    val pVapor = humidity * pSaturation
                    val pDryAir = pressure - pVapor
                    val rho = pDryAir / (tKelvin * R_SPECIFIC_AIR) + pVapor / (tKelvin * R_SPECIFIC_WATER) // kg/m3
                    //                    LLog.i("Rho = " + Format.format(rho, 3) + ", Rho dry air = " + Format.format(rhoDryAir, 3));
                    deviceData.add(address, DataType.RHO, rho)

                    BlueLib.fireData(this, deviceData, true)
                    isBeacon = true
                }
                else -> {}
            }
            iCount += dataLen + 1

        }
    }

    override fun onDeviceState(device: Device, state: Device.StateInfo, data: Data?) {
        if (DEBUG) LLog.i("State = $state")
        when (state) {
            Device.StateInfo.DISCONNECTED -> { }
            Device.StateInfo.CLOSED -> {
                if (device === bleDevice) bleDevice = null
//                if (device === btDevice) btDevice = null
            }
            else -> {}
        }
        BlueLib.fireState(this, state, data)
    }

    override fun onDeviceData(device: Device, data: Data) {
        reconnectCount = 0
        timeRead = data.getTime()
        data.merge(data)
        BlueLib.fireData(this, data, true)
    }

    override fun onCommandDone(
        device: Device,
        commandId: String,
        data: ByteArray,
        success: Boolean,
        closeOnDone: Boolean
    ) {
        LLog.i("Result of command for $commandId = $success")
        if (closeOnDone) {
            bleDevice?.close("onCommandDone method")
            bleDevice = null
        }
    }

    override val manufacturer: String
        get() {
            if (manufacturerId > 0) manufacturerName = BleManufacturer.getManufacturerName(manufacturerId)
            return manufacturerName?.let { it } ?: "Manufacturer $manufacturerId"
        }

    companion object {
        private val DEBUG = false
        private val R_SPECIFIC_AIR = 287.058    // J/(kg⋅K)
        private val R_SPECIFIC_WATER = 461.495    // J/(kg⋅K)
    }


}

fun parseManufacturer(scanRecord: ByteArray): Pair<Int, String> {
    var iCount = 0
    var manufacturerId = 0
    var manufacturer = ""
    while (iCount < scanRecord.size) {
        val dataLen = scanRecord[iCount].toInt() and 0xFF
        if (dataLen == 0) break
        if (iCount + 1 >= scanRecord.size) break
        val type = ScanRecordType.tupleFromId(scanRecord[iCount + 1])
        //LLog.i("Record at " + iCount + ", datatype " + type.t2() + " = " + type.t1().parse(scanRecord, iCount + 2, dataLen - 1));

        val offset = iCount + 2
        when (type.t1()) {
            ScanRecordType.DATA_TYPE_LOCAL_NAME_COMPLETE -> {
//                    val name = scanRecord.getString(offset, dataLen).trim('?')
//                    LLog.d("Local name = $name")
            }
            ScanRecordType.DATA_TYPE_FLAGS -> {
//                    for (i in 0 until dataLen) LLog.d("Flag = ${scanRecord.getUInt8(offset + i)}")
            }
            ScanRecordType.DATA_TYPE_MANUFACTURER_SPECIFIC_DATA -> {
                manufacturerId = Numbers.getUInt16(scanRecord, offset)
                manufacturer = BleManufacturer.getManufacturerName(manufacturerId)
            }
            ScanRecordType.DATA_TYPE_PLUSOT_MTAG -> {
                manufacturer = "Plusot"
            }
            else -> {}
        }
        iCount += dataLen + 1
    }
    return manufacturerId to manufacturer
}
