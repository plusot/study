package com.plusot.bluelib.widget.graph

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import kotlin.math.log10
import kotlin.math.pow
import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.util.cut
import kotlin.math.ceil

class Axes(
    context: Context,
    //private val axesType: AxesType,
    private val xLabel: String? = null,
    private val yLabel: String? = null,
) {
    private val paint: Paint = Paint()
    private val textPaint: Paint = Paint()
    private val path: Path = Path()
    private val currentPoint = Point()

    private fun calcStep(len: Float): Float {
        val power = log10((len / 10).toDouble()).toInt()
        val step = 10.0.pow(power).toFloat()
        val steps = (len / step).toInt()
        return when {
            steps > 75 -> step * 20F
            steps > 50 -> step * 10F
            steps > 25 -> step * 5F
            steps > 10 -> step * 2F
            else -> step
        }
    }

    init {
        paint.also {
            it.style = Paint.Style.STROKE
            it.strokeCap = Paint.Cap.BUTT
            it.strokeWidth = 3.0F
            it.isAntiAlias = true
            it.textSize = 18F
            Globals.appContext?.let { ctx -> it.typeface = ResourcesCompat.getFont(ctx, R.font.frutiger) }
            it.color = ContextCompat.getColor(context, R.color.shaded)
        }
        textPaint.also {
            it.typeface = ResourcesCompat.getFont(context, R.font.frutiger)
            it.textSize = 15F
            it.color = ContextCompat.getColor(context, R.color.dark)
        }
    }

    fun draw(canvas: Canvas,
             yAxisVisible: Boolean,
             minXVal: Float,
             minYVal: Float,
             maxXVal: Float,
             maxYVal: Float,
             posX: Float = 0F,
             posY: Float = 0F,
             width: Float,
             height: Float,
             border: Float = 15f) {
        val xLen = maxXVal - minXVal
        val yLen = maxYVal - minYVal
        textPaint.textSize = (width + height) / 80F

        val stepX = calcStep(xLen)
        val stepY = calcStep(yLen)

        path.reset()

        // x-labels
        currentPoint.y = if (minYVal < 0F )
            posY + height - border - (height - 2 * border) * (0F - minYVal) / yLen
        else
            posY + height - border - 2.4F * textPaint.textSize

        var x = if (minXVal > 0) ceil(minXVal / stepX) * stepX else stepX
        while (x < maxXVal) {
            currentPoint.x = posX + (width - 2 * border) * (x - minXVal) / xLen + border
            val text = x.cut(stepX)
            canvas.drawText(
                text,
                currentPoint.x - textPaint.measureText(text) / 2,
                currentPoint.y + 1.2F * textPaint.textSize,
                textPaint
            )
            x += stepX
        }
        if (minXVal < 0) {
            x = -stepX
            while (x > minXVal) {
                currentPoint.x = posX + (width - 2 * border) * (x - minXVal) / xLen + border
                val text = x.cut(stepX)
                canvas.drawText(
                    text,
                    currentPoint.x - textPaint.measureText(text) / 2,
                    currentPoint.y + 1.2F * textPaint.textSize,
                    textPaint
                )
                x -= stepX
            }
        }

        // x-axis
        currentPoint.x = posX + border
        path.moveTo(currentPoint.x, currentPoint.y)

        currentPoint.x = posX + width - border
        path.lineTo(currentPoint.x, currentPoint.y)

        //x-label
        val label = if (!yAxisVisible && yLabel != null) {
            if (xLabel != null) "x = $xLabel, y = $yLabel" else "y = $yLabel"
        } else
            xLabel
        if (label != null ) canvas.drawText(
            label,
            currentPoint.x - textPaint.measureText(label) - 1,
            currentPoint.y + 2.4F * textPaint.textSize,
            textPaint
        )

        // y-labels
        currentPoint.x = if (yAxisVisible)
            posX + (width - 2 * border) * (0F - minXVal) / xLen + border - textPaint.textSize * 0.2F
        else
            posX + width - border
        var minXPosYLabel = Float.POSITIVE_INFINITY

        var y = if (minYVal > 0) ceil(minYVal / stepY) * stepY else stepY
        while (y < maxYVal) {
            currentPoint.y = posY + height - border - (height - 2 * border) * (y - minYVal) / yLen
            val text = y.cut(stepY)
            val xPos = currentPoint.x - textPaint.measureText(text)
            minXPosYLabel = minOf(xPos, minXPosYLabel)
            canvas.drawText(
                text,
                xPos,
                currentPoint.y + textPaint.textSize * .3F,
                textPaint
            )
            y += stepY
        }
        if (minYVal < 0) {
            y = -stepY
            while (y > minYVal) {
                currentPoint.y =
                    posY + height - border - (height - 2 * border) * (y - minYVal) / yLen
                val text = y.cut(stepY)
                val xPos = currentPoint.x - textPaint.measureText(text)
                minXPosYLabel = minOf(xPos, minXPosYLabel)
                canvas.drawText(
                    text,
                    xPos,
                    currentPoint.y + textPaint.textSize * .3F,
                    textPaint
                )
                y -= stepY
            }
        }

        // y-axis
        if (yAxisVisible) {
            currentPoint.set(
                posX + (width - 2 * border) * (0F - minXVal) / xLen + border,
                posY + height - border
            )
            path.moveTo(currentPoint.x, currentPoint.y)

            currentPoint.set(
                posX + (width - 2 * border) * (0F - minXVal) / xLen + border,
                posY + border
            )
            path.lineTo(currentPoint.x, currentPoint.y)
            if (yLabel != null) {
                currentPoint.set(
                    minXPosYLabel - textPaint.textSize * .2F,
                    currentPoint.y + textPaint.measureText(yLabel),
                )
                canvas.rotate(-90F, currentPoint.x, currentPoint.y)
                canvas.drawText(yLabel, currentPoint.x, currentPoint.y, textPaint)
                canvas.rotate(90F, currentPoint.x, currentPoint.y)
            }
        }

        canvas.drawPath(path, paint)
    }
}
