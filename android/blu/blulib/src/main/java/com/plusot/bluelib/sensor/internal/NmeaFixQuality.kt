package com.plusot.bluelib.sensor.internal

enum class NmeaFixQuality private constructor(internal val id: Int) {
    INVALID(0),
    GPS_FIX(1),
    DGPS_FIX(2),
    PPS_FIX(3),
    RTK(4),
    RTK_FLOAT(5),
    ESTIMATED(6),
    MANUAL_INPUT(7),
    SIMULATION(8);

    companion object {
        fun fromId(id: Int): NmeaFixQuality {
            return values()[id % values().size]
        }
    }
}