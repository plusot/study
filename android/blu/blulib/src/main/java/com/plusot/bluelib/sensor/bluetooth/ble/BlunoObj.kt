package com.plusot.bluelib.sensor.bluetooth.ble

/**
 * Package: com.plusot.bluelib.sensor.bluetooth.ble
 * Project: blu
 *
 * Created by Peter Bruinink on 2019-12-14.
 * Copyright © 2019 Plusot. All rights reserved.
 */

class BlunoObj(val alive: Long?, val info: String?, val voltage: Int?)

