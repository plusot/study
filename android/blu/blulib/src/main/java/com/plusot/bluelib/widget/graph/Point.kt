package com.plusot.bluelib.widget.graph

class Point {
    var x = 0f
    var y = 0f
    var dx = 0f
    var dy = 0f

    fun set(x: Float, y: Float) {
        this.x = x
        this.y = y
    }

    fun dset(dx: Float, dy: Float) {
        this.dx = dx
        this.dy = dy
    }

    fun set(p: Point) {
        x = p.x
        y = p.y
        dx = p.dx
        dy = p.dy
    }

    override fun toString(): String {
        return "$x, $y"
    }
}
