package com.plusot.bluelib.util


class MathVector {
    private var values: DoubleArray = DoubleArray(0)
    private var ref: MathVector? = null

    val floatValues: FloatArray
        get() {
            val result = FloatArray(values.size)
            for (i in values.indices) {
                result[i] = values[i].toFloat()
            }
            return result
        }

    constructor(d1: Double, d2: Double, d3: Double) {
        values = DoubleArray(3)
        values[0] = d1
        values[1] = d2
        values[2] = d3
        setReference()
    }

    constructor(d1: Double, d2: Double) {
        values = DoubleArray(2)
        values[0] = d1
        values[1] = d2
        setReference()
    }

    constructor(vector: DoubleArray) : this(vector, true)
    constructor(vector: IntArray) : this(vector.map{ it.toDouble()}.toDoubleArray(), true)
    constructor(vector: ByteArray) : this(vector.map{ it.toDouble()}.toDoubleArray(), true)

    constructor(vector: FloatArray, maxLength: Int = 1000) {
        values = DoubleArray(Math.min(maxLength, vector.size))
        for (i in values.indices) {
            values[i] = vector[i].toDouble()
        }
        setReference()
    }

    private constructor(vector: DoubleArray?, isSetReference: Boolean) {
        if (vector == null) return
        values = DoubleArray(vector.size)
        System.arraycopy(vector, 0, values, 0, values.size)
        if (isSetReference) setReference()
    }

    constructor(vector: MathVector) : this(vector.values)

    @JvmOverloads
    constructor(length: Int, unityPos: Int = -1) {
        values = DoubleArray(length)
        for (i in 0 until length) {
            if (i == unityPos)
                values[i] = 1.0
            else
                values[i] = 0.0
        }
        setReference()
    }

    fun size(): Int {
        return values.size
    }

    fun assign(vector: FloatArray?) {
        if (vector != null)
            for (i in 0 until Math.min(values.size, vector.size)) values[i] = vector[i].toDouble()
    }

    fun assign(vector: MathVector?) {
        if (vector != null)
            for (i in 0 until Math.min(values.size, vector.values.size))
                values[i] = vector.values[i]
    }

    fun setReference() {
        ref = MathVector(values, false)
    }

    fun maxDelta(): Double {
        if (ref == null) return java.lang.Double.MAX_VALUE
        val delta = this.min(ref)
        return delta?.maxAbs() ?: java.lang.Double.MAX_VALUE
    }

    fun deltaLength(): Double {
        if (ref == null) return java.lang.Double.MAX_VALUE
        val delta = this.min(ref)
        return delta?.length() ?: java.lang.Double.MAX_VALUE
    }

    fun min(v: MathVector?): MathVector? {
        if (v == null) return null
        val length = Math.min(values.size, v.values.size)
        val result = MathVector(length)
        for (i in 0 until length) {
            result.values[i] = values[i] - v.values[i]
        }
        return result
    }

    operator fun plus(v: MathVector?): MathVector? {
        if (v == null) return null
        val length = Math.min(values.size, v.values.size)
        val result = MathVector(length)
        for (i in 0 until length) {
            result.values[i] = values[i] + v.values[i]
        }
        return result
    }

    fun add(v: FloatArray?) {
        if (v == null) return
        val length = Math.min(values.size, v.size)
        for (i in 0 until length) {
            values[i] += v[i].toDouble()
        }
    }

    fun add(v: FloatArray?, filter: Double) {
        if (v == null) return
        val length = Math.min(values.size, v.size)
        for (i in 0 until length) {
            values[i] *= 1.0 - filter
            values[i] += filter * v[i]
        }
    }

    fun deduct(v: MathVector?) {
        if (v == null) return
        val length = Math.min(values.size, v.values.size)
        for (i in 0 until length) {
            values[i] -= v.values[i]
        }
    }

    fun add(v: MathVector?) {
        if (v == null) return
        val length = Math.min(values.size, v.values.size)
        for (i in 0 until length) {
            values[i] += v.values[i]
        }
    }

    fun timestimes(d: Double) {
        for (i in values.indices) {
            values[i] *= d
        }
    }

    operator fun times(d: Double): MathVector {
        val result = MathVector(values.size)
        for (i in values.indices) {
            result.values[i] = values[i] * d
        }
        return result
    }

    fun length(): Double {
        var sum = 0.0
        for (value in values) {
            sum += value * value
        }
        return Math.sqrt(sum)
    }

    fun maxAbs(): Double {
        var max = 0.0
        for (value in values) {
            max = Math.max(max, Math.abs(value))
        }
        return max
    }

    fun avg(): Double {
        var sum = 0.0
        for (value in values) {
            sum += value
        }
        return sum / values.size
    }

    /*public double avgNoZeroOrNegative() {
		double sum = 0;
		int div = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > 0) {
				sum += values[i];
				div++;
			}
		}
		if (div == 0) return 0;
		return sum / div;
	}*/

    fun dotProduct(other: MathVector): Double {
        var result = 0.0
        for (i in 0 until Math.min(values.size, other.values.size)) {
            result += values[i] * other.values[i]
        }
        return result
    }

    fun crossProduct(other: MathVector): MathVector {
        val result = MathVector(3)
        if (values.size >= 3 && other.values.size >= 3) {
            result.values[0] = values[1] * other.values[2] - values[2] * other.values[1]
            result.values[1] = values[2] * other.values[0] - values[0] * other.values[2]
            result.values[2] = values[0] * other.values[1] - values[1] * other.values[0]
        }

        return result
    }

    fun crossProductPart(other: MathVector): Double {
        return if (values.size >= 2 && other.values.size >= 2) {
            values[0] * other.values[1] - values[1] * other.values[0]
        } else 0.0
    }

    fun unitVector(): MathVector {
        val result = MathVector(this)
        val length = length()
        for (i in values.indices) {
            result.values[i] = values[i] / length
        }
        return result
    }

    fun largestAxis(): Int {
        var axis = 0
        var value = 0.0
        for (i in values.indices) {
            if (Math.abs(values[i]) > value) {
                value = Math.abs(values[i])
                axis = i
            }
        }
        return axis
    }

    fun getClonedValues(): DoubleArray {
        val result = DoubleArray(values.size)
        System.arraycopy(values, 0, result, 0, values.size)
        return result
    }

    fun getValue(index: Int): Double {
        return if (index >= values.size || index < 0) 0.0 else values[index]
    }

    fun getAbsValue(index: Int): Double {
        return if (index >= values.size || index < 0) 0.0 else Math.abs(values[index])
    }

    override fun toString(): String {
        return '('.toString() + StringUtil.toString(values, ", ", 4) + ')'.toString()
    }

    fun toString(decimals: Int): String {
        return '('.toString() + StringUtil.toString(values, ", ", decimals) + ')'.toString()
    }

}
