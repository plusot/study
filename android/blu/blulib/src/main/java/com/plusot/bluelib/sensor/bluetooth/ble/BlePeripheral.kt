package com.plusot.bluelib.sensor.bluetooth.ble

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothGattService
import android.os.ParcelUuid
import com.plusot.bluelib.sensor.data.Data
import java.util.*

/**
 * Package: com.plusot.bluelib.sensor.bluetooth.ble
 * Project: kaleido-app
 *
 * Created by Peter Bruinink on 2019-06-11.
 * Copyright © 2019 Plusot. All rights reserved.
 */

interface PeripheralServiceDelegate {
    fun sendNotification(characteristic: BluetoothGattCharacteristic)
}

abstract class BlePeripheral(serviceUUID: UUID) {
    val service: BluetoothGattService = BluetoothGattService(serviceUUID, BluetoothGattService.SERVICE_TYPE_PRIMARY)
    var serviceDelegate: PeripheralServiceDelegate? = null
    val characteristics: Set<GattAttribute>
        get() = service.characteristics.map { GattAttribute.fromUUID(it.uuid) }.toSet()
    var currentValue = mutableMapOf<UUID, ByteArray>()
    abstract fun writeCharacteristic(characteristic: BluetoothGattCharacteristic, offset: Int, value: ByteArray): Int
    abstract fun notificationsDisabled(characteristic: BluetoothGattCharacteristic )
    abstract fun notificationsEnabled(characteristic: BluetoothGattCharacteristic, indicate: Boolean)
    abstract fun setValue(data: Data)
    fun getServiceUUID() = ParcelUuid(service.uuid)


    companion object {
        fun getClientCharacteristicConfigurationDescriptor(): BluetoothGattDescriptor {
            val descriptor = BluetoothGattDescriptor(
                GattAttribute.CLIENT_CHARACTERISTIC_CONFIGURATION.uuid,
                BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE
            )
            descriptor.value = byteArrayOf(0, 0)
            return descriptor
        }

        fun getCharacteristicUserDescriptionDescriptor(defaultValue: String): BluetoothGattDescriptor {
            val descriptor = BluetoothGattDescriptor(
                GattAttribute.CHARACTERISTIC_USER_DESCRIPTION.uuid,
                BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE
            )
            try {
                descriptor.value = defaultValue.toByteArray(charset("UTF-8"))
            } finally {
                return descriptor
            }
        }
    }
}

