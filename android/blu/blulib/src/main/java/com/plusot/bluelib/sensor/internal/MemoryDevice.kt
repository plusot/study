package com.plusot.bluelib.sensor.internal

import android.os.Build
import android.os.Debug
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.ShellExec
import com.plusot.bluelib.util.SleepAndWake

import java.io.IOException

class MemoryDevice(scannedDevice: ScannedDevice) : InternalDevice(DeviceType.INTERNAL_MEMORY_SENSOR, scannedDevice) {
    private var topCmd = emptyArray<String>()
    private var rss: Long = 0
    private var prevRss: Long = 0
    private var cpuUsage = 0
    private var prevCpuUsage = 0
    private var multiplier = 1.0
    private var mayRun = true

    private val thread = Thread {
        while (mayRun && Globals.runMode.isRun) {

            val runtime = Runtime.getRuntime() ?: continue
            var gcAllocated: Long = -1
            var gcFreed: Long = -1
            //Debug.getMemoryInfo();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val gcAllocatedStr = Debug.getRuntimeStat("art.gc.bytes-allocated")
                val gcFreedStr = Debug.getRuntimeStat("art.gc.bytes-freed")
                try {
                    gcAllocated = java.lang.Long.parseLong(gcAllocatedStr)
                } catch (e: NumberFormatException) {
                    LLog.i("Could not convert $gcAllocatedStr to a number")
                }

                try {
                    gcFreed = java.lang.Long.parseLong(gcFreedStr)
                } catch (e: NumberFormatException) {
                    LLog.i("Could not convert $gcFreedStr to a number")
                }

            }
            SleepAndWake.runInNewThread() {
                try {
                    ShellExec().exec(topCmd, 200, object : ShellExec.ShellCallback {
                        override fun shellOut(shellLine: String) {
                            //                            LLog.i("Top = " + shellLine);
                            val parts = shellLine.trim { it <= ' ' }.split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }
                                .toTypedArray()
                            if (parts.size > 8) {
                                //processInfo.pid = parts[0];
                                if (parts[2].endsWith("%")) parts[4] = parts[2]
                                if (parts[4].endsWith("%")) {
                                    parts[4] = parts[4].substring(0, parts[4].length - 1)

                                    try {
                                        cpuUsage = Integer.parseInt(parts[4])
                                    } catch (e: NumberFormatException) {
                                        LLog.i("Could not convert " + parts[4] + " to cpu usage (" + shellLine + ")")
                                    }

                                }

                                //                                try {
                                //                                    processInfo.threads = Integer.parseInt(parts[6]);
                                //                                } catch (NumberFormatException e) {
                                //                                    LLog.i("Could not convert " + parts[6] + " to number of threads (" + shellLine + ")");
                                //                                }
                                //processInfo.vss = parts[7];
                                if (parts[7].endsWith("g")) parts[7] = parts[6]
                                if (parts[7].endsWith("K")) {
                                    parts[7] = parts[7].substring(0, parts[7].length - 1)
                                    multiplier = 1024.0
                                }

                                try {
                                    rss = java.lang.Long.parseLong(parts[7])
                                } catch (e: NumberFormatException) {
                                    LLog.i("Could not convert " + parts[7] + " to rss (" + shellLine + ")")
                                }

                            }
                        }

                        override fun processComplete(tag: Int, commandLine: String, exitValue: Int) {

                        }
                    })
                } catch (e: IOException) {
                    LLog.i("Exception calling top command", e)
                } catch (e: InterruptedException) {
                    LLog.i("Top command interrupted")
                }
            }

            val data = Data()
            if (gcAllocated > 0) data.add(address, DataType.MEMORY_GC_ALLOCATED, 1.0 * gcAllocated)
            if (gcFreed > 0) data.add(address, DataType.MEMORY_GC_FREED, 1.0 * gcFreed)
            if (gcAllocated > 0 && gcFreed > 0) data.add(address, DataType.MEMORY_GC, 1.0 * (gcAllocated - gcFreed))
            if (rss != prevRss && DataType.MEMORY_RSS.visible) {
                prevRss = rss
                data.add(address, DataType.MEMORY_RSS, multiplier * rss)
            }
            if (DataType.MEMORY_FREE.visible) data.add(address, DataType.MEMORY_FREE, 1.0 * runtime.freeMemory())
            if (DataType.MEMORY_TOTAL.visible) data.add(address, DataType.MEMORY_TOTAL, 1.0 * runtime.totalMemory())
            if (DataType.MEMORY_MAX.visible) data.add(address, DataType.MEMORY_MAX, 1.0 * runtime.maxMemory())
            if (DataType.MEMORY_HEAP_ALLOCATED.visible) data.add(
                address,
                DataType.MEMORY_HEAP_ALLOCATED,
                1.0 * Debug.getNativeHeapAllocatedSize()
            )

            if (cpuUsage != prevCpuUsage) {
                prevCpuUsage = cpuUsage
                data.add(address, DataType.CPU, 0.01 * cpuUsage)
            }
            fireData(
                data.add(address, DataType.MEMORY, 1.0 * (runtime.totalMemory() - runtime.freeMemory()))
                //                .addWatchable(getAddress(), DataType.MEMORY_PSS, 1.0 * Debug.getPss())
            )
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                LLog.d("Thread interrupted")
            }
        }
    }

    init {
        val pid = android.os.Process.myPid()
        topCmd = arrayOf("sh", "-c", "top -m 20 -d 1 -n 1 | grep \"$pid\" ")
        thread.start()
    }

    override fun close(source: String) {
        mayRun = false
        thread.interrupt()
        super.close(source)
    }
}
