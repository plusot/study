package com.plusot.bluelib.widget.graph

enum class AxesType {
    NO_AXIS,
    BOTH_VISIBLE,
    X_VISIBLE;

    val visible: Boolean
        get() = this != NO_AXIS

    val xVisible: Boolean
        get() = when (this) {
            NO_AXIS -> false
            else -> true
        }

    val yVisible: Boolean
        get() = when (this) {
            NO_AXIS, X_VISIBLE -> false
            else -> true
        }
}