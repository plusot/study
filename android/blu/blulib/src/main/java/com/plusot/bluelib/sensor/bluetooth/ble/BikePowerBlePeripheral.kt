package com.plusot.bluelib.sensor.bluetooth.ble;

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.toHex

class BikePowerBlePeripheral: BlePeripheral(GattAttribute.CYCLING_POWER_SERVICE.uuid) {
    private val measurementCharacteric = BluetoothGattCharacteristic(
        GattAttribute.CYCLING_POWER_MEASUREMENT.uuid,
        BluetoothGattCharacteristic.PROPERTY_NOTIFY,
        BluetoothGattCharacteristic.PERMISSION_READ
    )

    init {
        measurementCharacteric.addDescriptor(getClientCharacteristicConfigurationDescriptor())
        measurementCharacteric.addDescriptor(getCharacteristicUserDescriptionDescriptor("Characteristic for power measurement"))
        service.addCharacteristic(measurementCharacteric)

//        var characteristic = BluetoothGattCharacteristic(
//            GattAttribute.BODY_SENSOR_LOCATION.uuid,
//            BluetoothGattCharacteristic.PROPERTY_READ,
//            BluetoothGattCharacteristic.PERMISSION_READ
//        )
//        service.addCharacteristic(characteristic)
//
//        characteristic = BluetoothGattCharacteristic(
//            GattAttribute.HEART_RATE_CONTROL_POINT.uuid,
//            BluetoothGattCharacteristic.PROPERTY_WRITE,
//            BluetoothGattCharacteristic.PERMISSION_WRITE
//        )
//        service.addCharacteristic(characteristic)
    }

    override fun setValue(data: Data) {
        if (data.hasDataType(DataType.POWER)) {
            measurementCharacteric.value = GattAttribute.CYCLING_POWER_MEASUREMENT.write(null, data)
            currentValue[measurementCharacteric.uuid] = measurementCharacteric.value

            LLog.d("Set value ${measurementCharacteric.value.toHex()}") //, Globals.logHistory)
            serviceDelegate?.sendNotification(measurementCharacteric)
        }
    }

    override fun writeCharacteristic(characteristic: BluetoothGattCharacteristic, offset: Int, value: ByteArray): Int {
        if (offset != 0) return BluetoothGatt.GATT_INVALID_OFFSET
        if (value.size != 1) return BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH

        if (value[0].toInt() and 1 == 1) {
            LLog.d("Characteristic written: ${value.toHex()}")
        }
        return BluetoothGatt.GATT_SUCCESS
    }

    override fun notificationsDisabled(characteristic: BluetoothGattCharacteristic) {
        if (characteristic.uuid != GattAttribute.CYCLING_POWER_MEASUREMENT.uuid) return
        LLog.d("Notifications disabled")
    }

    override fun notificationsEnabled(characteristic: BluetoothGattCharacteristic, indicate: Boolean) {
        if (characteristic.uuid != GattAttribute.CYCLING_POWER_MEASUREMENT.uuid) return
        LLog.d("Notifications enabled, indicate = $indicate")
    }
}