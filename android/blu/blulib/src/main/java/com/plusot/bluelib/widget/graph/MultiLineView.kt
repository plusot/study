package com.plusot.bluelib.widget.graph

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.view.View
import androidx.annotation.StringRes
import com.plusot.bluelib.R
import com.plusot.bluelib.util.createIfNull
import com.plusot.bluelib.util.getColored
import java.util.*

class MultiLineView (
    context: Context?,
    private val baseColor: Int = 0,
    private val numberOfPoints: Int = 10000,
    private val autoScale: Boolean = true,
    private val minimalIterations: Int = 2,
    private val axesType: AxesType = AxesType.BOTH_VISIBLE,
    @StringRes xAxisLabel: Int? = null,
    @StringRes yAxisLabel: Int? = null,
    private var maxYVal: Float = Float.NEGATIVE_INFINITY,
    private var minYVal: Float = Float.POSITIVE_INFINITY,
    private var maxXVal: Float = Float.NEGATIVE_INFINITY,
    private var minXVal: Float = Float.POSITIVE_INFINITY
) : View(context), Graph {
    private val lineData = mutableMapOf<Int, Line>()
    private var hasFirstPoint = true

    private val backgroundArea = Line(
        fillAlpha = 1F
    )

    private val axis = if (axesType.visible && context != null) {
        val xLabel = if (xAxisLabel != null) context.getString(xAxisLabel) else null
        val yLabel = if (yAxisLabel != null) context.getString(yAxisLabel) else null
        Axes(context, xLabel, yLabel)
    } else
        null

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawAt(canvas, 0, 0, this.width, this.height, 5.0F)
    }

    override fun drawAt(
        canvas: Canvas,
        posX: Int,
        posY: Int,
        width: Int,
        height: Int,
        strokeWidth: Float
    ) {
        val border = 60f
        val w = width.toFloat()
        val h = height.toFloat()
        val minMax = MinMax(axesType.yVisible && hasFirstPoint)
        if (baseColor != 0) distributeColors()
        lineData.forEach { (_, line) ->
            if (line.size < minimalIterations) return@forEach
            line.minMax(minMax)
        }
        backgroundArea.minMax(minMax)

        if (minMax.isInValid) return

        val lenX = minMax.maxX - minMax.minX
        val lenY = minMax.maxY - minMax.minY
        if (autoScale) {
            maxXVal = minMax.maxX + freeSpace * lenX
            maxYVal = minMax.maxY + freeSpace * lenY
            minXVal = minMax.minX - freeSpace * lenX
            minYVal = minMax.minY - freeSpace * lenY
        }

        if (backgroundArea.size > 0) {
            backgroundArea.draw(
                canvas,
                minXVal,
                minYVal,
                maxXVal,
                maxYVal,
                posX.toFloat(),
                posY.toFloat(),
                w,
                h,
                border
            )
        }

        axis?.draw(
            canvas,
            axesType.yVisible && hasFirstPoint,
            minXVal,
            minYVal,
            maxXVal,
            maxYVal,
            posX.toFloat(),
            posY.toFloat(),
            w,
            h,
            border
        )

        lineData.forEach { (_, line) ->
            line.strokeWidth = strokeWidth
            line.draw(
                canvas,
                minXVal,
                minYVal,
                maxXVal,
                maxYVal,
                posX.toFloat(),
                posY.toFloat(),
                w,
                h,
                border
            )
        }
    }

    override val view: View
        get() = this

    private fun getLine(lineId: Int) = lineData.createIfNull(lineId) {
        Line().also {
            it.color = baseColor

        }
    }

    fun addValue(lineId: Int, x: Float, y: Float) {
        val line = if (lineId == -1) {
            backgroundArea
        } else
            getLine(lineId)
        line.addPoint(x, y)
        if (line.size > numberOfPoints + 1) {
            line.removeFirstPoint()
            hasFirstPoint = false
        }
        this.postInvalidate()
    }

    fun isNotEmpty(lineId: Int): Boolean {
        val line = if (lineId == -1) {
            backgroundArea
        } else
            getLine(lineId)
        return line.size > 0
    }

    fun setOffsetValue(lineId: Int, x: Float, y: Float) {
        val line = if (lineId == -1) {
            backgroundArea
        } else
            getLine(lineId)
        line.offset = x to y
        this.postInvalidate()
    }

    fun addValue(lineId: Int, y: Float) {
        val line = if (lineId == -1) {
            backgroundArea
        } else
            getLine(lineId)
        val (x0, _) = line.lastPoint ?: 0f to 0f
        line.addPoint((x0 + 1f), y)
        if (line.size > numberOfPoints + 1) {
            line.removeFirstPoint()
            hasFirstPoint = false
        }
        this.postInvalidate()
    }

    fun clear() {
        backgroundArea.clearPoints()
        lineData.forEach { (_, map) -> map.clearPoints() }
        this.postInvalidate()
    }

    fun clearLines() {
        lineData.forEach { (_, map) -> map.clearPoints() }
        this.postInvalidate()
    }

    fun backgroundClear() {
        backgroundArea.clearPoints()
    }

    private fun distributeColors() {
        val a = baseColor shr 24 and 0xff
        val r = baseColor shr 16 and 0xff
        val g = baseColor shr 8 and 0xff
        val b = baseColor and 0xff
        lineData.forEach { (key, value) ->
            value.color = argbToInt(
                a,
                (1f * r * (key + 1f) / (lineData.size + 1f)).toInt(),
                (1f * g * (key + 1f) / (lineData.size + 1f)).toInt(),
                (1f * b * (key + 1f) / (lineData.size + 1f)).toInt()
            )
        }
    }


//    fun setCubic(lineId: Int, isCubic: Boolean) {
//        val line = getLine(lineId)
//        line.isCubic = isCubic
//    }

    init {
        setWillNotDraw(false)
        setBackgroundColor(Color.TRANSPARENT)
        context?.let {
            backgroundArea.color = context.getColored(R.color.shady)
            id = NO_ID
        }
    }

    companion object {
        const val freeSpace = 0.05F
    }
}