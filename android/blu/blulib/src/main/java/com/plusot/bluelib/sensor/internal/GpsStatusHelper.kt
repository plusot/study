package com.plusot.bluelib.sensor.internal

import java.util.*

@Suppress("DEPRECATION")
class GpsStatusHelper(val event: Int, status: android.location.GpsStatus?) {
    val timeToFirstFix: Int = status?.timeToFirstFix ?: 0
    var satellites: Array<android.location.GpsSatellite>? = null
        private set

    init {
        val max = status?.maxSatellites ?: 0
        if (max > 0) {
            val list = ArrayList<android.location.GpsSatellite>()
            val it = status?.satellites?.iterator()
            if (it != null) while (it.hasNext()) {
                list.add(it.next())
            }
            if (list.size > 0) {
                satellites = list.toTypedArray()
            } else {
                satellites = null
            }
        } else
            satellites = null
    }

    override fun toString(): String {
        return satellites?.let {
            "GpsStatusHelper: " + it.fold("", { acc, sat -> "$acc ${sat.prn}"}) + ", event = " + event + ", timetofirstfix = " + timeToFirstFix
        } ?: "GpsStatusHelper: null, event = $event, timetofirstfix = $timeToFirstFix"
    }

}
