package com.plusot.bluelib.sensor.data

/**
 * Package: com.plusot.bluelib.sensor.data
 * Project: blu
 *
 * Created by Peter Bruinink on 2019-11-18.
 * Copyright © 2019 Plusot. All rights reserved.
 */

enum class PTSConnectPacketType(val id: Int) {
    serialNumberRequest(3),
    ;


}