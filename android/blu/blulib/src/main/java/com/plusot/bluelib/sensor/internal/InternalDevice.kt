package com.plusot.bluelib.sensor.internal

import com.plusot.bluelib.sensor.Device
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.util.proper

/**
 * Created by peet on 23/1/17.
 */

open class InternalDevice(protected val type: DeviceType, scannedDevice: ScannedDevice) : Device(scannedDevice) {

    override val address: String
        get() = type.toString().proper()


}
