package com.plusot.bluelib.sensor

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.hardware.Sensor
import android.os.Build
import com.plusot.bluelib.util.proper

import java.util.EnumSet

enum class DeviceType {
    TYPE_UNKNOWN,
    ANT_BIKE_POWER,
    ANT_CONTROLLABLE_DEVICE,
    ANT_FITNESS_EQUIPMENT,
    ANT_BLOOD_PRESSURE,
    ANT_GEOCACHE,
    ANT_ENVIRONMENT,
    ANT_WEIGHT_SCALE,
    ANT_HEARTRATE,
    ANT_BIKE_SPDCAD,
    ANT_BIKE_CADENCE,
    ANT_BIKE_SPD,
    ANT_STRIDE_SDM,
    BLUETOOTH_CLASSIC,
    BLUETOOTH_LE,
    BLUETOOTH_DUAL,
    BLUETOOTH_UNKNOWN,
    INTERNAL_GPS,
    INTERNAL_CLOCK,
    INTERNAL_BATTERY_SENSOR,
    INTERNAL_MEMORY_SENSOR,
    INTERNAL_ACCELEROMETER,
    INTERNAL_GEOMAGNETIC_FIELD,
    INTERNAL_ORIENTATION,
    INTERNAL_GYROSCOPE,
    INTERNAL_LIGHT,
    INTERNAL_PRESSURE,
    INTERNAL_TEMPERATURE,
    INTERNAL_PROXIMITY,
    INTERNAL_GRAVITY,
    INTERNAL_LINEAR_ACCELERATION,
    INTERNAL_ROTATION_VECTOR,
    INTERNAL_RELATIVE_HUMIDITY,
    INTERNAL_AMBIENT_TEMPERATURE,
    INTERNAL_MAGNETIC_FIELD_UNCALIBRATED,
    INTERNAL_GAME_ROTATION_VECTOR,
    INTERNAL_GYROSCOPE_UNCALIBRATED,
    INTERNAL_SIGNIFICANT_MOTION,
    INTERNAL_STEP_DETECTOR,
    INTERNAL_STEP_COUNTER,
    INTERNAL_GEOMAGNETIC_ROTATION_VECTOR,
    INTERNAL_HEART_RATE_MONITOR,
    INTERNAL_WAKE_UP_TILT_DETECTOR,
    INTERNAL_WAKE_GESTURE,
    INTERNAL_GLANCE_GESTURE,
    INTERNAL_PICK_UP_GESTURE,
    INTERNAL_WRIST_TILT_GESTURE,
    INTERNAL_DEVICE_ORIENTATION,
    INTERNAL_POSE_6DOF,
    INTERNAL_STATIONARY_DETECT,
    INTERNAL_MOTION_DETECT,
    INTERNAL_HEART_BEAT,
    INTERNAL_DYNAMIC_SENSOR_META,
    INTERNAL_LOW_LATENCY_OFFBODY_DETECT,
    INTERNAL_ACCELEROMETER_UNCALIBRATED,
    ;

    val isInternal: Boolean
        get() = when (this) {
            TYPE_UNKNOWN-> false
            ANT_BIKE_POWER-> false
            ANT_CONTROLLABLE_DEVICE-> false
            ANT_FITNESS_EQUIPMENT-> false
            ANT_BLOOD_PRESSURE-> false
            ANT_GEOCACHE-> false
            ANT_ENVIRONMENT-> false
            ANT_WEIGHT_SCALE-> false
            ANT_HEARTRATE-> false
            ANT_BIKE_SPDCAD-> false
            ANT_BIKE_CADENCE-> false
            ANT_BIKE_SPD-> false
            ANT_STRIDE_SDM-> false
            BLUETOOTH_CLASSIC-> false
            BLUETOOTH_LE-> false
            BLUETOOTH_DUAL-> false
            BLUETOOTH_UNKNOWN-> false
            INTERNAL_GPS-> true
            INTERNAL_CLOCK-> true
            INTERNAL_BATTERY_SENSOR-> true
            INTERNAL_MEMORY_SENSOR-> true
            INTERNAL_ACCELEROMETER -> true
            INTERNAL_GEOMAGNETIC_FIELD -> true
            INTERNAL_ORIENTATION -> true
            INTERNAL_GYROSCOPE -> true
            INTERNAL_LIGHT -> true
            INTERNAL_PRESSURE -> true
            INTERNAL_TEMPERATURE -> true
            INTERNAL_PROXIMITY -> true
            INTERNAL_GRAVITY -> true
            INTERNAL_LINEAR_ACCELERATION -> true
            INTERNAL_ROTATION_VECTOR -> true
            INTERNAL_RELATIVE_HUMIDITY -> true
            INTERNAL_AMBIENT_TEMPERATURE -> true
            INTERNAL_MAGNETIC_FIELD_UNCALIBRATED -> true
            INTERNAL_GAME_ROTATION_VECTOR -> true
            INTERNAL_GYROSCOPE_UNCALIBRATED -> true
            INTERNAL_SIGNIFICANT_MOTION -> true
            INTERNAL_STEP_DETECTOR -> true
            INTERNAL_STEP_COUNTER -> true
            INTERNAL_GEOMAGNETIC_ROTATION_VECTOR -> true
            INTERNAL_HEART_RATE_MONITOR -> true
            INTERNAL_WAKE_UP_TILT_DETECTOR -> true
            INTERNAL_WAKE_GESTURE -> true
            INTERNAL_GLANCE_GESTURE -> true
            INTERNAL_PICK_UP_GESTURE -> true
            INTERNAL_WRIST_TILT_GESTURE -> true
            INTERNAL_DEVICE_ORIENTATION -> true
            INTERNAL_POSE_6DOF -> true
            INTERNAL_STATIONARY_DETECT -> true
            INTERNAL_MOTION_DETECT -> true
            INTERNAL_HEART_BEAT -> true
            INTERNAL_DYNAMIC_SENSOR_META -> true
            INTERNAL_LOW_LATENCY_OFFBODY_DETECT -> true
            INTERNAL_ACCELEROMETER_UNCALIBRATED -> true
        }


    val isANT: Boolean
        get() = when (this) {
            TYPE_UNKNOWN-> false
            ANT_BIKE_POWER-> true
            ANT_CONTROLLABLE_DEVICE-> true
            ANT_FITNESS_EQUIPMENT-> true
            ANT_BLOOD_PRESSURE-> true
            ANT_GEOCACHE-> true
            ANT_ENVIRONMENT-> true
            ANT_WEIGHT_SCALE-> true
            ANT_HEARTRATE-> true
            ANT_BIKE_SPDCAD-> true
            ANT_BIKE_CADENCE-> true
            ANT_BIKE_SPD-> true
            ANT_STRIDE_SDM-> true
            else-> false
        }


    fun toSensorType(): Int? = when(this) {
        INTERNAL_GEOMAGNETIC_FIELD -> { Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR}
        INTERNAL_ORIENTATION -> {
            @Suppress("DEPRECATION")
            Sensor.TYPE_ORIENTATION
        }
        INTERNAL_GYROSCOPE -> { Sensor.TYPE_GYROSCOPE }
        INTERNAL_LIGHT -> { Sensor.TYPE_LIGHT}
        INTERNAL_PRESSURE -> { Sensor.TYPE_PRESSURE}
        INTERNAL_TEMPERATURE -> {
            @Suppress("DEPRECATION")
            Sensor.TYPE_TEMPERATURE
        }
        INTERNAL_PROXIMITY -> { Sensor.TYPE_PROXIMITY}
        INTERNAL_GRAVITY -> { Sensor.TYPE_GRAVITY}
        INTERNAL_LINEAR_ACCELERATION -> { Sensor.TYPE_LINEAR_ACCELERATION}
        INTERNAL_ROTATION_VECTOR -> { Sensor.TYPE_ROTATION_VECTOR}
        INTERNAL_RELATIVE_HUMIDITY -> { Sensor.TYPE_RELATIVE_HUMIDITY}
        INTERNAL_AMBIENT_TEMPERATURE -> { Sensor.TYPE_AMBIENT_TEMPERATURE}
        INTERNAL_MAGNETIC_FIELD_UNCALIBRATED -> { Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED}
        INTERNAL_GAME_ROTATION_VECTOR -> { Sensor.TYPE_GAME_ROTATION_VECTOR }
        INTERNAL_GYROSCOPE_UNCALIBRATED -> { Sensor.TYPE_GYROSCOPE_UNCALIBRATED}
        INTERNAL_SIGNIFICANT_MOTION -> { Sensor.TYPE_SIGNIFICANT_MOTION}
        INTERNAL_STEP_DETECTOR -> { Sensor.TYPE_STEP_DETECTOR}
        INTERNAL_STEP_COUNTER -> { Sensor.TYPE_STEP_COUNTER }
        INTERNAL_GEOMAGNETIC_ROTATION_VECTOR -> { Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR}
        INTERNAL_HEART_RATE_MONITOR -> { Sensor.TYPE_HEART_RATE}
//                    INTERNAL_WAKE_UP_TILT_DETECTOR -> { Sensor.TYPE_}
//                    INTERNAL_WAKE_GESTURE -> { Sensor.TYPE_}
//                    INTERNAL_GLANCE_GESTURE -> { Sensor.TYPE_}
//                    INTERNAL_PICK_UP_GESTURE -> { Sensor.TYPE_}
//                    INTERNAL_WRIST_TILT_GESTURE -> { Sensor.TYPE_}
//                    INTERNAL_DEVICE_ORIENTATION -> { Sensor.TYPE_}
        INTERNAL_POSE_6DOF -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sensor.TYPE_POSE_6DOF
            } else {
                28
            }
        }
        INTERNAL_STATIONARY_DETECT -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sensor.TYPE_STATIONARY_DETECT
            } else {
                29
            }
        }
        INTERNAL_MOTION_DETECT -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sensor.TYPE_MOTION_DETECT
            } else {
                30
            }
        }
        INTERNAL_HEART_BEAT -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sensor.TYPE_HEART_BEAT
            } else {
                31
            }
        }
//                    INTERNAL_DYNAMIC_SENSOR_META -> { Sensor.TYPE_}
        INTERNAL_LOW_LATENCY_OFFBODY_DETECT -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sensor.TYPE_LOW_LATENCY_OFFBODY_DETECT
            } else {
                34
            }
        }
        INTERNAL_ACCELEROMETER_UNCALIBRATED -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sensor.TYPE_ACCELEROMETER_UNCALIBRATED
            } else {
                35
            }
        }
        else -> null
    }

    @SuppressLint("InlinedApi")
    override fun toString(): String {
        @Suppress("DEPRECATION")
        return when (this) {
            ANT_BIKE_POWER ->  "ANT Bike Power"
            ANT_CONTROLLABLE_DEVICE ->  "ANT Controllable"
            ANT_FITNESS_EQUIPMENT ->  "ANT Fitness"
            ANT_BLOOD_PRESSURE ->  "ANT Blood Pressure"
            ANT_GEOCACHE ->  "ANT Geocache"
            ANT_ENVIRONMENT ->  "ANT Environment"
            ANT_WEIGHT_SCALE ->  "ANT Weight scale"
            ANT_HEARTRATE ->  "ANT Heart rate"
            ANT_BIKE_SPDCAD ->  "ANT Speed Cadence"
            ANT_BIKE_CADENCE ->  "ANT Cadence"
            ANT_BIKE_SPD ->  "ANT Speed"
            ANT_STRIDE_SDM ->  "ANT Stride"
            BLUETOOTH_CLASSIC ->  "Bluetooth Classic"
            BLUETOOTH_LE ->  "Bluetooth LE"
            BLUETOOTH_DUAL ->  "Bluetooth DUAL"
            BLUETOOTH_UNKNOWN ->  "Bluetooth"
            INTERNAL_GPS ->  "GPS"
            INTERNAL_CLOCK ->  "Clock"
            INTERNAL_BATTERY_SENSOR ->  "Battery sensor"
            INTERNAL_MEMORY_SENSOR ->  "Memory Sensor"
            INTERNAL_ACCELEROMETER ->  Sensor.STRING_TYPE_ACCELEROMETER
            INTERNAL_GEOMAGNETIC_FIELD ->  Sensor.STRING_TYPE_GEOMAGNETIC_ROTATION_VECTOR
            INTERNAL_ORIENTATION ->  Sensor.STRING_TYPE_ORIENTATION
            INTERNAL_GYROSCOPE ->  Sensor.STRING_TYPE_GYROSCOPE
            INTERNAL_LIGHT ->  Sensor.STRING_TYPE_LIGHT
            INTERNAL_PRESSURE ->  Sensor.STRING_TYPE_PRESSURE
            INTERNAL_TEMPERATURE ->  Sensor.STRING_TYPE_AMBIENT_TEMPERATURE
            INTERNAL_PROXIMITY ->  Sensor.STRING_TYPE_PROXIMITY
            INTERNAL_GRAVITY ->  Sensor.STRING_TYPE_GRAVITY
            INTERNAL_LINEAR_ACCELERATION ->  Sensor.STRING_TYPE_LINEAR_ACCELERATION
            INTERNAL_ROTATION_VECTOR ->  Sensor.STRING_TYPE_ROTATION_VECTOR
            INTERNAL_RELATIVE_HUMIDITY ->  Sensor.STRING_TYPE_AMBIENT_TEMPERATURE
            INTERNAL_AMBIENT_TEMPERATURE ->  Sensor.STRING_TYPE_AMBIENT_TEMPERATURE
            INTERNAL_MAGNETIC_FIELD_UNCALIBRATED ->  Sensor.STRING_TYPE_MAGNETIC_FIELD_UNCALIBRATED
            INTERNAL_GAME_ROTATION_VECTOR ->  Sensor.STRING_TYPE_GAME_ROTATION_VECTOR
            INTERNAL_GYROSCOPE_UNCALIBRATED ->  Sensor.STRING_TYPE_GYROSCOPE_UNCALIBRATED
            INTERNAL_SIGNIFICANT_MOTION ->  Sensor.STRING_TYPE_SIGNIFICANT_MOTION
            INTERNAL_STEP_DETECTOR ->  Sensor.STRING_TYPE_STEP_DETECTOR
            INTERNAL_STEP_COUNTER ->  Sensor.STRING_TYPE_STEP_COUNTER
            INTERNAL_GEOMAGNETIC_ROTATION_VECTOR ->  Sensor.STRING_TYPE_GEOMAGNETIC_ROTATION_VECTOR
            INTERNAL_HEART_RATE_MONITOR ->  Sensor.STRING_TYPE_HEART_RATE
            INTERNAL_POSE_6DOF ->  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Sensor.STRING_TYPE_POSE_6DOF
            } else {
                "android.sensor.pose_6dof"
            }
            INTERNAL_STATIONARY_DETECT->  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Sensor.STRING_TYPE_STATIONARY_DETECT
            } else {
                "android.sensor.stationary_detect"
            }
            INTERNAL_MOTION_DETECT->  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Sensor.STRING_TYPE_MOTION_DETECT
            } else {
                "android.sensor.motion_detect"
            }
            INTERNAL_HEART_BEAT->  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Sensor.STRING_TYPE_HEART_BEAT
            } else {
                "android.sensor.heart_beat"
            }
            INTERNAL_LOW_LATENCY_OFFBODY_DETECT ->  Sensor.STRING_TYPE_LOW_LATENCY_OFFBODY_DETECT
            INTERNAL_ACCELEROMETER_UNCALIBRATED ->  Sensor.STRING_TYPE_ACCELEROMETER_UNCALIBRATED
            TYPE_UNKNOWN ->  "UNKNOWN"
            else ->  "UNKNOWN"
        }   .replace("android.sensor.", "")
            .replace("_", " ")
            .proper()
    }

    companion object {

        fun fromBluetoothDeviceTypeInt(value: Int): DeviceType {
            when (value) {
                BluetoothDevice.DEVICE_TYPE_CLASSIC -> return BLUETOOTH_CLASSIC
                BluetoothDevice.DEVICE_TYPE_DUAL -> return BLUETOOTH_DUAL
                BluetoothDevice.DEVICE_TYPE_LE -> return BLUETOOTH_LE
                BluetoothDevice.DEVICE_TYPE_UNKNOWN -> return BLUETOOTH_UNKNOWN
                else -> return TYPE_UNKNOWN
            }
        }

        fun fromSensorType(value: Int): DeviceType? {
            values().forEach {
                if (it.toSensorType() == value) return it
            }
            return null
        }

        //    public static DeviceType fromAntType(com.dsi.ant.plugins.antplus.pcc.defines.DeviceType value) {
        //        switch (value) {
        //            case BIKE_POWER: return ANT_BIKE_POWER;
        //            case CONTROLLABLE_DEVICE: return ANT_CONTROLLABLE_DEVICE;
        //            case FITNESS_EQUIPMENT: return ANT_FITNESS_EQUIPMENT;
        //            case BLOOD_PRESSURE: return ANT_BLOOD_PRESSURE;
        //            case GEOCACHE: return ANT_GEOCACHE;
        //            case ENVIRONMENT: return ANT_ENVIRONMENT;
        //            case WEIGHT_SCALE: return ANT_WEIGHT_SCALE;
        //            case HEARTRATE: return ANT_HEARTRATE;
        //            case BIKE_SPDCAD: return ANT_BIKE_SPDCAD;
        //            case BIKE_CADENCE: return ANT_BIKE_CADENCE;
        //            case BIKE_SPD: return ANT_BIKE_SPD;
        //            case STRIDE_SDM: return ANT_STRIDE_SDM;
        //            default:
        //            case UNKNOWN: return TYPE_UNKNOWN;
        //        }
        //    }
        //
        //    public static DeviceType fromInternalType(InternalDevice.InternalType type) {
        //        switch(type) {
        //            case GPS:
        //                return INTERNAL_GPS;
        //            case CLOCK:
        //                return INTERNAL_CLOCK;
        //            case ACCELEROMETER:
        //                return INTERNAL_ACCELEROMETER;
        //            case MAGNETIC_SENSOR:
        //                return INTERNAL_MAGNETIC_SENSOR;
        //            case PROXIMITY_SENSOR:
        //                return INTERNAL_PROXIMITY_SENSOR;
        //            case BATTERY_SENSOR:
        //                return INTERNAL_BATTERY_SENSOR;
        //            case AIR_PRESSURE_SENSOR:
        //                return INTERNAL_AIR_PRESSURE_SENSOR;
        //            case ORIENTATION_SENSOR:
        //                return INTERNAL_ORIENTATION_SENSOR;
        //            case MEMORY_SENSOR:
        //                return INTERNAL_MEMORY_SENSOR;
        //            case TEMPERATURE_SENSOR:
        //                return INTERNAL_TEMPERATURE_SENSOR;
        //            case STEP_SENSOR:
        //                return INTERNAL_STEP_SENSOR;
        //            default:
        //                return TYPE_UNKNOWN;
        //        }
        //    }

        fun fromString(value: String?): DeviceType {
            if (value == null) return TYPE_UNKNOWN
            for (type in values()) {
                if (type.name.equals(value, ignoreCase = true)) return type
            }
            return TYPE_UNKNOWN
        }

        fun fromStrings(values: Array<String>?): EnumSet<DeviceType> {

            val set = EnumSet.noneOf(DeviceType::class.java)
            if (values == null) return set
            for (value in values) {
                val type = fromString(value)
                if (type != TYPE_UNKNOWN) set.add(type)
            }
            return set
        }
    }
}
