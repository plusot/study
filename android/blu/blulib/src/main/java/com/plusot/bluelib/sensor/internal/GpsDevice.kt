package com.plusot.bluelib.sensor.internal

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.*
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.preferences.PreferenceKey
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.*

/**
 * Package: com.plusot.bluelib.sensor.internal
 * Project: helloWatch
 *
 * Created by Peter Bruinink on 2019-05-20.
 * Copyright © 2019 Plusot. All rights reserved.
 */

class GpsDevice (scannedDevice: ScannedDevice, val useNmea: Boolean) : InternalDevice(DeviceType.INTERNAL_GPS, scannedDevice) {

    private var locationMgr: LocationManager? = null
    private var prevSatellites = 0
    private val gpsLocationListener = GpsLocationListener()
    private var nmeaAltitude: Double? = null
    private var nmeaRead: Long = 0L

    @RequiresApi(Build.VERSION_CODES.N)
    private val onNmeaListener = OnNmeaMessageListener{ nmea, timestamp -> handleNmeaString(nmea, timestamp) }

    @Suppress("DEPRECATION")
    private val nmeaListener = GpsStatus.NmeaListener { timestamp, nmea -> handleNmeaString(nmea, timestamp) }

    @Suppress("DEPRECATION")
    private val gpsStatusListener = GpsStatus.Listener { event ->
        val appContext = Globals.appContext ?: return@Listener
        val mgr = locationMgr ?: return@Listener

        if (ContextCompat.checkSelfPermission(
                appContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val status = mgr.getGpsStatus(null)
            when (event) {
                GpsStatus.GPS_EVENT_STARTED -> {
                }
                GpsStatus.GPS_EVENT_STOPPED -> {
                }
                GpsStatus.GPS_EVENT_FIRST_FIX -> {
                    if (debug) LLog.i("First fix")
                    val statusHelper = GpsStatusHelper(event, status)
                    //LLog.i("Satellite status " + parcel.toString());
                    statusHelper.satellites?.let {
                        fireData(Data().add(address, DataType.SATELLITES_VISIBLE, it.size))
                    }
                }
                GpsStatus.GPS_EVENT_SATELLITE_STATUS -> {

                    val statusHelper = GpsStatusHelper(event, status)
                    statusHelper.satellites?.let {
                        //LLog.i("Satellite status " + parcel.toString());
                        fireData(Data().add(address, DataType.SATELLITES_VISIBLE, it.size))
                    }

                }
            }

        }
    }

    private val gnssStatusListener = @RequiresApi(Build.VERSION_CODES.N) object: GnssStatus.Callback(){
        override fun onSatelliteStatusChanged(status: GnssStatus) {
            status.let { fireData(Data().add(address, DataType.SATELLITES_VISIBLE, it.satelliteCount)) }
            super.onSatelliteStatusChanged(status)
        }

        override fun onFirstFix(ttffMillis: Int) {
            if (debug) LLog.i("First fix")
            super.onFirstFix(ttffMillis)
        }

    }

    val lastLocation: Location?
        get() {
            if (prevLocation != null) return prevLocation
            val appContext = Globals.appContext ?: return null

            if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
            ) {
                LLog.i("No permission to access fine or coarse location")
                return null
            }
            locationMgr?.let { locationMgr ->
                var loc: Location? = locationMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (loc == null)
                    loc = locationMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
//                if (loc == null) {
//                    loc = Location(LocationManager.GPS_PROVIDER)
//                    loc.latitude = PreferenceKey.LAST_LOCATION_LAT.getDouble()
//                    loc.longitude = PreferenceKey.LAST_LOCATION_LONG.getDouble()
//                }
                return loc
            }
            return null
        }

    private enum class State (private val idLabel: Int) {
        PROVIDER_DISABLED(R.string.provider_disabled),
        PROVIDER_ENABLED(R.string.provider_enabled),
        PROVIDER_AVAILABLE(R.string.provider_available),
        PROVIDER_OUT_OF_SERVICE(R.string.provider_out_of_service),
        PROVIDER_TEMPORARILY_UNAVAILABLE(R.string.provider_temporarily_unavailable),
        SATELLITES_IN_VIEW(R.string.satellites_in_view),
        LOCATION_FIRST_FIX(R.string.location_first_fix);

        fun getLabel(context: Context): String {
            return context.getString(idLabel)
        }

        fun getLabel(context: Context?, vararg args: Any): String {
            if (context == null) {
                LLog.e("No context in Device.State.getLabel")
                return "unknown state"
            }
            return context.getString(idLabel, *args)
        }
    }

    override fun pauseDevice() {
        if (isActive) {
            if (debug) LLog.i("Closing GPS")
            //if (totalDistance > -1) PreferenceKey.TOTAL_DISTANCE.set(totalDistance)
            locationMgr?.also { locationMgr ->
                val appContext = Globals.appContext ?: return

                if (ContextCompat.checkSelfPermission(
                        appContext,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                        appContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    LLog.i("No permission to access fine or coarse location")
                    return
                }
                locationMgr.removeUpdates(gpsLocationListener)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    locationMgr.removeNmeaListener(onNmeaListener)
                    locationMgr.registerGnssStatusCallback(gnssStatusListener)
                } else {
//                    @Suppress("DEPRECATION")
//                    locationMgr.removeNmeaListener(nmeaListener)
                    @Suppress("DEPRECATION")
                    locationMgr.removeGpsStatusListener(gpsStatusListener)

                }
            }
        }
        super.pauseDevice()
    }

    private fun handleNmeaString(nmea: String?, timestamp: Long) {
        if (nmea == null) return
        if (debugNmea && !nmea.equals("", ignoreCase = true)) LLog.i(
            "nmea = " + nmea.replace(
                "\r\n",
                ""
            ) + " at " + TimeUtil.formatTime(timestamp, decimals = 3)
        )
        val parts = nmea.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (parts.isNotEmpty()) {
            val nmeaType = Nmea.fromString(parts[0])
            if (nmeaType != null) when (nmeaType) {
                Nmea.GPGSV -> if (parts.size > 4) {
                    if (debug) LLog.i("nmea Satellites raw: " + parts[3])
                    try {
                        val satellites = Numbers.parseInt(parts[3])
                        if (satellites > 0 && satellites != prevSatellites) {
                            if (debug) LLog.i("nmea Satellites: $satellites")
                            prevSatellites = satellites
                            fireData(Data().add(address, DataType.SATELLITES_VISIBLE, satellites))
                        }
                    } catch (e: NumberFormatException) {
                        LLog.e("nmea Invalid number: " + parts[3])
                    }

                }
                Nmea.GNGGA, Nmea.GPGGA -> if (parts.size > 13 && parts[1].isNotEmpty()) {
                    if (debug) LLog.i("nmea: " + nmea + " at " + TimeUtil.formatTime(timestamp))
                    var parse = parts[1]
                    val timeParts = parse.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (timeParts.size > 1) {
                        timeParts[1] = (timeParts[1] + "000").substring(0, 3)
                        parse = TimeUtil.formatTime(
                            pattern ="yyyy-MM-dd",
                            utc = true
                        ) + " " + timeParts[0] + '.'.toString() + timeParts[1]
                    } else {
                        parse = TimeUtil.formatTime(
                            pattern ="yyyy-MM-dd",
                            utc = true
                        ) + " " + timeParts[0] + ".000"
                    }
                    val time = TimeUtil.parseTimeUTC(parse, "yyyy-MM-dd HHmmss.S")

                    if (debug) LLog.i("nmea: " + parse + " -> Time = " + TimeUtil.formatTime(time, decimals = 3))
                    val quality = Numbers.parseInt(parts[6])
                    val data = Data().add(address, DataType.TIME_GPS, time)
                        .add(address, DataType.TIME_CLOCK, timestamp)
                        .add(address, DataType.TIME_ACCURACY, timestamp - System.currentTimeMillis())
                        .add(address, DataType.POSITION_QUALITY, quality)
                    if (parts[2].length > 3 && parts[4].length > 3) {
                        var lat = Numbers.parseInt(
                            parts[2].substring(
                                0,
                                2
                            )
                        ) + Numbers.parseDouble(parts[2].substring(2)) / 60
                        if (parts[3] == "S") lat *= -1.0
                        var lng = Numbers.parseInt(
                            parts[4].substring(
                                0,
                                3
                            )
                        ) + Numbers.parseDouble(parts[4].substring(3)) / 60
                        if (parts[5] == "W") lng *= -1.0
                        data.add(address, DataType.LOCATION_NMEA, doubleArrayOf(lat, lng))
                    }
                    val satellites = Numbers.parseInt(parts[7])
                    data.add(address, DataType.SATELLITES_FIX, satellites)

                    if (parts[8].isNotEmpty()) {
                        val hdop = +Numbers.parseDouble(parts[8])
                        data.add(address, DataType.HDOP, hdop)
                    }
                    if (parts[9].isNotEmpty() && parts[10].equals("M", ignoreCase = true)) {
                        val alt = +Numbers.parseDouble(parts[9])
                        data.add(address, DataType.ALTITUDE_NMEA, alt)
                        nmeaAltitude = alt
                        nmeaRead = System.currentTimeMillis()
                    }
                    if (parts[11].isNotEmpty() && parts[12].equals("M", ignoreCase = true)) {
                        val geoid = +Numbers.parseDouble(parts[11])
                        data.add(address, DataType.GEOID_HEIGHT, geoid)
                    }
                    data.add(address, DataType.NMEA, nmea)
                    fireData(data)
                }
            }
        }
    }

    override fun resumeDevice() {
        if (totalDistance < 0) totalDistance = PreferenceKey.TOTAL_DISTANCE.double
        if (locationMgr == null) locationMgr = Globals.appContext?.getSystemService(Context.LOCATION_SERVICE) as LocationManager? ?: return

        if (!isActive) {
            if (debug) LLog.i("Launching GPS")
            val appContext = Globals.appContext ?: return

            if (ContextCompat.checkSelfPermission(
                    appContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                    appContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                LLog.i("No permission to access fine or coarse location")
                return
            }

            val minDistance = gpsMinDistance
            SleepAndWake.runInMain {
                locationMgr?.let { locationMgr ->
                    try {
                        locationMgr.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            gpsInterval.toLong(), // minTime in ms
                            minDistance.toFloat(), // minDistance in meters
                            gpsLocationListener
                        )
                    } catch (e: IllegalArgumentException) {
                        try {
                            locationMgr.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                gpsInterval.toLong(), // minTime in ms
                                minDistance.toFloat(), // minDistance in meters
                                gpsLocationListener
                            )
                        } catch (e2: IllegalArgumentException) {
                            ToastHelper.showToastLong(R.string.no_location_device)
                            return@runInMain
                        }
                    }

                    if (useNmea) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            locationMgr.addNmeaListener(onNmeaListener)
                            locationMgr.registerGnssStatusCallback(gnssStatusListener)
                        } else {
//                            @Suppress("DEPRECATION")
//                            locationMgr.addNmeaListener(nmeaListener)
                            @Suppress("DEPRECATION")
                            locationMgr.addGpsStatusListener(gpsStatusListener)
                        }
                    }
                }
            }
        }
        super.resumeDevice()
    }

    private inner class GpsLocationListener : LocationListener {
        private var prevSlopeLocation: Location? = null
        private var slope = 0.0
        private var altitude = java.lang.Double.NaN
        private var altitudeCount = 0
        private var prevSlopeAltitude = java.lang.Double.NaN
        private var prevAscentAltitude = java.lang.Double.NaN
        private var distanceUpdate: Long = 0

        private fun handleLocationChanged(now: Long, location: Location) {
            val loc = doubleArrayOf(location.latitude, location.longitude)
            val data = Data().add(address, DataType.LOCATION, loc)
            gpsLastUpdate = now

            if (location.hasAltitude()) {
                var rawAltitude = location.altitude
                data.add(address, DataType.ALTITUDE_RAW, rawAltitude)
                nmeaAltitude?.let { if (now - nmeaRead < 30000) rawAltitude = it }
                if (java.lang.Double.isNaN(altitude)) {
                    altitude = rawAltitude
                    prevSlopeAltitude = altitude
                    prevSlopeLocation = location
                } else if (altitudeCount++ < 6) {
                    prevAscentAltitude = altitude
                    altitude =
                        altitude * (1.0 - DataType.ALTITUDE.floatAvgFactor()) + DataType.ALTITUDE.floatAvgFactor() * rawAltitude
                    prevSlopeAltitude = altitude
                    prevSlopeLocation = location
                } else {
                    altitude = altitude * (1.0 - DataType.ALTITUDE.floatAvgFactor()) + DataType.ALTITUDE.floatAvgFactor() * rawAltitude
                    val slopeDistance = location.distanceTo(prevSlopeLocation).toDouble()
                    if (slopeDistance > SLOPE_DISTANCE_DELTA) {
                        val deltaAltitude = altitude - prevSlopeAltitude
                        slope =
                            slope * (1.0 - DataType.SLOPE.floatAvgFactor()) + DataType.SLOPE.floatAvgFactor() * deltaAltitude / slopeDistance
                        data.add(address, DataType.SLOPE, slope)
                        prevSlopeAltitude = altitude
                        prevSlopeLocation = location
                    }
                    data.add(address, DataType.ALTITUDE, altitude)
                    if (java.lang.Double.isNaN(prevAscentAltitude)) prevAscentAltitude = altitude
                    val delta = altitude - prevAscentAltitude
                    if (delta > ASCENT_DELTA) {
                        if (java.lang.Double.isNaN(ascent)) ascent = 0.0
                        ascent += delta
                        data.add(address, DataType.ASCENT, ascent)

                    } else if (delta < -ASCENT_DELTA) {
                        if (java.lang.Double.isNaN(descent)) descent = 0.0
                        descent += delta
                        data.add(address, DataType.DESCENT, descent)
                    }

                    if (Math.abs(delta) > ASCENT_DELTA) prevAscentAltitude = altitude
                }
            }
            prevLocation?.let { prevLocation ->
                val deltaDistance = location.distanceTo(prevLocation).toDouble()
                speed = 1000 * deltaDistance / (location.time - prevLocation.time)
                val bearing = Calc.bearing(prevLocation, location)
                if (!java.lang.Double.isNaN(bearing)) data.add(address, DataType.BEARING, bearing)

                if (deltaDistance > 0) {
                    if (java.lang.Double.isNaN(distance)) distance = 0.0
                    distance += deltaDistance
                    data.add(address, DataType.DISTANCE, distance)
                    val time = ClockDevice.moving
                    if (time > 30000) data.add(address, DataType.SPEED_AVG, 1000.0 * distance / time)

                }

                if (deltaDistance > 0) {
                    totalDistance += deltaDistance
                    data.add(address, DataType.DISTANCE_TOTAL, totalDistance)
                    if (now - distanceUpdate > 60000) {
                        PreferenceKey.TOTAL_DISTANCE.double = totalDistance
                        distanceUpdate = now
                    }
                }
                data.add(address, DataType.SPEED, speed)
            }

            val extras = location.extras
            if (extras != null && extras.containsKey("satellites")) {
                atStateChanged(State.SATELLITES_IN_VIEW, extras.getInt("satellites"))
                data.add(address, DataType.SATELLITES_USED, extras.getInt("satellites"))
            }
            fireData(data)
            prevLocation = location

            //lastLocationTime = now;
        }


        override fun onLocationChanged(location: Location) {
            //LLog.i("GpsDevice Location changed: " + location);
            val now = System.currentTimeMillis() //location.getTime();
            if (Math.abs(location.longitude) > 0.01 && Math.abs(location.latitude) > 0.01) {
                SleepAndWake.runInNewThread() { handleLocationChanged(now, location) }
            }
        }

        private fun atStateChanged(state: State, vararg args: Any) {
            if (!debug) return
            val appContext = Globals.appContext ?: return
            LLog.i("StateChanged = " + state.getLabel(appContext, *args))
        }


        override fun onProviderDisabled(provider: String) {
            atStateChanged(State.PROVIDER_DISABLED, provider)
        }

        override fun onProviderEnabled(provider: String) {
            atStateChanged(State.PROVIDER_ENABLED, provider)
        }

        @Suppress("DEPRECATION")
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle?) {
            when (status) {
                LocationProvider.AVAILABLE ->
                    //					LLog.i("Status provider available");
                    atStateChanged(State.PROVIDER_AVAILABLE, provider)
                LocationProvider.OUT_OF_SERVICE -> {
                    if (debug) LLog.i("Status provider out of service")
                    atStateChanged(State.PROVIDER_OUT_OF_SERVICE, provider)
                }
                LocationProvider.TEMPORARILY_UNAVAILABLE -> {
                    if (debug) LLog.i("Status provider temporarily unavailable")
                    atStateChanged(State.PROVIDER_TEMPORARILY_UNAVAILABLE, provider)
                }
            }

            if (extras != null && extras.containsKey("satellites")) {
                //					LLog.i("Satellites: " + extras.getInt("satellites"));
                atStateChanged(State.SATELLITES_IN_VIEW, extras.getInt("satellites"))
                fireData(Data().add(address, DataType.SATELLITES_USED, extras.getInt("satellites")))
            }
        }
    }

    companion object {
        val debug = false
        var debugNmea = false
        var gpsInterval = 1000
        var gpsMinDistance = 0
        private var prevLocation: Location? = null
        private const val ASCENT_DELTA = 5.0
        private const val SLOPE_DISTANCE_DELTA = 10.0 //meter
        private var totalDistance = -1.0
        private var ascent = 0.0
        private var descent = 0.0
        private var distance = 0.0
        private var gpsLastUpdate: Long = -1
        var speed = 0.0
            private set


        fun available(): Boolean {
            val mgr = Globals.appContext?.getSystemService(Context.LOCATION_SERVICE) as LocationManager? ?: return false
            return mgr.getProviders(true).size > 0
        }

    }

}
