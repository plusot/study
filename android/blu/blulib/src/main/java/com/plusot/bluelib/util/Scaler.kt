package com.plusot.bluelib.util

interface Scaler {
    fun scale(value: Number): Number
    fun scaleBack(value: Number): Number
}
