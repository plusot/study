package com.plusot.bluelib.sensor

import android.hardware.SensorManager
import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.util.getColored

/**
 * Package: com.plusot.bluelib.sensor
 * Project: helloWatch
 *
 * Created by Peter Bruinink on 2019-05-12.
 * Copyright © 2019 Plusot. All rights reserved.
 */

enum class SensorAccuracy(val intValue: Int) {
    UNKNOWN(-2),
    NO_CONTACT(SensorManager.SENSOR_STATUS_NO_CONTACT),
    UNRELIABLE(SensorManager.SENSOR_STATUS_UNRELIABLE),
    ACCURACY_LOW(SensorManager.SENSOR_STATUS_ACCURACY_LOW),
    ACCURACY_MEDIUM(SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM),
    ACCURACY_HIGH(SensorManager.SENSOR_STATUS_ACCURACY_HIGH);

    val useIt: Boolean
        get() = when (this) {
            UNKNOWN -> false
            NO_CONTACT -> false
            UNRELIABLE -> false
            ACCURACY_LOW -> true
            ACCURACY_MEDIUM -> true
            ACCURACY_HIGH -> true
        }

    val color: Int?
        get() = when(this) {
            UNKNOWN -> Globals.appContext?.resources?.getColored(R.color.unknownColor)
            NO_CONTACT -> Globals.appContext?.resources?.getColored(R.color.errorColor)
            UNRELIABLE -> Globals.appContext?.resources?.getColored(R.color.warningColor)
            ACCURACY_LOW -> Globals.appContext?.resources?.getColored(R.color.lowColor)
            ACCURACY_MEDIUM -> Globals.appContext?.resources?.getColored(R.color.normalColor)
            ACCURACY_HIGH -> Globals.appContext?.resources?.getColored(R.color.excellentColor)
        }
}