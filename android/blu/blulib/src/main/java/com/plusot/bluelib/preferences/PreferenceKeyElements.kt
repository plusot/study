package com.plusot.bluelib.preferences

import java.util.Arrays

/**
 * Created by peet on 11-11-15.
 */
class PreferenceKeyElements(
    val label: String,
    private val defaultValue: Any?,
    private val fallBackValue: Any?,
    private val valueClass: Class<*>,
    val flags: Long
) {
    constructor(label: String, defaultValue: Any, valueClass: Class<*>, flags: Long) : this(
        label,
        defaultValue,
        null,
        valueClass,
        flags
    )

    private val defaultIntValue: Int
        get() = if (defaultValue is Int) {
            defaultValue
        } else
            0

    private val defaultBoolValue: Boolean
        get() = if (defaultValue is Boolean) {
            defaultValue
        } else
            false

    @Suppress("UNCHECKED_CAST")
    private val defaultSet: Set<String>?
        get() {
            if (defaultValue is Set<*>) {
                return try {
                    defaultValue as Set<String>?
                } catch (e: Exception) {
                    null
                }
            }
            return null
        }

    val fallbackStringValue: String?
        get() {
            if (fallBackValue is String) {
                return fallBackValue
            } else if (fallBackValue is Int) {
                return fallBackValue.toString()
            } else if (fallBackValue is Double) {
                return fallBackValue.toString()
            } else if (fallBackValue is Float) {
                return fallBackValue.toString()
            } else if (fallBackValue is Long) {
                return fallBackValue.toString()
            }
            return null
        }

    private val defaultStringValue: String
        get() {
            if (defaultValue is String) {
                return defaultValue
            } else if (defaultValue is Int) {
                return defaultValue.toString()
            } else if (defaultValue is Double) {
                return defaultValue.toString()
            } else if (defaultValue is Float) {
                return defaultValue.toString()
            } else if (defaultValue is Long) {
                return defaultValue.toString()
            }
            return ""
        }

    private val defaultDoubleValue: Double
        get() = if (defaultValue != null && defaultValue is Double) {
            defaultValue
        } else
            0.0

    private val defaultFloatValue: Float
        get() = if (defaultValue != null && defaultValue is Float) {
            defaultValue
        } else
            0.0F

    private val defaultLongValue: Long
        get() = if (defaultValue != null && defaultValue is Long) {
            defaultValue
        } else
            0L

    var string: String
        get() = PreferenceHelper.get(key, this.defaultStringValue)
        set(value) { PreferenceHelper.set(key, value) }

    val strings: Array<String>
        get() {
            val str = string
            return str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }

    val stringList: List<String>
        get() = Arrays.asList(*strings)

    @Suppress("UNCHECKED_CAST")
    val stringSet: Set<String>?
        get() = if (defaultValue is Set<*>) PreferenceHelper.getStringSet(
            this.toString(),
            defaultValue as Set<String>?
        ) else null

    var int: Int
        get() {
            if (valueClass == String::class.java || valueClass == String::class.javaPrimitiveType) return try {
                string.toInt()
            } catch (e: Exception) {
                0
            }
            if (valueClass != Int::class.java && valueClass != Int::class.javaPrimitiveType) return 0
            return if (flags and PreferenceDefaults.FLAG_ISARRAY > 0L && defaultValue is Int)
                PreferenceHelper.getFromListArray(key, defaultValue)
            else
                PreferenceHelper[key, this.defaultIntValue]
        }
        set(value) {
            if (flags and PreferenceDefaults.FLAG_ISARRAY > 0)
                PreferenceHelper.setFromListArray(key, value)
            else
                PreferenceHelper[key] = value
        }

    var long: Long
        get() = PreferenceHelper[key, this.defaultLongValue]
        set(value) { PreferenceHelper[key] = value }

    var double: Double
        get() = PreferenceHelper[key, this.defaultDoubleValue]
        set(value) {
            PreferenceHelper[key] = value
        }

    var float: Float
        get() = PreferenceHelper[key, this.defaultFloatValue]
        set(value) { PreferenceHelper[key] = value.toDouble() }

    var boolean: Boolean
        get() = PreferenceHelper[key, this.defaultBoolValue]
        set(value) { PreferenceHelper[key] = value }

    private val key: String
        get() = this.toString()

    override fun toString(): String {
        return label
    }

    fun get() {
        if (valueClass == Long::class.java) {
            PreferenceHelper.get(key, this.defaultLongValue)
        } else if (valueClass == Int::class.java) {
            if (flags and PreferenceDefaults.FLAG_ISARRAY > 0)
                PreferenceHelper.get(key, this.defaultStringValue)
            else
                PreferenceHelper.get(key, this.defaultIntValue)
        }
        if (valueClass == Int::class.javaPrimitiveType) {
            if (flags and PreferenceDefaults.FLAG_ISARRAY > 0)
                PreferenceHelper.get(key, this.defaultStringValue)
            else
                PreferenceHelper.get(key, this.defaultIntValue)
        } else if (valueClass == Boolean::class.java) {
            PreferenceHelper.get(key, this.defaultBoolValue)
        } else if (valueClass == String::class.java) {
            if (flags and PreferenceDefaults.FLAG_ISSTRINGSET > 0)
                PreferenceHelper.getStringSet(key, this.defaultSet)
            else
                PreferenceHelper.get(key, this.defaultStringValue)
        } else if (valueClass == Double::class.java) {
            PreferenceHelper.get(key, this.defaultDoubleValue)
        } else if (valueClass == Float::class.java) {
            PreferenceHelper.get(key, this.defaultFloatValue)
        } else if (valueClass == Set::class.java) {
            PreferenceHelper.getStringSet(key, this.defaultSet)
        }
    }


    fun getStringCheckUnset(unsetCheck: String): String? {
        val value = string
        return if (value.contains(unsetCheck)) fallbackStringValue else value
    }

    fun getString(index: Int): String? {
        return PreferenceHelper[key, index, this.defaultStringValue]
    }

    fun getString(defaultValue: String): String? {
        return PreferenceHelper[key, defaultValue]
    }

    fun getStringFromSet(index: Int): String? {
        return if (flags and PreferenceDefaults.FLAG_ISSTRINGSET == 0L) null else PreferenceHelper.getStringFromSet(
            this.toString(),
            index
        )
    }

    fun addStringToSet(value: String): Boolean {
        return if (flags and PreferenceDefaults.FLAG_ISSTRINGSET == 0L) false else PreferenceHelper.addToSet(
            this.toString(),
            value
        )
    }

    fun deleteStringFromSet(value: String) {
        if (flags and PreferenceDefaults.FLAG_ISSTRINGSET == 0L) return
        PreferenceHelper.deleteFromSet(this.toString(), value)
    }

    fun set(value: Int): Boolean {
        if (int == value) return false
        if (flags and PreferenceDefaults.FLAG_ISARRAY > 0)
            PreferenceHelper.setFromListArray(key, value)
        else
            PreferenceHelper[key] = value
        return true
    }

    fun set(value: Set<String>) {
        PreferenceHelper.setStringSet(key, value)
    }

    fun setString(index: Int, value: String) {
        PreferenceHelper[key, index] = value

    }

    fun setDefault() {
        @Suppress("UNCHECKED_CAST")
        when (valueClass) {
            Long::class.java -> long = defaultValue as Long
            Int::class.java -> int = defaultValue as Int
            Int::class.javaPrimitiveType -> int = defaultValue as Int
            String::class.java -> string = defaultValue as String
            Boolean::class.java -> boolean = defaultValue as Boolean
            Boolean::class.javaPrimitiveType -> boolean = defaultValue as Boolean
            Double::class.java -> double = defaultValue as Double
            Double::class.javaPrimitiveType -> double = defaultValue as Double
            Float::class.java -> float = defaultValue as Float
            Float::class.javaPrimitiveType -> float = defaultValue as Float
            Set::class.java -> set(defaultValue as Set<String>)
        }
    }
}
