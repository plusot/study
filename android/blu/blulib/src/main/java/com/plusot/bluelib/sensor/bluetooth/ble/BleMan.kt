package com.plusot.bluelib.sensor.bluetooth.ble

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import com.plusot.bluelib.Globals
import com.plusot.bluelib.BlueLib
import com.plusot.bluelib.R
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.*
import com.plusot.bluelib.sensor.bluetooth.ScannedBluetoothDevice
import com.plusot.bluelib.sensor.bluetooth.parseManufacturer
import com.plusot.bluelib.sensor.bluetooth.util.BleManufacturer
import com.plusot.bluelib.sensor.bluetooth.util.BleState
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.SleepAndWake
import com.plusot.bluelib.util.ToastHelper
import com.plusot.bluelib.util.runInMain
import com.plusot.bluelib.util.toTimeStr
import java.util.*

class BleMan: ScanManager {
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var mScanning: Boolean = false
    private var scannerAPI21: BluetoothLeScanner? = null
    private var nanoOffset: Long = 0
    private var stopper: SleepAndWake.Stopper? = null
    private var scanStartTime = 0L

    private val scannedDevices = mutableMapOf<String, ScannedBluetoothDevice>()
    private val unScannedDevices = mutableSetOf<String>()

    private var leScanCallback = BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
        parseScanResult(device, rssi, scanRecord, System.currentTimeMillis())
    }

    private inner class LeScanCallbackAPI21 : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            //LLog.d("Scan result")
            val now = System.currentTimeMillis()
            val nanoTime = result.timestampNanos / 1000000
            if (nanoOffset == 0L) nanoOffset = now - nanoTime
            val record = result.scanRecord
            if (record != null) parseScanResult(
                result.device,
                result.rssi,
                record.bytes,
                now
            )
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            //LLog.d("Scan results")
            val now = System.currentTimeMillis()
            for (result in results) {
                val nanoTime = result.timestampNanos / 1000000
                if (nanoOffset == 0L) nanoOffset = now - nanoTime

                val record = result.scanRecord
                if (record != null) parseScanResult(
                    result.device,
                    result.rssi,
                    record.bytes,
                    now
                )
            }
        }

        override fun onScanFailed(errorCode: Int) {
            LLog.i("BLE Scan failed. Reason: " + ScanFailure.fromId(errorCode))
            runInMain {
                ToastHelper.showToastLong(R.string.scanFailed)
            }
        }
    }

    private val leScanCallbackAPI21 = LeScanCallbackAPI21()

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    enum class ScanFailure (val id: Int) {
        NO_ERROR(0),
        SCAN_FAILED_ALREADY_STARTED(ScanCallback.SCAN_FAILED_ALREADY_STARTED),
        SCAN_FAILED_APPLICATION_REGISTRATION_FAILED(ScanCallback.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED),
        SCAN_FAILED_INTERNAL_ERROR(ScanCallback.SCAN_FAILED_INTERNAL_ERROR),
        SCAN_FAILED_FEATURE_UNSUPPORTED(ScanCallback.SCAN_FAILED_FEATURE_UNSUPPORTED),
        SCAN_FAILED_OUT_OF_HARDWARE_RESOURCES(5);

        companion object {
            fun fromId(id: Int): ScanFailure {
                for (failure in values()) {
                    if (failure.id == id) return failure
                }
                return NO_ERROR
            }
        }
    }

    init {
        Globals.appContext?.let { appContext ->
            val bluetoothManager = appContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?
            if (bluetoothManager != null) bluetoothAdapter = bluetoothManager.adapter
        }
        if (bluetoothAdapter == null || bluetoothAdapter?.isEnabled == false) {
            ToastHelper.showToastLong(R.string.bluetooth_not_enabled)
            val activity = Globals.visibleActivity
            if (activity != null) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
            //instance = null
        }

        val thread = Thread {
            while (Globals.runMode.isRun && bluetoothAdapter == null) Globals.appContext?.let { appContext ->
                    val bluetoothManager = appContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?
                    if (bluetoothManager != null) bluetoothAdapter = bluetoothManager.adapter
                }
                Thread.sleep(10000)
            }
        thread.start()
    }



    private fun parseScanResult(device: BluetoothDevice, rssi: Int, scanRecord: ByteArray, now: Long) {
        //        LLog.i("Parsing scan result");
        var sd = scannedDevices[device.address]
        val (manufacturerId, _) = parseManufacturer(scanRecord)
        if (sd == null && !Globals.verboseDevices && manufacturerId != 0x023D) {
            if (unScannedDevices.contains(device.address)) return
            val type = DeviceType.fromBluetoothDeviceTypeInt(device.type)
            val name = device.name
            if (name == null && (type == DeviceType.TYPE_UNKNOWN || type == DeviceType.BLUETOOTH_UNKNOWN)) {
                unScannedDevices.add(device.address)
                return
            }
            if (name != null) {
                if (name.toLowerCase(Locale.getDefault()).startsWith("[tv]")) {
                    unScannedDevices.add(device.address)
                    return
                }
                if (name.toLowerCase(Locale.getDefault()).contains("undefined")) {
                    unScannedDevices.add(device.address)
                    return
                }
            }
        }

        var isNew = false
        if (sd == null) {
            isNew = true
            sd = ScannedBluetoothDevice(device, now, true)
            scannedDevices[device.address] = sd
        }
        sd.incScanCount()
        sd.timeRead = now
        sd.deviceData.add(device.address, DataType.RSSI, rssi)
        sd.rssi = rssi

        sd.parseScanRecord(scanRecord)
        if (isNew && debug) LLog.i("New device found: " + device.address + " " + device.name) // + " " + sd.getData().toString());
        BlueLib.fireScanned(sd, isNew)

        val appContext = Globals.appContext ?: return
        val bluetoothManager = appContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?

        if (sd.isSelected) {
            if (sd.bleDevice == null) {
                stopScan("Connecting device")
                sd.bleDevice = BleDevice(sd, device)
            } else if (bluetoothManager != null &&
                (bluetoothManager.getConnectionState(
                    device,
                    BluetoothProfile.GATT
                ) == BluetoothProfile.STATE_DISCONNECTED ||
                sd.bleDevice?.state == BleState.DISCONNECTED)
            ) {
                stopScan("Reconnecting device")
                if (sd.reconnectCount > 4) {
                    LLog.i("ReconnectCount too high. Creating new BleDevice for $sd")
                    sd.bleDevice?.close("Reconnect count too high")
                    val sdFinal= sd
                    SleepAndWake.runInNewThread() { sdFinal.bleDevice = BleDevice(sdFinal, device) }
                    sd.reconnectCount = 0
//                } else if (sd.reconnectCount < 1 && sd.bleDevice?.reconnect(true) == true) {
//                    sd.reconnectCount++
//                    LLog.i("Trying to reconnect GATT for " + sd.name + ", reconnect count = " + sd.reconnectCount)
                } else {
                    sd.reconnectCount++
                    LLog.i("Device reconnect for " + sd.name + ", reconnect count = " + sd.reconnectCount)
                    sd.bleDevice?.connect()
                }

            }

        }
    }

    override val devices: List<ScannedDevice>
        get () = scannedDevices.values.toList()

    override fun stopScan(comment: String) {
//        LLog.d("Called stopScan from $comment while scanning = $mScanning")
        if (!mScanning) return
        stopper?.stop()
        stopper = null
        mScanning = false
        try {
            if (scannerAPI21 != null) {
                scannerAPI21?.stopScan(leScanCallbackAPI21)
            } else {
                @Suppress("DEPRECATION")
                bluetoothAdapter?.stopLeScan(leScanCallback)
            }
            BlueLib.fireScanning(ScanType.BLE_SCAN, false)
        } catch (e: IllegalStateException) {
            LLog.i("Could not stop BLE scan", e)
        }

    }

    override fun startScan(): Boolean {
        if (bluetoothAdapter == null) return false

        if (mScanning) return false
        if (!Globals.runMode.isRun) return false
        val now = System.currentTimeMillis()
        scanStartTime = now
//        LLog.d("Starting scan: ${now.toTimeStr()}")
        stopper = runInMain(BlueLib.scanPeriod) {
            if (now == scanStartTime) {
//                LLog.d("Stopping scan: ${now.toTimeStr()}")
                stopper = null
                stopScan("Stopper")
            }
        }
        mScanning = true
        scannerAPI21 = bluetoothAdapter?.bluetoothLeScanner
        val scanSettingsBuilder = ScanSettings.Builder().setReportDelay(0)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                scanSettingsBuilder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            } catch (e: IllegalArgumentException) {
                LLog.i("Could not set scan mode")
            }
            scanSettingsBuilder.setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
            scanSettingsBuilder.setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
        }
        if (scannerAPI21 == null) {
            BlueLib.fireScanning(ScanType.BLE_SCAN, true)
            @Suppress("DEPRECATION")
            bluetoothAdapter?.startLeScan(leScanCallback)
        } else {
            BlueLib.fireScanning(ScanType.BLE_SCAN, true)
            scannerAPI21?.startScan(null, scanSettingsBuilder.build(), leScanCallbackAPI21)
        }

        return true
    }

    override fun close() {
        LLog.i("Closing BleMan")

        for (sd in scannedDevices.values) sd.close("Close method of BleMan")
        scannedDevices.clear()

        if (bluetoothAdapter != null && bluetoothAdapter?.isDiscovering == true) bluetoothAdapter?.cancelDiscovery()

        if (debug) LLog.i("Closed BleMan")
    }

    companion object {
        const val REQUEST_ENABLE_BT = 8001
        const val debug = false

        private var instance: BleMan? = null

        fun getInstance(): BleMan = instance ?: synchronized(this) {
            BleMan().also {
                instance = it
            }
        }


    }


}
