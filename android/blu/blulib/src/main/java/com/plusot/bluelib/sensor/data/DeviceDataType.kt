package com.plusot.bluelib.sensor.data

import com.plusot.bluelib.sensor.ScannedDevice

class DeviceDataType(val device: ScannedDevice, val dataType: DataType) {
    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is DeviceDataType) return false
        return this.dataType == other.dataType && device.address == other.device.address
    }
    override fun hashCode(): Int = device.hashCode() + 31 * dataType.hashCode()
    override fun toString(): String = "${device.address} $dataType"
    fun toNiceString(): String = "$device $dataType"
    //fun toDataTypeString(): String = "$dataType"
}