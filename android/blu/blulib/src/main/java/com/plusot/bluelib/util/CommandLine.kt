package com.plusot.bluelib.util

import android.util.Log
import com.plusot.bluelib.util.SleepAndWake.runInNewThread
import java.io.*

object CommandLine {
    const val TAG = "wehkamp"

    fun execute(args: Array<String>) {
        runInNewThread {
            try {
                val process = Runtime.getRuntime().exec(args)
                val reader = BufferedReader(InputStreamReader(process.inputStream), 1024)
                var line: String?
                Log.d(TAG, args.fold("") { acc, element -> if (acc.isNotEmpty()) "$acc $element" else element })
                while (reader.readLine().also { line = it } != null) {
                    line?.let { ln -> Log.d(TAG, ln) }
                    Thread.yield()
                }
                reader.close()

            } catch (e: IOException) {
                Log.e(TAG, "IOException " + e.message)
            }
        }
    }

    fun execute(arg: String) {
        execute(arg.split(" ").toTypedArray())
    }

    fun execute(arg: String, delay: Long) {
        runInMain (delay) {  execute(arg.split(" ").toTypedArray()) }
    }
}