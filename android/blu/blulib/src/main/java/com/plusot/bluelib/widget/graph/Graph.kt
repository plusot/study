package com.plusot.bluelib.widget.graph

import android.graphics.Canvas
import android.view.View

interface Graph {
    fun drawAt(canvas: Canvas, posX: Int, posY: Int, width: Int, height: Int, strokeWidth: Float = 5.0F)
    val view: View
}