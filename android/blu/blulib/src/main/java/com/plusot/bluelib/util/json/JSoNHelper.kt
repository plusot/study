package com.plusot.bluelib.util.json

import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.json.JSoNHelper.decode
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.util.*

/**
 * Created by peet on 04-11-14.
 */
object JSoNHelper {


    class JSoNException : Exception {
        constructor(message: String, cause: Throwable) : super(message, cause) {}
        constructor(message: String) : super(message) {}
    }

    @Throws(UnsupportedEncodingException::class)
    fun decode(value: String): String {
        return URLDecoder.decode(value, "UTF-8")
    }

    fun fromString(value: String?): JSONObject {
        if (value == null || value.length == 0) return JSONObject()
        try {
            return JSONObject(decode(value))
        } catch (e: JSONException) {
            LLog.e("Could not parseCharacteristic: $value", e)
            return JSONObject()
        } catch (e: UnsupportedEncodingException) {
            LLog.e("Could not decode: $value", e)
            return JSONObject()
        }

    }

    fun getIntArray(json: JSONObject, key: String): Array<Int>? {
        try {
            val arr = json.getJSONArray(key)
            val vals = ArrayList<Int>()
            //if (arr != null)
            for (i in 0 until arr.length()) {
                val value: Int = arr.optInt(i, Integer.MIN_VALUE)
                if (value  > Integer.MIN_VALUE) {
                    vals.add(value)
                }
            }
            return vals.toTypedArray()
        } catch (e: JSONException) {
            LLog.e("JSONException " + e.message)
        }

        return null
    }

    @Throws(JSONException::class, JSoNException::class)
    private fun toString(json: JSONObject, key: String, obj: Any?): String {
        if (obj == null) throw JSoNException("Could not get: $key")
        if (obj is String) return obj
        if (obj is Double) return java.lang.Double.toString(json.getDouble(key))
        if (obj is Long) return java.lang.Long.toString(json.getLong(key))
        if (obj is Int) return Integer.toString(json.getInt(key))
        return if (obj is Boolean) java.lang.Boolean.toString(json.getBoolean(key)) else ""
    }

    @Throws(JSONException::class, JSoNException::class)
    private fun toString(json: JSONArray, i: Int, obj: Any?): String {
        if (obj == null) throw JSoNException("Could not get array element: $i")
        if (obj is String) return obj
        if (obj is Double) return java.lang.Double.toString(json.getDouble(i))
        if (obj is Long) return java.lang.Long.toString(json.getLong(i))
        if (obj is Int) return Integer.toString(json.getInt(i))
        return if (obj is Boolean) java.lang.Boolean.toString(json.getBoolean(i)) else ""
    }

    @Throws(JSoNException::class)
    fun fromJSONString(value: String): Map<String, String> {
        try {
            val json = JSONObject(JSoNHelper.decode(value))
            return fromJSON("", json)
        } catch (e: JSONException) {
            throw JSoNException("Could not parseCharacteristic: $value", e)
        } catch (e: UnsupportedEncodingException) {
            throw JSoNException("Could not decode: $value", e)
        }

    }

    @Throws(JSONException::class, JSoNException::class)
    private fun handleArray(key: String, arr: JSONArray, map: MutableMap<String, String>) {
        for (i in 0 until arr.length()) {
            val arrObj = arr.get(i)
            if (arrObj is JSONObject)
                map.putAll(fromJSON(key + '_'.toString() + i, arr.getJSONObject(i)))
            else if (arrObj is JSONArray) {
                val arr2 = arrObj.getJSONArray(i)
                handleArray(key + '_'.toString() + i, arr2, map)
            } else
                map[key] = toString(arr, i, arrObj)
        }
    }

    @Throws(JSoNException::class)
    private fun fromJSON(masterKey: String, json: JSONObject): Map<String, String> {
        val map = HashMap<String, String>()
        try {
            val keys = json.keys()
            while (keys.hasNext()) {
                val key = keys.next()
                val obj = json.get(key)
                if (obj is JSONObject)
                    map.putAll(fromJSON(masterKey + key, json.getJSONObject(key)))
                else if (obj is JSONArray) {
                    val arr = json.getJSONArray(key)
                    handleArray(masterKey + key, arr, map)
                } else
                    map[masterKey + key] = toString(json, key, obj)

            }
        } catch (e: JSONException) {
            throw JSoNException("Could not parseCharacteristic JSON", e)
        }

        return map

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: String): String {
        if (json == null) return defaultValue
        try {
            var string: String? = null
            if (json.has(key.toString())) string = json.getString(key.toString())
            return if (string == null || string.length == 0) defaultValue else string
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    fun getNullable(json: JSONObject?, key: JSoNField, defaultValue: String?): String? {
        if (json == null) return defaultValue
        try {
            var string: String? = null
            if (json.has(key.toString())) string = json.getString(key.toString())
            return if (string == null || string.length == 0) defaultValue else string
        } catch (e: JSONException) {
            return defaultValue
        }

    }


    operator fun get(json: JSONObject?, key: JSoNField): JSONArray? {
        if (json == null) return null
        try {
            var arr: JSONArray? = null
            if (json.has(key.toString())) arr = json.getJSONArray(key.toString())
            return if (arr == null || arr.length() == 0) null else arr
        } catch (e: JSONException) {
            return null
        }

    }

    fun getObj(json: JSONObject?, key: JSoNField): JSONObject? {
        if (json == null) return null
        try {
            var obj: JSONObject? = null
            if (json.has(key.toString())) obj = json.getJSONObject(key.toString())
            return if (obj == null || obj.length() == 0) null else obj
        } catch (e: JSONException) {
            return null
        }

    }

    fun getObj(json: JSONObject?, key: String): JSONObject? {
        if (json == null) return null
        try {
            var obj: JSONObject? = null
            if (json.has(key)) obj = json.getJSONObject(key)
            return if (obj == null || obj.length() == 0) null else obj
        } catch (e: JSONException) {
            return null
        }

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: Double): Double {
        if (json == null) return defaultValue
        try {
            return json.getDouble(key.toString())
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: Float): Float {
        if (json == null) return defaultValue
        try {
            return json.getDouble(key.toString()).toFloat()
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: DoubleArray): DoubleArray {
        if (json == null) return defaultValue
        try {
            val jArray = json.getJSONArray(key.toString())
            val values = DoubleArray(jArray.length())
            for (i in 0 until jArray.length()) {
                values[i] = jArray.getDouble(i)
            }
            return values
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: IntArray): IntArray {
        if (json == null) return defaultValue
        try {
            val jArray = json.getJSONArray(key.toString())
            val values = IntArray(jArray.length())
            for (i in 0 until jArray.length()) {
                values[i] = jArray.getInt(i)
            }
            return values
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: FloatArray): FloatArray {
        if (json == null) return defaultValue
        try {
            val jArray = json.getJSONArray(key.toString())
            val values = FloatArray(jArray.length())
            for (i in 0 until jArray.length()) {
                values[i] = jArray.getDouble(i).toFloat()
            }
            return values
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: Int): Int {
        if (json == null) return defaultValue
        try {
            return json.getInt(key.toString())
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONObject?, key: JSoNField, defaultValue: Long): Long {
        if (json == null) return defaultValue
        try {
            return json.getLong(key.toString())
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONArray?, key: Int, defaultValue: String): String {
        if (json == null) return defaultValue
        try {
            return json.getString(key)
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONArray?, key: Int, defaultValue: Double): Double {
        if (json == null) return defaultValue
        try {
            return json.getDouble(key)
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONArray?, key: Int, defaultValue: Int): Int {
        if (json == null) return defaultValue
        try {
            return json.getInt(key)
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun get(json: JSONArray?, key: Int, defaultValue: Long): Long {
        if (json == null) return defaultValue
        try {
            return json.getLong(key)
        } catch (e: JSONException) {
            return defaultValue
        }

    }

    operator fun set(json: JSONObject, key: JSoNField, value: Any) {
        set(json, key.toString(), value)
    }

    operator fun set(json: JSONObject, key: JSoNField, value: String) {
        set(json, key.toString(), value)
    }

    operator fun set(json: JSONObject, key: JSoNField, value: Long) {
        set(json, key.toString(), value)
    }

    operator fun set(json: JSONObject, key: DataType, value: Any) {
        set(json, key.name, value)
    }

    operator fun set(json: JSONObject, key: String, values: FloatArray) {
        val array = JSONArray()
        for (value in values) {
            try {
                array.put(value.toDouble())
            } catch (e: JSONException) {
                LLog.e("JSONException " + e.message)
            }

        }
        try {
            json.put(key.toLowerCase(), array)
        } catch (e: JSONException) {
            LLog.e("JSONException " + e.message)
        }

    }

    operator fun set(json: JSONObject, key: String, values: DoubleArray) {
        val array = JSONArray()
        for (value in values) {
            try {
                array.put(value)
            } catch (e: JSONException) {
                LLog.e("JSONException " + e.message)
            }

        }
        try {
            json.put(key, array)
        } catch (e: JSONException) {
            LLog.e("JSONException " + e.message)
        }

    }

    operator fun set(json: JSONObject, key: String, values: IntArray) {
        val array = JSONArray()
        for (value in values) {
            array.put(value)
        }
        try {
            json.put(key, array)
        } catch (e: JSONException) {
            LLog.e("JSONException " + e.message)
        }

    }

    operator fun set(json: JSONObject, key: String, values: ByteArray) {
        val array = JSONArray()
        for (value in values) {
            array.put(value.toInt())
        }
        try {
            json.put(key, array)
        } catch (e: JSONException) {
            LLog.e("JSONException " + e.message)
        }

    }

    operator fun set(json: JSONObject, key: String, values: LongArray) {
        val array = JSONArray()
        for (value in values) {
            array.put(value)
        }
        try {
            json.put(key, array)
        } catch (e: JSONException) {
            LLog.e("JSONException " + e.message)
        }

    }

    operator fun set(json: JSONObject, key: String, values: Array<String>) {
        val array = JSONArray()
        for (value in values) {
            array.put(value)
        }
        try {
            json.put(key, array)
        } catch (e: JSONException) {
            LLog.e("JSONException " + e.message)
        }

    }

    operator fun set(json: JSONObject, key: String, value: Long) {
        try {
            json.put(key, value)
        } catch (e: JSONException) {
            LLog.e("Could not convert $value to JSON object")
        }

    }

    operator fun set(json: JSONObject, key: String, value: String) {
        try {
            json.put(key, value)
        } catch (e: JSONException) {
            LLog.e("Could not convert $value to JSON object")
        }

    }

    operator fun set(json: JSONObject, key: String, value: Any) {
        (value as? FloatArray)?.let { set(json, key, it) } ?: ((value as? DoubleArray)?.let { set(json, key, it) }
            ?: ((value as? Array<*>)?.let { set(json, key, it) } ?: ((value as? IntArray)?.let {
                set(
                    json,
                    key,
                    it
                )
            } ?: ((value as? ByteArray)?.let { set(json, key, it) } ?: ((value as? LongArray)?.let {
                set(
                    json,
                    key,
                    it
                )
            } ?: try {
                json.put(key, value)
            } catch (e: JSONException) {
                LLog.e("Could not convert $value to JSON object")
            })))))

    }
}
