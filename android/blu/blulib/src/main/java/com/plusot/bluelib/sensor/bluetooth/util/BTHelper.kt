package com.plusot.bluelib.sensor.bluetooth.util

import android.bluetooth.BluetoothClass
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothProfile
import android.util.SparseArray
import com.plusot.bluelib.Globals
import com.plusot.bluelib.R
import com.plusot.bluelib.util.StringUtil
import com.plusot.bluelib.util.getUInt64

/**
 * Created by peet on 10-09-15.
 */
object BTHelper {

    var appearance: SparseArray<String>

    fun getName(device: BluetoothDevice): String {
        var name: String? = device.name

        if (name == null) {
            val appContext = Globals.appContext
            name = if (appContext == null)
                device.address
            else
                appContext.getString(R.string.unknown_device, device.address)
        }
        //        LLog.i(StringUtil.toReadableString(name, true));
        return name ?: "Unknown device"
    }

    fun hasName(device: BluetoothDevice): Boolean {
        return device.name != null
    }

    enum class BTState {
        NONE,
        CONNECTING,
        CONNECTED,
        DISCONNECTED,
        SENDING;


        companion object {

            fun fromInt(value: Int): BTState? {
                for (state in BTState.values()) {
                    if (state.ordinal == value) return state
                }
                return null
            }
        }
    }


    fun getAppearance(value: Int): String {
        return appearance.get(value) ?: return appearance.get(0)
    }

    init {
        appearance = SparseArray()
        appearance.put(0, "Unknown")
        appearance.put(64, "Generic Phone")
        appearance.put(128, "Generic Computer")
        appearance.put(192, "Generic Watch")
        appearance.put(193, "Watch: Sports Watch")
        appearance.put(256, "Generic Clock")
        appearance.put(320, "Generic Display")
        appearance.put(384, "Generic Remote Control")
        appearance.put(448, "Generic Eye-glasses")
        appearance.put(512, "Generic Tag")
        appearance.put(576, "Generic Keyring")
        appearance.put(640, "Generic Media Player")
        appearance.put(704, "Generic Barcode Scanner")
        appearance.put(768, "Generic Thermometer")
        appearance.put(769, "Thermometer: Ear")
        appearance.put(832, "Generic Heart rate Sensor")
        appearance.put(833, "Heart Rate Sensor: Heart Rate Belt")
        appearance.put(896, "Generic Blood Pressure")
        appearance.put(897, "Blood Pressure: Arm")
        appearance.put(898, "Blood Pressure: Wrist")
        appearance.put(960, "Human Interface Device (HID)")
        appearance.put(961, "Keyboard")
        appearance.put(962, "Mouse")
        appearance.put(963, "Joystick")
        appearance.put(964, "Gamepad")
        appearance.put(965, "Digitizer Tablet")
        appearance.put(966, "Card Reader")
        appearance.put(967, "Digital Pen")
        appearance.put(968, "Barcode Scanner")
        appearance.put(1024, "Generic Glucose Meter")
        appearance.put(1088, "Generic: Running Walking Sensor")
        appearance.put(1089, "Running Walking Sensor: In-Shoe")
        appearance.put(1090, "Running Walking Sensor: On-Shoe")
        appearance.put(1091, "Running Walking Sensor: On-Hip")
        appearance.put(1152, "Generic: Cycling")
        appearance.put(1153, "Cycling: Cycling Computer")
        appearance.put(1154, "Cycling: Speed Sensor")
        appearance.put(1155, "Cycling: Cadence Sensor")
        appearance.put(1156, "Cycling: Power Sensor")
        appearance.put(1157, "Cycling: Speed and Cadence Sensor")
        appearance.put(3136, "Generic: Pulse Oximeter")
        appearance.put(3137, "Fingertip")
        appearance.put(3138, "Wrist Worn")
        appearance.put(3200, "Generic: Weight Scale")
        appearance.put(5184, "Generic: Outdoor Sports Activity")
        appearance.put(5185, "Location Display Device")
        appearance.put(5186, "Location and Navigation Display Device")
        appearance.put(5187, "Location Pod")
        appearance.put(5188, "Location and Navigation Pod")

    }

    enum class BTTypeMinor private constructor(
        //        AUDIO_VIDEO_UNCATEGORIZED(BluetoothClass.Device.Major.AUDIO_VIDEO),
        //        WEARABLE_UNCATEGORIZED(BluetoothClass.Device.Major.WEARABLE),
        //        TOY_UNCATEGORIZED(BluetoothClass.Device.Major.TOY),
        //        HEALTH_UNCATEGORIZED(BluetoothClass.Device.Major.HEALTH),


        private val id: Int
    ) {
        MISC_UNCATEGORIZED(BluetoothClass.Device.Major.MISC),
        COMPUTER_UNCATEGORIZED(BluetoothClass.Device.COMPUTER_UNCATEGORIZED),
        COMPUTER_DESKTOP(BluetoothClass.Device.COMPUTER_DESKTOP),
        COMPUTER_SERVER(BluetoothClass.Device.COMPUTER_SERVER),
        COMPUTER_LAPTOP(BluetoothClass.Device.COMPUTER_LAPTOP),
        COMPUTER_HANDHELD_PC_PDA(BluetoothClass.Device.COMPUTER_HANDHELD_PC_PDA),
        COMPUTER_PALM_SIZE_PC_PDA(BluetoothClass.Device.COMPUTER_PALM_SIZE_PC_PDA),
        COMPUTER_WEARABLE(BluetoothClass.Device.COMPUTER_WEARABLE),

        PHONE_UNCATEGORIZED(BluetoothClass.Device.PHONE_UNCATEGORIZED),
        PHONE_CELLULAR(BluetoothClass.Device.PHONE_CELLULAR),
        PHONE_CORDLESS(BluetoothClass.Device.PHONE_CORDLESS),
        PHONE_SMART(BluetoothClass.Device.PHONE_SMART),
        PHONE_MODEM_OR_GATEWAY(BluetoothClass.Device.PHONE_MODEM_OR_GATEWAY),
        PHONE_ISDN(BluetoothClass.Device.PHONE_ISDN),

        NETWORKING_UNCATEGORIZED(BluetoothClass.Device.Major.NETWORKING),

        AUDIO_VIDEO_UNCATEGORIZED(BluetoothClass.Device.AUDIO_VIDEO_UNCATEGORIZED),
        AUDIO_VIDEO_WEARABLE_HEADSET(BluetoothClass.Device.AUDIO_VIDEO_WEARABLE_HEADSET),
        AUDIO_VIDEO_HANDSFREE(BluetoothClass.Device.AUDIO_VIDEO_HANDSFREE),
        AUDIO_VIDEO_MICROPHONE(BluetoothClass.Device.AUDIO_VIDEO_MICROPHONE),
        AUDIO_VIDEO_LOUDSPEAKER(BluetoothClass.Device.AUDIO_VIDEO_LOUDSPEAKER),
        AUDIO_VIDEO_HEADPHONES(BluetoothClass.Device.AUDIO_VIDEO_HEADPHONES),
        AUDIO_VIDEO_PORTABLE_AUDIO(BluetoothClass.Device.AUDIO_VIDEO_PORTABLE_AUDIO),
        AUDIO_VIDEO_CAR_AUDIO(BluetoothClass.Device.AUDIO_VIDEO_CAR_AUDIO),
        AUDIO_VIDEO_SET_TOP_BOX(BluetoothClass.Device.AUDIO_VIDEO_SET_TOP_BOX),
        AUDIO_VIDEO_HIFI_AUDIO(BluetoothClass.Device.AUDIO_VIDEO_HIFI_AUDIO),
        AUDIO_VIDEO_VCR(BluetoothClass.Device.AUDIO_VIDEO_VCR),
        AUDIO_VIDEO_VIDEO_CAMERA(BluetoothClass.Device.AUDIO_VIDEO_VIDEO_CAMERA),
        AUDIO_VIDEO_CAMCORDER(BluetoothClass.Device.AUDIO_VIDEO_CAMCORDER),
        AUDIO_VIDEO_VIDEO_MONITOR(BluetoothClass.Device.AUDIO_VIDEO_VIDEO_MONITOR),
        AUDIO_VIDEO_VIDEO_DISPLAY_AND_LOUDSPEAKER(BluetoothClass.Device.AUDIO_VIDEO_VIDEO_DISPLAY_AND_LOUDSPEAKER),
        AUDIO_VIDEO_VIDEO_CONFERENCING(BluetoothClass.Device.AUDIO_VIDEO_VIDEO_CONFERENCING),
        AUDIO_VIDEO_RESERVED(0x0444),
        AUDIO_VIDEO_VIDEO_GAMING_TOY(BluetoothClass.Device.AUDIO_VIDEO_VIDEO_GAMING_TOY),

        //        PERIPHERAL_UNCATEGORIZED                    (BluetoothClass.Device.Major.PERIPHERAL),
        PERIPHERAL_NON_KEYBOARD_NON_POINTING(0x0500),
        PERIPHERAL_KEYBOARD(0x0540),
        PERIPHERAL_POINTING(0x0580),
        PERIPHERAL_KEYBOARD_POINTING(0x05C0),

        IMAGING_UNCATEGORIZED(BluetoothClass.Device.Major.IMAGING),

        WEARABLE_UNCATEGORIZED(BluetoothClass.Device.WEARABLE_UNCATEGORIZED),
        WEARABLE_WRIST_WATCH(BluetoothClass.Device.WEARABLE_WRIST_WATCH),
        WEARABLE_PAGER(BluetoothClass.Device.WEARABLE_PAGER),
        WEARABLE_JACKET(BluetoothClass.Device.WEARABLE_JACKET),
        WEARABLE_HELMET(BluetoothClass.Device.WEARABLE_HELMET),
        WEARABLE_GLASSES(BluetoothClass.Device.WEARABLE_GLASSES),

        TOY_UNCATEGORIZED(BluetoothClass.Device.TOY_UNCATEGORIZED),
        TOY_ROBOT(BluetoothClass.Device.TOY_ROBOT),
        TOY_VEHICLE(BluetoothClass.Device.TOY_VEHICLE),
        TOY_DOLL_ACTION_FIGURE(BluetoothClass.Device.TOY_DOLL_ACTION_FIGURE),
        TOY_CONTROLLER(BluetoothClass.Device.TOY_CONTROLLER),
        TOY_GAME(BluetoothClass.Device.TOY_GAME),

        HEALTH_UNCATEGORIZED(BluetoothClass.Device.HEALTH_UNCATEGORIZED),
        HEALTH_BLOOD_PRESSURE(BluetoothClass.Device.HEALTH_BLOOD_PRESSURE),
        HEALTH_THERMOMETER(BluetoothClass.Device.HEALTH_THERMOMETER),
        HEALTH_WEIGHING(BluetoothClass.Device.HEALTH_WEIGHING),
        HEALTH_GLUCOSE(BluetoothClass.Device.HEALTH_GLUCOSE),
        HEALTH_PULSE_OXIMETER(BluetoothClass.Device.HEALTH_PULSE_OXIMETER),
        HEALTH_PULSE_RATE(BluetoothClass.Device.HEALTH_PULSE_RATE),
        HEALTH_DATA_DISPLAY(BluetoothClass.Device.HEALTH_DATA_DISPLAY),

        UNKNOWN(0xFFFF),
        UNCATEGORIZED(BluetoothClass.Device.Major.UNCATEGORIZED);


        companion object {

            fun fromInt(value: Int): BTTypeMinor {
                for (minor in BTTypeMinor.values()) if (minor.id == value) return minor
                return UNKNOWN
            }
        }
    }

}

enum class BleState(val state: Int) {
    DISCONNECTED(BluetoothProfile.STATE_DISCONNECTED),
    CONNECTING(BluetoothProfile.STATE_CONNECTING),
    CONNECTED(BluetoothProfile.STATE_CONNECTED),
    DISCONNECTING(BluetoothProfile.STATE_DISCONNECTING),
    UNKNOWN(-1);

    companion object {
        fun fromInt(value: Int): BleState {
            for (state in BleState.values()) {
                if (state.state == value) return state
            }
            return UNKNOWN
        }
    }
}

enum class BTTypeMajor(val intType: Int) {
    COMPUTER(BluetoothClass.Device.Major.COMPUTER),
    MISC(BluetoothClass.Device.Major.MISC),
    PHONE(BluetoothClass.Device.Major.PHONE),
    NETWORKING(BluetoothClass.Device.Major.NETWORKING),
    AUDIO_VIDEO(BluetoothClass.Device.Major.AUDIO_VIDEO),
    PERIPHERAL(BluetoothClass.Device.Major.PERIPHERAL),
    IMAGING(BluetoothClass.Device.Major.IMAGING),
    WEARABLE(BluetoothClass.Device.Major.WEARABLE),
    TOY(BluetoothClass.Device.Major.TOY),
    HEALTH(BluetoothClass.Device.Major.HEALTH),
    UNCATEGORIZED(BluetoothClass.Device.Major.UNCATEGORIZED);

    override fun toString(): String {
        return StringUtil.proper(name) ?: name
    }

    companion object {

        fun fromInt(type: Int): BTTypeMajor {
            for (major in BTTypeMajor.values()) {
                if (major.intType == type) return major
            }
            return UNCATEGORIZED
        }

        fun fromDevice(device: BluetoothDevice): BTTypeMajor = fromInt(device.bluetoothClass.majorDeviceClass)
    }
}


fun BluetoothGattCharacteristic.getUInt16(pos: Int = 0): Int {
    return this.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, pos) ?: 0
}

fun BluetoothGattCharacteristic.getUInt32(pos: Int = 0): Int {
    return this.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, pos) ?: 0
}

fun BluetoothGattCharacteristic.getUInt64(pos: Int = 0): Long {
    return this.value.getUInt64(pos)
}

fun BluetoothGattCharacteristic.getUInt8(pos: Int = 0): Int {
    return this.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, pos) ?: 0
}


