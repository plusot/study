package com.plusot.bluelib.sensor.bluetooth.ble

import android.bluetooth.*
import android.os.Build
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.Device
import com.plusot.bluelib.sensor.bluetooth.ScannedBluetoothDevice
import com.plusot.bluelib.sensor.bluetooth.util.BTHelper
import com.plusot.bluelib.sensor.bluetooth.util.BleState
import com.plusot.bluelib.sensor.data.CommandType
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.*
import java.lang.reflect.Field
import java.util.*
import java.util.concurrent.LinkedBlockingQueue

enum class BleRWSuccess {
    FATAL,
    FAILURE,
    SUCCESS
}
typealias GattCallListener = (BleRWSuccess, String) -> Unit


class BleDevice constructor(
    scannedDevice: ScannedBluetoothDevice,
    private val device: BluetoothDevice
) : Device(scannedDevice) {
    private var gatt: BluetoothGatt? = null
    private val notifications = HashMap<GattAttribute, Boolean>()
    private val indications = HashMap<GattAttribute, Boolean>()
    private val listLock = Object()
    private val callQueue = LinkedBlockingQueue<GattCall>()
    private val callLock = Object()
    var state = BleState.DISCONNECTED
        private set
    private var mayRun = true
    private var runner: Thread? = null
    private val gattCallback: BluetoothGattCallback
    private val characteristics = HashMap<GattAttribute, BluetoothGattCharacteristic>()
    private var deviceBusyField: Field? = null
    private var mtuStopper: SleepAndWake.Stopper? = null

    var lastRead = 0L
        private set

    private val deviceBusy: Boolean?
        get() {
            if (gatt == null) return null
            deviceBusyField?.let {
                try {
                    val obj = it.get(gatt)
                    if (obj is Boolean) {
                        return obj
                    }
                } catch (e: IllegalAccessException) {
                    LLog.e("Could not access mDeviceBusy field of BluetoothGatt", e)
                }
            }

            return null
        }

    override val address: String
        get() = device.address

    val name: String
        get() = scannedDevice.name //BTHelper.getName(device)



    internal abstract inner class GattCall(val listener: GattCallListener?) {
        abstract fun fireGatt()

    }

    private inner class CharacteristicReadCall (
        val characteristic: BluetoothGattCharacteristic,
        listener: GattCallListener? = null
    ): GattCall(listener) {

        override fun fireGatt() {
            //LLog.i("Firing read: " + GattAttribute.fromCharacteristic(characteristic));
            readCharacteristic(characteristic, listener)
        }
    }

    private inner class CharacteristicWriteCall (
        internal val characteristic: BluetoothGattCharacteristic,
        internal val bytes: ByteArray,
        internal val writeType: Int,
        listener: GattCallListener
    ): GattCall(listener) {

        override fun fireGatt() {
            writeCharacteristic(characteristic, bytes, writeType, listener)
        }
    }

    private inner class NotificationCall internal constructor(internal val characteristic: BluetoothGattCharacteristic, internal val enable: Boolean) :
        GattCall(null) {

        override fun fireGatt() {
            val attr = GattAttribute.fromCharacteristic(characteristic)

            gatt?.let {g ->
                if (state == BleState.CONNECTED && g.setCharacteristicNotification(characteristic, enable)) { // && attr.hasDescriptor()) {
                    val descriptor = characteristic.getDescriptor(GattAttribute.CLIENT_CHARACTERISTIC_CONFIGURATION.uuid)
                    if (descriptor != null) {
                        if (enable)
                            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                        else
                            descriptor.value = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
//                        val result =
                        g.writeDescriptor(descriptor)
//                        LLog.i("Preparing notifications ${enable.toEnum<OnOff>()} for $attr = ${result.toEnum<Success>()}")
                        runInMain(4000) {
                            if (gatt != null
                                && state == BleState.CONNECTED
                                && Globals.runMode.isRun && notifications[attr] != enable) synchronized(callLock) {
                                callQueue.offer(NotificationCall(characteristic, true))
                            }
                        }
                    }
                }
            }
        }
    }



    private inner class IndicationCall internal constructor(internal val characteristic: BluetoothGattCharacteristic, internal val enable: Boolean) : GattCall(null) {
        override fun fireGatt() {
            val attr = GattAttribute.fromCharacteristic(characteristic)

            gatt?.let { g ->
                if (state == BleState.CONNECTED && g.setCharacteristicNotification(characteristic, enable)) {
                    val descriptor = characteristic.getDescriptor(GattAttribute.CLIENT_CHARACTERISTIC_CONFIGURATION.uuid)
                    if (descriptor != null) {
                        descriptor.value = if (enable)
                            BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
                        else
                            BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
//                        val result =
                        g.writeDescriptor(descriptor)
                        runInMain(4000) {
                            if (gatt != null
                                && state == BleState.CONNECTED
                                && Globals.runMode.isRun && indications[attr] != enable) synchronized(callLock) {
                                callQueue.offer(IndicationCall(characteristic, true))
                            }
                        }
//                        LLog.i("Preparing indications ${enable.toEnum<OnOff>()} for $attr = ${result.toEnum<Success>()}");
                    }
                }
            }
        }
    }

    init {
        gattCallback = object : BluetoothGattCallback() {
            override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
                if (gatt.device.address != device.address) LLog.d("Wrong gatt device: ${BTHelper.getName(gatt.device)} for $name ${gatt.device.address} ")
                //                LLog.i("Connection state changed: " + $name);
                state = BleState.fromInt(newState)
                when (newState) {
                    BluetoothProfile.STATE_CONNECTING -> { }
                    BluetoothProfile.STATE_CONNECTED -> {
                        //                        LLog.i("$name Connected to GATT server.");
                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            if (requestMtu) {
                                gatt.requestMtu(150)
                                mtuStopper = runInMain(1500) {
                                    mtuStopper = null
                                    gatt.discoverServices()
                                }
                            } else
                                gatt.discoverServices()
                            //LLog.i("$name Attempting to start service discovery:" + result);
                        }
                        this@BleDevice.fireState(StateInfo.CONNECTED)
                    }
                    BluetoothProfile.STATE_DISCONNECTING -> LLog.i("$name disconnecting from GATT server.")
                    BluetoothProfile.STATE_DISCONNECTED -> {

                        LLog.i("$name ${gatt.device.address} disconnected from GATT server.")
                        synchronized(listLock) {
                            notifications.clear()
                            indications.clear()
                        }
                        this@BleDevice.fireState(StateInfo.DISCONNECTED)
                    }
                    else -> LLog.i("New state: $newState")
                }
            }
            override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                if (gatt.device.address != device.address) LLog.d("Wrong gatt device: ${BTHelper.getName(gatt.device)} for $name ${gatt.device.address} ")
                LLog.i("${gatt.device.address} Services discovered: $name ${gatt.device.address} with status ${GattState.fromId(status)} ($status)")
                if (status == BluetoothGatt.GATT_SUCCESS) SleepAndWake.runInMain("BleDevice.onServicesDiscovered", GATT_WAIT){
                    for (gattService in gatt.services) {
                        val serviceUuid = gattService.uuid
                        val service = GattAttribute.fromUUID(serviceUuid)
                        if (service == GattAttribute.UNKNOWN) {
                            LLog.i("${gatt.device.address} Found unknown gatt service: $serviceUuid")
                            continue
                        }

                        if (!service.isSupported) {
                            LLog.i("${gatt.device.address} Skipping verbose service: $service")
                            continue
                        }

                        LLog.i("${gatt.device.address} Discovered service = $service")
                        if (mayRun)
                            for (characteristic in gattService.characteristics) {
                                val attr = GattAttribute.fromCharacteristic(characteristic)
                                LLog.i("${gatt.device.address} Discovered characteristic $attr, ${characteristic.instanceId.toHex()}")
                                if (attr == GattAttribute.UNKNOWN) {
                                    LLog.i("Found unknown characteristic: ${characteristic.uuid}, ${characteristic.instanceId.toHex()}")
                                    continue
                                }

                                if (!attr.isSupported) {
                                    LLog.i("Skipping characteristic: $attr, ${characteristic.instanceId.toHex()}")
                                    continue
                                }

                                characteristics[attr] = characteristic
                                handles["${address}_$attr"] = characteristic.instanceId
                                val properties = characteristic.properties
                                if (attr.needsInit()) {
                                    LLog.i("Checking inits for $attr")
                                    if (properties and BluetoothGattCharacteristic.PROPERTY_WRITE == 0) {
                                        LLog.e("Property write not found!")
                                    } else {
                                        LLog.i("Adding inits for $attr to callQueue")
                                        val inits = attr.commands(CommandType.INIT)
                                        if (inits != null) for (init in inits) {
                                            callQueue.offer(
                                                CharacteristicWriteCall(
                                                    characteristic,
                                                    init.t1(),
                                                    attr.writeType
                                                ) { success, reason -> LLog.d("Write of ${init.t2()} = $success, reason: $reason") }
                                            )
                                        }
                                    }
                                }
                                if (properties and BluetoothGattCharacteristic.PROPERTY_NOTIFY > 0) {
                                    //LLog.i("Adding notification call for " + attribute);
                                    fireData(Data.fromDataTypes(attr.dataTypes))
                                    runInMain(100) {callQueue.offer(NotificationCall(characteristic, true)) }
                                    notifications[attr] = false
                                }
                                if (properties and BluetoothGattCharacteristic.PROPERTY_INDICATE > 0) {
                                    //LLog.i("Adding indication call for " + attribute);
                                    fireData(Data.fromDataTypes(attr.dataTypes))
                                    runInMain(100) { callQueue.offer(IndicationCall(characteristic, true))}
                                    indications[attr] = false
                                }
                                if (properties and BluetoothGattCharacteristic.PROPERTY_READ > 0) {
                                    //LLog.i("Adding read call for " + attribute);
                                    fireData(Data.fromDataTypes(attr.dataTypes))
                                    if (autoRead) callQueue.offer(CharacteristicReadCall(characteristic, null))
                                    fireData(Data(address).add(DataType.GATT_ATTRIBUTE_DISCOVERED, attr.ordinal))
                                }
                            }
                    }
                    synchronized(callLock) { callLock.notifyAll() }
                } else {
                    LLog.i("Received: $status")
                }
            }

            override fun onCharacteristicRead(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int
            ) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    lastRead = System.currentTimeMillis()
                    fire(gatt, characteristic)
                } else {
                    LLog.i(
                        "Characteristic received status: ${GattState.fromId(status)} (${status.toHex()}) for ${GattAttribute.fromCharacteristic(characteristic)}"
                    )
                    fireState(StateInfo.WARNING, Data(address).add(DataType.GATT_STATUS, status))
                }
                synchronized(callLock) { callLock.notifyAll() }
            }

            override fun onCharacteristicWrite(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int
            ) {
                val attr = GattAttribute.fromCharacteristic(characteristic)
//                LLog.i("Characteristic written: $attr / Notified")
                if (mayRun && attr.readAfterWrite && (characteristic.properties and BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                    callQueue.offer(CharacteristicReadCall(characteristic))
                    attr.characteristicToReadAfter()?.also { gattAttribute ->
                        characteristics[gattAttribute]?.let { callQueue.offer(CharacteristicReadCall(it)) }
                    }
                }
                synchronized(callLock) { callLock.notifyAll() }
                super.onCharacteristicWrite(gatt, characteristic, status)
            }

            override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
//                LLog.i("Characteristic changed: ${GattAttribute.fromCharacteristic(characteristic)}");
                fire(gatt, /* GattAction.ACTION_DATA_AVAILABLE,*/ characteristic)
                synchronized(callLock) { callLock.notifyAll() }
            }

            override fun onDescriptorRead(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
                //                LLog.i("Descriptor read: " + descriptor.getUuid() + " / Notified");
                synchronized(callLock) { callLock.notifyAll() }
            }

            override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
                val attr = GattAttribute.fromCharacteristic(descriptor.characteristic)
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    synchronized(listLock) {
                        if (indications[attr] != null) {
                            val enabled = descriptor.value?.contentEquals(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE)
                            LLog.i("${gatt.device.address} Indications for $attr = ${enabled?.toEnum<OnOff>()}")
                            if (enabled != null) indications[attr] = enabled
                        } else if (notifications[attr] != null) {
                            val enabled = descriptor.value?.contentEquals(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                            LLog.i("${gatt.device.address} Notifications for $attr = ${enabled?.toEnum<OnOff>()}")
                            if (enabled != null) notifications[attr] = enabled
                        } else
                            LLog.i("${gatt.device.address} Descriptor write: ${descriptor.uuid}, ${attr}: ${GattState.fromId(status)} (${status.toHex()}")

                    }
                } else {
                    LLog.i("${gatt.device.address} Descriptor write: ${descriptor.uuid}, ${attr}: ${GattState.fromId(status)} (${status.toHex()}")
                }
                synchronized(callLock) { callLock.notifyAll() }
            }

            override fun onMtuChanged(gatt: BluetoothGatt, mtu: Int, status: Int) {
                if (mtuStopper != null) mtuStopper?.stop()
                mtuStopper = null
                gatt.discoverServices()
                LLog.i("Mtu changed to $mtu, success = ${GattState.fromId(status)}")
                synchronized(callLock) { callLock.notifyAll() }
            }

            override fun onReadRemoteRssi(gatt: BluetoothGatt, rssi: Int, status: Int) {
                LLog.i("Read remote Rssi: $rssi / Notified")
                synchronized(callLock) { callLock.notifyAll() }
            }

            override fun onReliableWriteCompleted(gatt: BluetoothGatt, status: Int) {
                LLog.i("Reliable write completed. / Notified")
                synchronized(callLock) { callLock.notifyAll() }
            }

        }
        //        LLog.i("Trying to connect Gatt for " + BTHelper.getName(device));

        runner = Thread(Runnable {
            var busyCount = 0

            while (Globals.runMode.isRun && /*state != BluetoothProfile.STATE_DISCONNECTED && */ mayRun) try {
                if (state == BleState.CONNECTED) {
                    val busy = deviceBusy
                    if (busy == true) {
                        busyCount++
                        if (busyCount > 20) {
                            callQueue.peek()?.listener?.invoke(BleRWSuccess.FATAL, "Gatt busy")
                            LLog.d("Gatt busy")
                            busyCount = 0
                            close("Gatt busy too long")
                        }
                    } else {
                        busyCount = 0
                        callQueue.take().fireGatt()
                    }
                }
                synchronized(callLock) {
                    val longAgo = System.currentTimeMillis()
                    callLock.wait(THREAD_TIMEOUT)
                    val w = System.currentTimeMillis() - longAgo
                    //if (w < THREAD_TIMEOUT)
                    //LLog.i("Waking " + runner.getName() + " after " + w + "ms");
                    if (w < GATT_WAIT) Thread.sleep(GATT_WAIT - w)
                }
            } catch (e: InterruptedException) {
                LLog.i("Runner interrupted")
            }
        })
        runner?.name = "Runner " + runner?.id
        runner?.start()

        connect()
    }

    fun connect() {
        LLog.d("Trying to connect $name ${device.address}")
        gatt?.close()
        val appContext = Globals.appContext ?: return

        gatt = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !name.startsWith("Ticwatch", false)) {
            device.connectGatt(appContext, false, gattCallback, BluetoothDevice.TRANSPORT_LE)
        } else {
            device.connectGatt(appContext, false, gattCallback)
        }
//            gatt = device.connectGatt(appContext, false, gattCallback) original version!
        try {
            deviceBusyField = BluetoothGatt::class.java.getDeclaredField("mDeviceBusy")
            deviceBusyField?.isAccessible = true
        } catch (e: NoSuchFieldException) {
            LLog.e("Could not access mDeviceBusy field of BluetoothGatt", e)
        }
    }

    fun reconnect(force: Boolean): Boolean {
        LLog.d("Bond state = $state, ${gatt?.device?.bondState?.toBondState() ?: BondState.BOND_UNKNOWN} ${gatt?.readRemoteRssi()}")
        if (!force && state == BleState.CONNECTED) return true
        gatt?.let {
            LLog.i("Called reconnect for $name ${gatt?.device?.address ?: ""} ")
            return it.connect()
        } ?: return false
    }

    fun indicationsSet(): Boolean = synchronized(listLock){
        indications.entries.count { (_, value) -> value } == indications.size && indications.size > 0
    }

    override fun close(source: String) {
        synchronized(listLock) {
            for (attr in notifications.keys) if (notifications[attr] == true) {
                notifications[attr] = false
                val characteristic = characteristics[attr]
                if (characteristic != null) callQueue.offer(
                    NotificationCall(
                        characteristic,
                        false
                    )
                )
            }
            for (attr in indications.keys) if (indications[attr] == true) {
                indications[attr] = false
                val characteristic = characteristics[attr]
                if (characteristic != null) callQueue.offer(
                    IndicationCall(
                        characteristic,
                        false
                    )
                )
            }
        }
        fireState(StateInfo.CLOSED)
        SleepAndWake.runInMain("BleDevice.close", 2000) {
            LLog.d("Closing $name ${gatt?.device?.address ?: "" }")
            gatt?.close()
            gatt = null
            mayRun = false
            callQueue.clear()
            runner?.interrupt()
        }
        super.close(source)
    }

    fun disconnect() {
        gatt?.disconnect()
    }

    private fun readCharacteristic(characteristic: BluetoothGattCharacteristic, listener: GattCallListener?) {
        val attr = GattAttribute.fromCharacteristic(characteristic)
        gatt?.let {
            if (it.readCharacteristic(characteristic)){
                listener?.invoke(BleRWSuccess.SUCCESS, "OK")
            } else {
                LLog.i("Could not read characteristic $attr for $name ${gatt?.device?.address ?: ""} , service ${getServiceInfo(characteristic).t2()}")
                try {
                    Thread.sleep(250)
                    if (!it.readCharacteristic(characteristic)) {
                        LLog.i("Could still not read characteristic $attr for $name ${gatt?.device?.address ?: ""}, service ${getServiceInfo(characteristic).t2()}")
                        listener?.invoke(BleRWSuccess.FAILURE, "Failed after 2nd try")
                        return
                    } else {
                        listener?.invoke(BleRWSuccess.SUCCESS, "Read after 2nd try")
                    }
                } catch (e: InterruptedException) {
                    LLog.i("Interrupted")
                    listener?.invoke(BleRWSuccess.FAILURE, "Interrupted")
                }
            }
        }
        if (mayRun) {
            val done = synchronized(listLock) { notifications[attr] }
            if ((done == null || !done) && (characteristic.properties and BluetoothGattCharacteristic.PROPERTY_NOTIFY > 0) and attr.maySetNotification()) {
                callQueue.offer(NotificationCall(characteristic, true))
            }
        }
    }

    private fun writeCharacteristic(
        characteristic: BluetoothGattCharacteristic,
        bytes: ByteArray,
        writeType: Int = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT,
        listener: GattCallListener?
    ) {
        val attr = GattAttribute.fromCharacteristic(characteristic)
        characteristic.value = bytes
        characteristic.writeType = writeType
        gatt?.let { gatt ->
            if (!gatt.writeCharacteristic(characteristic)) {
                LLog.i("Could not write characteristic $attr for $name ${gatt.device.address} , service ${getServiceInfo(characteristic).t2()}")
                try {
                    Thread.sleep(250)
                    if (gatt.writeCharacteristic(characteristic)) {
                        listener?.invoke(BleRWSuccess.SUCCESS, "Written after 2nd try")
                    } else {
                        listener?.invoke(BleRWSuccess.FAILURE, "Failed after 2nd try")
                        LLog.i("Could still not write characteristic $attr for $name ${gatt.device.address} , service ${getServiceInfo(characteristic).t2()}")
                    }
                } catch (e: InterruptedException) {
                    listener?.invoke(BleRWSuccess.FAILURE, "Interrupted")
                }
            } else {
                listener?.invoke(BleRWSuccess.SUCCESS, "OK")
            }
        }
    }

    private fun fire(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
        fireData(GattAttribute.fromUUID(characteristic.uuid).parse(gatt, characteristic), false)
    }

    fun write(gattAttribute: GattAttribute, data: Data, listener: GattCallListener) {
        gatt?.also {
            val bytes = gattAttribute.write(it, data)
            if (bytes == null)  {
                listener(BleRWSuccess.FAILURE, "No data to write")
            } else
                write(gattAttribute, bytes, listener)
        } ?: listener(BleRWSuccess.FAILURE, "No valid gatt connection")
    }

    fun write(gattAttribute: GattAttribute, values: ByteArray, listener: GattCallListener) {
        //this.command = Tuple(gattAttribute, values)
        gatt?.let { gatt ->
            for (gattService in gatt.services) {
                val service = GattAttribute.fromUUID(gattService.uuid)
//                LLog.i("Service = $service")
                if (mayRun && service.isSupported) {
                    val characteristic = gattService.getCharacteristic(gattAttribute.uuid)
                    if (characteristic != null) { // && this.singleCall) {
                        LLog.i("Write added for $gattAttribute")
                        callQueue.offer(
                            CharacteristicWriteCall(
                                characteristic,
                                values,
                                gattAttribute.writeType,
                                listener
                            )
                        )
                        return
                    }
                }
            }
            listener(BleRWSuccess.FAILURE, "No characteristic found to write to")
        } ?: listener(BleRWSuccess.FAILURE, "No valid gatt connection")
    }

    fun read(gattAttribute: GattAttribute, listener: GattCallListener) {
        gatt?.let { gatt ->
            for (gattService in gatt.services) {
                val service = GattAttribute.fromUUID(gattService.uuid)
//                LLog.i("Service = $service")
                if (mayRun && service.isSupported) {
                    val characteristic = gattService.getCharacteristic(gattAttribute.uuid)
                    if (characteristic != null) { // && this.singleCall) {
                        LLog.i("Read added for $gattAttribute")
                        callQueue.offer(CharacteristicReadCall(characteristic, listener))
                        return
                    }
                }
            }
            listener(BleRWSuccess.FAILURE, "No characteristic found to read from")
        } ?: listener(BleRWSuccess.FAILURE, "No valid gatt connection")
    }

    companion object {
        private const val THREAD_TIMEOUT: Long = 800 //10000;
        private const val GATT_WAIT: Long = 100 //250
        var autoRead = true
        var requestMtu = true
        val handles = mutableMapOf<String, Int>()

        private fun getServiceInfo(gattService: BluetoothGattService): Tuple<UUID, String> {
            val serviceUuid = gattService.uuid
            return Tuple(serviceUuid, GattAttribute.stringFromUUID(serviceUuid))
        }

        private fun getServiceInfo(characteristic: BluetoothGattCharacteristic): Tuple<UUID, String> {
            return getServiceInfo(characteristic.service)
        }
    }

}