package com.plusot.bluelib.util

import android.content.pm.PackageManager
import com.plusot.bluelib.Globals

/**
 * Package: com.plusot.bluelib.util
 * Project: kaleido-app
 *
 * Created by Peter Bruinink on 2019-06-25.
 * Copyright © 2019 Plusot. All rights reserved.
 */
fun appVersion(): String = try {
    Globals.appContext?.let { context ->
        val pm = context.packageManager
        val info = pm?.getPackageInfo(context.getPackageName(), 0)
        info?.versionName ?: "0.0"
    } ?: "0.0"
} catch (e: PackageManager.NameNotFoundException) {
    "0.0"
}

