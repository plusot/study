package com.plusot.bluelib

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager.NameNotFoundException
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.util.SleepAndWake
import com.plusot.bluelib.util.TimeUtil
import java.io.File
import java.lang.ref.WeakReference


object Globals {
    private val CLASSTAG = Globals::class.java.simpleName
    private var appRef: WeakReference<Application>? = null
    private val bootTime = System.currentTimeMillis()
    private var metrics: DisplayMetrics? = null
    private var lastActiveActivity: WeakReference<Activity>? = null

    const val FILE_DETAIL_DELIM = "_"
    const val FLUSH_TIME: Long = 20000
    const val TAB = "   "
    const val TAG_WIDTH = 80
    var verboseDevices = false
    const val SYSLOG_SPEC = "syslog"
    const val APPLOG_SPEC = "applog"
    const val LOG_EXT = ".txt"
    const val logHistory = 30000L


    var TAG = "PlusotApp"
        private set

    var runMode = RunMode.RUN
    var screenWidth = 640
    var screenHeight = 640

    val appContext: Context?
        get() = appRef?.get()?.applicationContext

    val storePath: String
        get() {
            val context = appContext
            if (context != null) {
                val file = context.getExternalFilesDir(null)
                if (file != null) return file.absolutePath
            }
            return ""
        }

    val dataPath: String
        get() = "$storePath/deviceData/"

    val logPath: String
        get() = "$storePath/log/"

    val sessionId: String
        get() = TimeUtil.formatTime(bootTime, "yyMMdd")

    var visibleActivity: Activity?
        get() = lastActiveActivity?.get()
        set(value) {
            if (value != null) Globals.lastActiveActivity = WeakReference<Activity>(value)
        }

    fun getMetrics(): DisplayMetrics? {
        if (metrics == null) {
            val appContext = appContext ?: return null
            val manager = appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager? ?: return null
            val display = manager.defaultDisplay
            if (display != null) {
                if (metrics == null) metrics = DisplayMetrics()
                display.getMetrics(metrics)
            }
        }
        return metrics
    }

    private fun boardStr(value: String): String {
        return ("   *" +
                "                          ".substring(0, 26 - value.length / 2) +
                value +
                "                             ").substring(0, 56) + "*\n"
    }

    fun init(app: Application, caller: String) {
        if (appRef != null) {
            LLog.i("Called by: $caller")
            if (appRef?.get() === app) return
        } else
            LLog.i("Started by: $caller")

        appRef = WeakReference(app)

        val appContext = app.applicationContext
        getMetrics()?.also {
            screenWidth = it.widthPixels
            screenHeight = it.heightPixels
        }

        val appName = appContext.getString(appContext.applicationInfo.labelRes)
        TAG = appName.replace("'", "").replace(" ", "")
        runMode = RunMode.RUN

        var name = appName
        try {
            val info = appContext.packageManager.getPackageInfo(appContext.packageName, 0)
            name += " " + info.versionName
        } catch (e: NameNotFoundException) {
            Log.e(TAG, "$CLASSTAG.initAfterPermissionsChecked: name not found")
        }


        val pidStr = android.os.Process.myPid().toString()

        Log.i(
            TAG, CLASSTAG + ".onCreate:\n" +
                    "   ******************************************************\n" +
                    "   *                                                    *\n" +
                    boardStr(name) +
                    "   *                 Application Started                *\n" +
                    boardStr(pidStr) +
                    "   *                                                    *\n" +
                    "   ******************************************************"
        )

        SleepAndWake.runInNewThread() {
            var file = File(storePath)
            if (!file.isDirectory && !file.mkdirs()) {
                Log.w(TAG, "Could not create: $storePath")
            }
            file = File(logPath)
            if (!file.isDirectory && !file.mkdirs()) {
                Log.w(TAG, "Could not create: $logPath")
            }
            file = File(dataPath)
            if (!file.isDirectory && !file.mkdirs()) {
                Log.w(TAG, "Could not create: $dataPath")
            }
        }

        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                //                    LLog.i("Created activity = " + activity.getClass().getSimpleName());
                if (visibleActivity == null) visibleActivity = activity
            }

            override fun onActivityStarted(activity: Activity) {

            }

            override fun onActivityResumed(activity: Activity) {
                //                    LLog.i("Active activity = " + activity.getClass().getSimpleName());
                visibleActivity = activity
            }

            override fun onActivityPaused(activity: Activity) {
                if (visibleActivity === activity) {
                    //                        LLog.i("Paused activity = " + activity.getClass().getSimpleName());
                    visibleActivity = null
                }
            }

            override fun onActivityStopped(activity: Activity) {

            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

            }

            override fun onActivityDestroyed(activity: Activity) {
                //Globals.runMode = Globals.RunMode.FINISHING;
            }
        })
    }

    enum class RunMode {
        RUN,
        FINISHING,
        FINISHED;

        val isRun: Boolean
            get() = this == RUN

        val isFinished: Boolean
            get() = this == FINISHED

    }
}
