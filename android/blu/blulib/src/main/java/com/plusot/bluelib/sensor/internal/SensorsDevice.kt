package com.plusot.bluelib.sensor.internal

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.SensorAccuracy
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType
import com.plusot.bluelib.util.MathVector
import com.plusot.bluelib.util.toEnum

class SensorsDevice(type: DeviceType, scannedDevice: ScannedDevice) : InternalDevice(type, scannedDevice), SensorEventListener {
    private lateinit var sensorManager: SensorManager

//    init {
//        resumeDevice()
//    }

    override fun resumeDevice() {
        if (!isActive) {
            sensorManager = Globals.appContext?.getSystemService(Context.SENSOR_SERVICE) as SensorManager? ?: return
            val sensorType = type.toSensorType() ?: return
            val sensor = sensorManager.getDefaultSensor(sensorType)
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        }
        super.resumeDevice()

    }

    override fun pauseDevice() {
        if (isActive) (Globals.appContext?.getSystemService(Context.SENSOR_SERVICE) as SensorManager?)?.unregisterListener(this)
        super.pauseDevice()
    }

    override fun close(source: String) {
        sensorManager.unregisterListener(this)
        LLog.i("Unregistering listener")
        super.close(source)
    }

    override fun onSensorChanged(event: SensorEvent) {
        when (event.sensor.type) {
            Sensor.TYPE_STEP_COUNTER -> {
                val steps = event.values[0].toDouble()
                val ref = refValue.getOrPut("${type}_${DataType.STEPS}", { steps })
                fireData(
                    Data()
                        .add(address, DataType.STEPS, (steps - ref).toInt())
                        .add(address, DataType.STEPS_TOTAL, steps.toInt())
                )
                //refValue["${type}_${DataType.STEPS}"] = steps
            }
            Sensor.TYPE_STEP_DETECTOR -> {
                var steps = refValue.getOrPut("${type}_${DataType.STEPS}", { 0.0 })
                steps += 1.0
                refValue["${type}_${DataType.STEPS}"] = steps
                fireData(
                    Data()
                        .add(address, DataType.STEPS, steps.toInt())
                )
            }
            Sensor.TYPE_LIGHT -> {
                fireData(Data(address).add(DataType.LIGHT, 1.0 * event.values[0]))
            }
            Sensor.TYPE_PRESSURE -> {
                fireData(Data(address).add(DataType.PRESSURE, 100.0 * event.values[0]))
            }
            Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_LINEAR_ACCELERATION, Sensor.TYPE_ACCELEROMETER_UNCALIBRATED -> {
                fireData(Data(address)
                    .add(DataType.ACCELERATION_XYZ, MathVector(event.values).getClonedValues())
                    .add(DataType.ACCELERATION, MathVector(event.values).length()))
            }
            Sensor.TYPE_HEART_BEAT -> fireData(Data().add(address, DataType.HEART_RATE, event.values[0].toInt()))
            Sensor.TYPE_HEART_RATE -> fireData(Data().add(address, DataType.HEART_RATE, event.values[0].toInt()))
            Sensor.TYPE_AMBIENT_TEMPERATURE -> fireData( Data().add(address, DataType.TEMPERATURE, event.values[0].toDouble()))
            Sensor.TYPE_PROXIMITY -> {
//                LLog.d("Proximity ${event.values[0]}")
                fireData(Data().add(address, DataType.PROXIMITY, 0.01 * event.values[0].toDouble()))
            }
            Sensor.TYPE_RELATIVE_HUMIDITY -> fireData(Data(address).add(DataType.HUMIDITY, 0.01 * event.values[0]))
            else -> fireData(Data().add(address, DataType.VECTOR, MathVector(event.values).getClonedValues()))

        }//                LLog.i("Steps detected = " + stepsDetected);
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        //sensor ?: return
        //val sensorAccuracy = accuracy.toEnum<SensorAccuracy>()
        fireState(StateInfo.CONNECTED, Data(address).add(DataType.SENSOR_ACCURACY, accuracy))
    }

    companion object {
        val refValue = mutableMapOf<String, Double>()
    }
}