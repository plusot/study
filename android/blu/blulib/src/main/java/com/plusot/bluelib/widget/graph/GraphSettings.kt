package com.plusot.bluelib.widget.graph

class GraphSettings(
    val axesType: AxesType,
    val numberOfPoints: Int = 10000,
    val clearLinesAtStart: Boolean = false
)