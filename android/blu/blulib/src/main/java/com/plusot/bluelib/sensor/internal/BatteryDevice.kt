package com.plusot.bluelib.sensor.internal

import android.app.Activity
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import com.plusot.bluelib.Globals
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.DeviceType
import com.plusot.bluelib.sensor.ScannedDevice
import com.plusot.bluelib.sensor.data.Data
import com.plusot.bluelib.sensor.data.DataType

class BatteryDevice (scannedDevice: ScannedDevice) : InternalDevice(DeviceType.INTERNAL_BATTERY_SENSOR, scannedDevice) {
    private var batteryPercentage = 0.0
    private var startPercentage = -1.0

    private val batteryReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
            val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0)
            //if (intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0) != 0) return;
            //LLog.i("BatteryDevice.onReceive: " + level);
            //				Bundle bundle = intent.getExtras();
            //				for (String key : bundle.keySet()) {
            //					LLog.i("Battery info for " + key + " = " + bundle.getDouble(key));
            //				}
            if (batteryPercentage != 100.0 * level / scale) {
                batteryPercentage = 100.0 * level / scale
                if (startPercentage == -1.0) {
                    startPercentage = batteryPercentage
                    //                    startTime = System.currentTimeMillis();
                }
                //                else {
                //                    batteryUsage = 3600000 * (startPercentage - batteryPercentage) / (System.currentTimeMillis() - startTime); // %/hr
                //                }

                fireData(Data().add(address, DataType.BATTERY_LEVEL, 0.01 * batteryPercentage))

                //LLog.i("Battery: " + batteryPercentage + "%, usage: " + Format.format(batteryUsage, 2) + " %/hr");

                //                if (batteryTesting) {
                //                    Map<String, String> map = new HashMap<String, String>();
                //                    map.put("time", "\"" + TimeUtil.formatMilli(System.currentTimeMillis()) + "\"");
                //                    map.put("batt", Format.format2(batteryPercentage));
                //                    map.put("usage", Format.format2(batteryUsage));
                //
                //                    SimpleLog.getInstance(SimpleLogType.JSON, "batt").log(map);
                //
                //                }

            }
        }
    }

    init {
        Globals.appContext?.registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    }


    override fun close(source: String) {
        val appContext = Globals.appContext ?: return
        appContext.unregisterReceiver(batteryReceiver)
        LLog.i("Unregistering listener")
        super.close(source)
    }
}

fun Activity.getBatteryLevel(): Double {
    val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { intentFilter ->
        this.registerReceiver(null, intentFilter)
    }
    return batteryStatus?.let { intent ->
        val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
        val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        (level.toDouble() / scale)
    } ?: 0.0
}
