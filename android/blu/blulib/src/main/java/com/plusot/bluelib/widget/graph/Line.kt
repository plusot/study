package com.plusot.bluelib.widget.graph

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import com.plusot.bluelib.util.minus
import kotlin.math.max
import kotlin.math.min

class MinMax(yVisible: Boolean) {
    var maxX = if (yVisible) 0F else Float.NEGATIVE_INFINITY
    var minX = if (yVisible) 0F else Float.POSITIVE_INFINITY
    var maxY = if (yVisible) 0F else Float.NEGATIVE_INFINITY
    var minY = if (yVisible) 0F else Float.POSITIVE_INFINITY

    val isInValid: Boolean
        get() = maxX == Float.NEGATIVE_INFINITY ||
                minX == Float.POSITIVE_INFINITY ||
                maxY == Float.NEGATIVE_INFINITY ||
                minY == Float.POSITIVE_INFINITY ||
                maxY == minY ||
                minX == maxX
}

class Line(
    private val fillAlpha: Float = 0F,
    private val isCubic: Boolean = false
) {
    private val dataPoints: MutableList<Pair<Float, Float>> = mutableListOf()
    var offset = 0F to 0F
    private val paint: Paint = Paint()
    private val fillPaint: Paint = Paint()
    private val path: Path = Path()
    private val prevPoint = Point()
    private val currentPoint = Point()
    private val nextPoint = Point()
    private val fill = fillAlpha > 0.01F
    private val lock = Object()

    var strokeWidth: Float
        get() = paint.strokeWidth
        set(value) {
            paint.strokeWidth = value
            fillPaint.strokeWidth = value
        }

    var color: Int = 0xFFFF0000L.toInt()
        set(value){
            field = value
            val a = value shr 24 and 0xff
            val r = value shr 16 and 0xff
            val g = value shr 8 and 0xff
            val b = value and 0xff
            fillPaint.setARGB((fillAlpha * a).toInt(), r, g, b)
            paint.setARGB(a, r, g, b)
        }

    init {
        paint.also {
            it.style = Paint.Style.STROKE
            it.strokeCap = Paint.Cap.ROUND
            it.strokeWidth = 5.0f
            it.isAntiAlias = true
            it.setARGB(255, 255, 0, 0)
        }
        fillPaint.also {
            it.style = Paint.Style.FILL
            it.strokeCap = Paint.Cap.ROUND
            it.strokeWidth = 5.0f
            it.isAntiAlias = true
            it.setARGB(128, 255, 0, 0)
        }
    }

    fun addPoint(x: Float, y: Float) {
        synchronized(lock) {
            dataPoints.add(x to y)
        }
    }

    val size: Int
        get() = synchronized(lock) { dataPoints.size }

    fun removeFirstPoint() {
        synchronized(lock) { dataPoints.removeAt(0) }
    }

    val lastPoint: Pair<Float, Float>?
        get() = synchronized(lock) { dataPoints.lastOrNull() }

    fun clearPoints() {
        synchronized(lock) { dataPoints.clear() }
    }

    fun minMax(minMax: MinMax) {
        synchronized(lock) {
            val (xOff, yOff) = offset
            dataPoints.forEach { (x, y) ->
                minMax.maxY = max(y - yOff, minMax.maxY)
                minMax.minY = min(y - yOff, minMax.minY)
                minMax.maxX = max(x - xOff, minMax.maxX)
                minMax.minX = min(x - xOff, minMax.minX)
            }
        }
    }

    fun draw(canvas: Canvas,
             minXVal: Float, minYVal: Float,
             maxXVal: Float, maxYVal: Float,
             posX: Float = 0F, posY: Float = 0F,
             w: Float, h: Float, border: Float = 15f) {
        path.reset()
        val xLen = maxXVal - minXVal
        val yLen = maxYVal - minYVal
        if (xLen == 0f) return

        synchronized(lock) {
            var ii = 0
            val iterations = dataPoints.size
            while (ii < iterations - 1) {
                val (x1, y1) = dataPoints[ii + 1] - offset
                nextPoint.set(
                    posX + 1.0f * (w - 2 * border) * (x1 - minXVal) / xLen + border,
                    posY + h - border - (h - 2 * border) * (y1 - minYVal) / yLen
                )
                if (ii == 0) {
                    val (x0, y0) = dataPoints[ii] - offset
                    currentPoint.set(
                        posX + 1.0f * (w - 2 * border) * (x0 - minXVal) / xLen + border,
                        posY + h - border - (h - 2 * border) * (y0 - minYVal) / yLen
                    )
                    path.moveTo(currentPoint.x, currentPoint.y)
                    prevPoint.set(currentPoint)
                }
                currentPoint.dset(
                    (nextPoint.x - prevPoint.x) / 5,
                    (nextPoint.y - prevPoint.y) / 5
                )
                //if (ii != 0) {
                if (isCubic) path.cubicTo(
                    prevPoint.x + prevPoint.dx, prevPoint.y + prevPoint.dy,
                    currentPoint.x - currentPoint.dx, currentPoint.y - currentPoint.dy,
                    currentPoint.x, currentPoint.y
                ) else path.lineTo(nextPoint.x, nextPoint.y)
                //}
                prevPoint.set(currentPoint)
                currentPoint.set(nextPoint)
                ii++
            }
        }
        currentPoint.dset(
            (currentPoint.x - prevPoint.x) / 5,
            (currentPoint.y - prevPoint.y) / 5
        )
        if (isCubic) path.cubicTo(
            prevPoint.x + prevPoint.dx, prevPoint.y + prevPoint.dy,
            currentPoint.x - currentPoint.dx, currentPoint.y - currentPoint.dy,
            currentPoint.x, currentPoint.y
        )
        //            else
//                path.lineTo(currentPoint.x, currentPoint.y);

        //sparkLinePaint.setPathEffect(new CornerPathEffect(10))''
        canvas.drawPath(path, paint)
        if (fill) {
//            currentPoint.set(
//                posX + 1.0f * (w - 2 * border) * (0F - minXVal) / xLen + border,
//                posY + h - border - (h - 2 * border) * (0F - minYVal) / yLen
//            )
//            path.lineTo(currentPoint.x, currentPoint.y)
//            currentPoint.set(posX, posY + h - border)
//            path.lineTo(currentPoint.x, currentPoint.y)
            path.close()
            canvas.drawPath(path, fillPaint)
        }
    }
}