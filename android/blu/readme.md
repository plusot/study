# Blu
Blu is a Bluetooth Low Energy project written completely in Kotlin. The project consists of:

* BlueLib: A library to give easy access to:
    * BLE sensors, 
    * Internal sensors,
    * Extensible peripheral server, currently supporting a heart rate peripheral  
* BluDemo: A simple app showing the working of the BluLib
* BluWatch: A simple Android Wear app showing the workings of the BluLib 
* PlayGround: A Kotlin desktop app to easily test algorithms

Future extensions of the BluLib will be Bluetooth Classic and ANT+ and more peripheral services.
