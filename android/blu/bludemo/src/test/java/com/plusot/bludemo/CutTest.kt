package com.plusot.bludemo

import com.plusot.bluelib.util.cut
import org.junit.Assert
import org.junit.Test

class CutTest {
    @Test
    fun checkCut() {
        println("Starting test")
        val step = 0.05
        val power = 0.98
        println(0.05F.cut(step, power))
        println(0.5F.cut(step, power))
        println(1.0F.cut(step, power))
        println(2.0F.cut(step, power))
        println(5.0F.cut(step, power))
        println(10.0F.cut(step, power))
        println(100.0F.cut(step, power))
        println()

    }
}