package com.plusot.bludemo

import android.net.wifi.hotspot2.pps.HomeSp
import com.plusot.bluelib.log.LLog
import com.plusot.bluelib.sensor.bluetooth.ble.GattAttribute
import com.plusot.bluelib.util.asEnum
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
enum class HomoSpecies {

    Antecessor,
    Cepranensis,
    Denisova,
    Erectus,
    Ergaster,
    Floresiensis,
    Georgicus,
    Habilis,
    Heidelbergensis,
    Helmei,
    Naledi,
    Neandertalensis,
    Rhodesiensis,
    Rudolfensis,
    Sapiens,
}

class GlobalUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun check_gattAttributes() {
        GattAttribute.values().forEach {
            println("GattAttribute(GattAttributeType.${it.type},Guid(\"${it.uuid}\"),\"${it.name.toLowerCase().replace("_"," ")}\"),");
        }
    }


    @Test
    fun check_enum() {
        print("Homo species ${5.asEnum<HomoSpecies>()}")
        assertEquals(HomoSpecies.Ergaster, 4.asEnum<HomoSpecies>())
    }

    @Test
    fun check_order() {
        val i = 1 shl 8 - 1
        val j = 1 shl 7
        println("Result = $i")
        assertEquals(i, 0x80)
        assertEquals(i, j)
    }
}