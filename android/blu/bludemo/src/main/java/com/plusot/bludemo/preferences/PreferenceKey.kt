package com.plusot.bludemo.preferences

import com.plusot.bluelib.preferences.PreferenceKeyElements


enum class PreferenceKey private constructor(label: String, defaultValue: Any, valueClass: Class<*>, flags: Long = 0) {
    PERIPHERAL("peripheral", false, Boolean::class.java),
    ;

    private val elements: PreferenceKeyElements = PreferenceKeyElements(label, defaultValue, valueClass, flags)

    var int: Int
        get() = elements.int
        set(value) {
            elements.set(value)
        }

    var double: Double
        get() = elements.double
        set(value) {
            elements.double = value
        }

    var long: Long
        get() = elements.long
        set(value) {
            elements.long = value
        }

    var string: String
        get() = elements.string
        set(value) {
            elements.string = value
        }

    var isTrue: Boolean
        get() = elements.boolean
        set(value) {
            elements.boolean = value
        }


    companion object {

        fun fromString(label: String): PreferenceKey? {
            for (key in PreferenceKey.values()) {
                if (key.elements.label == label) return key
            }
            return null
        }
    }

}