package com.plusot.bludemo.preferences

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.plusot.bludemo.R

class ApplicationSettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.preferences, ApplicationSettingsFragment())
            .commit()
    }
}