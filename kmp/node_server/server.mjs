import express from "express";

const app = express();
const port = 43000;
const host = '0.0.0.0';

app.get('/', (req, res) => {
    var date = new Date();
    res.send(`Mire Node on ${date.toDateString()} ${date.toTimeString().split('(')[0]}`);
});

app.listen(port, host, () => {
  console.log(`Server is running on host ${host} on port ${port}`);
});