package com.plusot.mire.permissions

import dev.icerock.moko.permissions.Permission as MokoPermission

public actual typealias Permission = MokoPermission
