package com.plusot.mire.permissions

import dev.icerock.moko.permissions.PermissionState as MokoPermissionState

public actual typealias PermissionState = MokoPermissionState
