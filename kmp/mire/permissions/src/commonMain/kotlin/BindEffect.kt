package com.plusot.mire.permissions

import androidx.compose.runtime.Composable

@Composable
public expect fun BindEffect(permissionsController: PermissionsController)
