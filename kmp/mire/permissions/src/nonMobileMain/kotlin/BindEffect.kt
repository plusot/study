package com.plusot.mire.permissions

import androidx.compose.runtime.Composable

@Composable
public actual fun BindEffect(permissionsController: PermissionsController) {
    // No-op
}
