package com.plusot.mire.permissions

public actual open class DeniedException : Exception()
public actual class DeniedAlwaysException : DeniedException()
