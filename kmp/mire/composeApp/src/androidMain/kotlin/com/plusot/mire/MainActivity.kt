package com.plusot.mire

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.ui.platform.LocalContext
import androidx.core.app.ActivityCompat
import org.koin.dsl.module

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        if (needToAskForRuntimePermissions()) {
//            askForRuntimePermissions {
//                // Do something when permission granted
//            }
//        }
        setContent {
            val context = LocalContext.current.applicationContext
            App(module{
                single<Activity> { this@MainActivity }
                single<Context> { context }
                single<SharedPreferences> {
                    get<Activity>().getSharedPreferences("MireApp", Context.MODE_PRIVATE)
                }
            })
        }
    }

//    private fun askForRuntimePermissions(doWhenPermissionAcquired: ()-> Unit) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//
//            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
//
//                if (permissions[Manifest.permission.BLUETOOTH_SCAN] == true &&
//                    permissions[Manifest.permission.BLUETOOTH_CONNECT] == true &&
//                    permissions[Manifest.permission.ACCESS_FINE_LOCATION] == true
//                ) {
//                    doWhenPermissionAcquired()
//                } else {
//                    Toast.makeText(this, "Cannot scan without permission.", Toast.LENGTH_LONG)
//                        .show()
//                }
//            }.apply {
//                launch(
//                    arrayOf(
//                        Manifest.permission.BLUETOOTH_SCAN,
//                        Manifest.permission.BLUETOOTH_CONNECT,
//                        Manifest.permission.ACCESS_FINE_LOCATION
//                    )
//                )
//            }
//        }
//    }
//
//
//    private fun needToAskForRuntimePermissions() =
//        Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
//
//                ((ActivityCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED ||
//
//                        ActivityCompat.checkSelfPermission(
//                            this,
//                            Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED ||
//
//                        ActivityCompat.checkSelfPermission(
//                            this,
//                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED))
}

//@Preview
//@Composable
//fun AppAndroidPreview() {
//    App(appModule = AppModule(LocalContext.current.applicationContext))
//}