package com.plusot.mire

import androidx.compose.ui.window.ComposeUIViewController
//import com.plusot.mire.di.BleModule
import org.koin.dsl.module
import com.russhwolf.settings.Settings
import platform.Foundation.NSUserDefaults
import com.russhwolf.settings.NSUserDefaultsSettings


fun MainViewController(userDefaults: NSUserDefaults) = ComposeUIViewController {
    App(
        module {
            single {
                //BleModule()
            }
            single<Settings> {
                NSUserDefaultsSettings(userDefaults)
            }
        }
    )
}