package com.plusot.mire.ui.sensor.chart

data class Sample(
    val t: Float,
    val x: Float,
    val y: Float,
    val z: Float,
)
