package com.plusot.mire.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable

//private val darkColorScheme = darkColorScheme(
//  primary = ColorPrimary,
//  primaryContainer = ColorPrimaryDark,
//  secondary = ColorAccent
//)
//
//private val lightColorScheme = lightColorScheme(
//  primary = ColorPrimary,
//  primaryContainer = ColorPrimaryDark,
//  secondary = ColorAccent,
//
//  /* Other default colors to override
//  background = Color.White,
//  surface = Color.White,
//  onPrimary = Color.White,
//  onSecondary = Color.Black,
//  onBackground = Color.Black,
//  onSurface = Color.Black,
//  */
//)

//@Composable
//fun AppTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
//  val colorScheme = if (darkTheme) {
//    darkColorScheme
//  } else {
//    lightColorScheme
//  }
//
//  MaterialTheme(
//    colorScheme = colorScheme,
//    typography = Typography,
//    shapes = Shapes,
//    content = content
//  )
//}

@Composable
fun AppTheme(
  darkTheme: Boolean = isSystemInDarkTheme(),
  content: @Composable () -> Unit,
) {
  MaterialTheme(
    colorScheme = if (darkTheme) darkColorScheme() else lightColorScheme(),
    content = content,
  )
}
