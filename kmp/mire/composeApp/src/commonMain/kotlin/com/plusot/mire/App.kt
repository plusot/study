package com.plusot.mire

import androidx.compose.runtime.Composable
import com.plusot.mire.data.DbHelper
import com.plusot.mire.data.MeasurementRepository
import com.plusot.mire.device.getPlatform
import com.plusot.mire.device.platformModule
import com.plusot.mire.network.MireApi
import com.plusot.mire.presentation.AboutViewModel
import com.plusot.mire.presentation.HomeViewModel
import com.plusot.mire.ui.root.AppScaffold
import com.plusot.mire.ui.theme.AppTheme
import com.plusot.mire.util.LogLevel
import com.plusot.mire.util.SimpleLogger
import org.jetbrains.compose.ui.tooling.preview.Preview
import org.koin.compose.KoinApplication
import org.koin.core.module.Module
import org.koin.dsl.module

@Composable
@Preview
fun App(appModule: Module) {
    SimpleLogger.tag = "MireTag"
    SimpleLogger.logLevel = LogLevel.DEBUG
    SimpleLogger.info("Started App")
    KoinApplication(application = {
        modules (
            appModule,
            platformModule,
            module {
                single { MireApi() }
                factory { getPlatform() }
                factory { DbHelper(get()) }
            },
            module {
                factory { MeasurementRepository(get()) }
                factory { AboutViewModel(get(), get(), get()) }
            },
            module {
                factory { HomeViewModel(get()) }
                // factory { ScanViewModel() }
            }
        )
    }) {
        AppTheme {
        //MaterialTheme {
            AppScaffold()
        }
    }
}