package com.plusot.mire.ui.sensor

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.plusot.mire.ui.sensor.chart.Sample
import com.plusot.mire.ui.sensor.chart.update
import kotlinx.coroutines.flow.Flow
import com.juul.krayon.compose.ElementView


@Composable
fun SensorDisplay(data: Flow<List<Sample>>, modifier: Modifier) {
    ElementView(data, ::update, modifier)
}
