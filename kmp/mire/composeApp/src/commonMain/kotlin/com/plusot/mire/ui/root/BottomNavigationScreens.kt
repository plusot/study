package com.plusot.mire.ui.root

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.Search
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import com.plusot.mire.ui.ScreenState
import mire.composeapp.generated.resources.Res
import mire.composeapp.generated.resources.icon_chart
import mire.composeapp.generated.resources.icon_home
import mire.composeapp.generated.resources.navigation_graph
import mire.composeapp.generated.resources.navigation_home
import mire.composeapp.generated.resources.navigation_info
import mire.composeapp.generated.resources.navigation_scanner
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource


sealed class BottomNavigationScreens(
  val route: String,
  val title: StringResource,
  val icon: @Composable () -> Unit
) {

  data object Home : BottomNavigationScreens(
    route = ScreenState.Home.route,
    title = Res.string.navigation_home,
    icon = {
      Icon(
        painter = painterResource(Res.drawable.icon_home),
        contentDescription = stringResource(Res.string.navigation_home)
      )
    }
  )

  data object Scan : BottomNavigationScreens(
    route = ScreenState.Scan.route,
    title = Res.string.navigation_scanner,
    icon = {
      Icon(
        imageVector = Icons.Outlined.Search,
        //painter = painterResource(Res.drawable.ic_bookmarks),
        contentDescription = stringResource(Res.string.navigation_scanner)
      )
    }
  )

  data object Graph : BottomNavigationScreens(
    route = ScreenState.Graph.route,
    title = Res.string.navigation_graph,
    icon = {
      Icon(
        painter = painterResource(Res.drawable.icon_chart),
        contentDescription = stringResource(Res.string.navigation_graph)
      )
    }
  )

  data object Info : BottomNavigationScreens(
    route = ScreenState.About.route,
    title = Res.string.navigation_info,
    icon = {
      Icon(
        imageVector = Icons.Outlined.Info,
        contentDescription = stringResource(Res.string.navigation_info)
      )
    }
  )
}