package com.plusot.mire.ui.home

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.PlayArrow
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.neverEqualPolicy
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEvent
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onPreviewKeyEvent
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import com.plusot.mire.Greeting
import com.plusot.mire.data.TimeHelperImpl
import com.plusot.mire.data.formatDateTime
import com.plusot.mire.data.model.Measurement
import com.plusot.mire.data.model.TimeHelper
import com.plusot.mire.presentation.HomeViewModel
import com.plusot.mire.util.format
import kotlinx.coroutines.delay
import mire.composeapp.generated.resources.Res
import mire.composeapp.generated.resources.aboutButton
import mire.composeapp.generated.resources.compose_multiplatform
import mire.composeapp.generated.resources.homeButton
import mire.composeapp.generated.resources.scanButton
import mire.composeapp.generated.resources.title
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource
import org.koin.compose.koinInject

const val timeMillis = 1000L // 1 second

@Composable
fun HomeView(
    viewModel: HomeViewModel = koinInject(),
) {
    var showContent by remember { mutableStateOf(false) }
    val timeHelper: TimeHelper = TimeHelperImpl()
    var measurements by remember {
        mutableStateOf(listOf<Measurement>(), policy = neverEqualPolicy())
    }

    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }
    var shouldFocusOnTextField by remember { mutableStateOf(false) }

    var textFieldValue by remember { mutableStateOf("") }

    viewModel.onMeasurementsUpdated = {
        measurements = it
    }

    Column(Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
        var timeString by remember { mutableStateOf(timeHelper.currentTimeString()) }
        LaunchedEffect(Unit) {
            while (true) {
                timeString = timeHelper.currentTimeString()
                delay(timeMillis) // Every minute
            }
        }
        Text(
            timeString, style = MaterialTheme.typography.headlineSmall,
            modifier = Modifier.padding(bottom = 16.dp)
        )

        AnimatedVisibility(showContent, modifier = Modifier.padding(bottom = 16.dp)) {
            val greeting = remember { Greeting().greet() }
            Column(Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
                Image(painterResource(Res.drawable.compose_multiplatform), null)
                Text(greeting)
            }
        }
        LazyColumn(modifier = Modifier.fillMaxWidth()) {
            items(items = measurements) { item ->
                val onItemClick = {
                    focusManager.clearFocus(true)
                    viewModel.markMeasurement(time = item.time, isStored = !item.isStored)
                }

                MeasurementItem(
                    title = "${item.time.formatDateTime()}: ${item.value.format()}",
                    isCompleted = item.isStored,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable(enabled = true, onClick = onItemClick)
                        .padding(horizontal = 16.dp, vertical = 4.dp)
                )
            }


            item {
                val onSubmit = {
                    if (textFieldValue.isNotEmpty()) viewModel.addMeasurement(
                        time = timeHelper.currentTime(),
                        value = (textFieldValue.toDoubleOrNull() ?: 0.0) )
                    textFieldValue = ""
                    shouldFocusOnTextField = true
                }

                NewMeasurementTextField(
                    value = textFieldValue,
                    onValueChange = { textFieldValue = it },
                    onSubmit = onSubmit,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp, horizontal = 16.dp)
                        .focusRequester(focusRequester)
                )

                LaunchedEffect(measurements) {
                    if (shouldFocusOnTextField) {
                        focusRequester.requestFocus()
                        shouldFocusOnTextField = false
                    }
                }
            }
        }
        Button(onClick = { showContent = !showContent }) {
            Text(stringResource(Res.string.homeButton))
        }
    }
}

@Composable
private fun MeasurementItem(
    title: String,
    isCompleted: Boolean,
    modifier: Modifier = Modifier,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start,
        modifier = modifier
    ) {
        RadioButton(
            selected = isCompleted,
            onClick = null
        )

        Text(
            text = title,
            style = if (isCompleted) {
                MaterialTheme.typography.bodyLarge.copy(
                    textDecoration = TextDecoration.LineThrough,
                    fontStyle = FontStyle.Italic,
                    color = Color.Gray,
                )
            } else {
                MaterialTheme.typography.bodyLarge
            },
            modifier = Modifier.padding(8.dp),
        )
    }
}

@Composable
private fun NewMeasurementTextField(
    value: String,
    onValueChange: (String) -> Unit,
    onSubmit: () -> Unit,
    modifier: Modifier = Modifier,
) {
    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        placeholder = { Text("Add a new measurement here") },
        keyboardOptions = KeyboardOptions.Default.copy(
            capitalization = KeyboardCapitalization.Words,
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Done,
        ),
        keyboardActions = KeyboardActions(
            onDone = { onSubmit() }
        ),
        modifier = modifier
            .onPreviewKeyEvent { event: KeyEvent ->
                if (event.key == Key.Enter) {
                    onSubmit()
                    return@onPreviewKeyEvent true
                }
                false
            }
    )
}
