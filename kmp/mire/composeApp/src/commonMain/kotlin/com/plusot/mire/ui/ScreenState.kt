package com.plusot.mire.ui

import mire.composeapp.generated.resources.Res
import mire.composeapp.generated.resources.navigation_graph
import mire.composeapp.generated.resources.navigation_home
import mire.composeapp.generated.resources.navigation_info
import mire.composeapp.generated.resources.navigation_scanner
import org.jetbrains.compose.resources.StringResource

enum class ScreenState(val route: String, val title: StringResource) {
    Home("home", Res.string.navigation_home),
    Scan("scan", Res.string.navigation_scanner),
    Graph("graph", Res.string.navigation_graph),
    About("about-device", Res.string.navigation_info),
    ;

    companion object {
        fun fromRoute(route: String?): ScreenState = entries.find { it.route == route } ?: Home
    }
}