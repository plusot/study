package com.plusot.mire.ui.theme

import androidx.compose.ui.graphics.Color

val ColorPrimary = Color(0xFF006837)
val ColorPrimaryDark = Color(0xFF004012)
val ColorAccent = Color(0xFFC75f00)