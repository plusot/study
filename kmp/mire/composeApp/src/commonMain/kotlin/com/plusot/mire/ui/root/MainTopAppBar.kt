import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.plusot.mire.ui.ScreenState
import mire.composeapp.generated.resources.Res
import mire.composeapp.generated.resources.aboutButton
import mire.composeapp.generated.resources.scanButton
import mire.composeapp.generated.resources.title
import org.jetbrains.compose.resources.stringResource

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainTopAppBar(
    navController: NavHostController,
) {
    val currentBackStackEntry by navController.currentBackStackEntryAsState()
    val currentScreen = ScreenState.fromRoute(currentBackStackEntry?.destination?.route)
    val currentRoute = stringResource(currentScreen.title)

    TopAppBar(
        title = { Text(text = "${stringResource(Res.string.title)}: $currentRoute") },
        actions = {
            IconButton(onClick = { navController.navigate(ScreenState.Scan.route) }) {
                Icon(
                    imageVector = Icons.Outlined.Search,
                    contentDescription = stringResource(Res.string.scanButton),
                )
            }
            IconButton(onClick = { navController.navigate(ScreenState.About.route) }) {
                Icon(
                    imageVector = Icons.Outlined.Info,
                    contentDescription = stringResource(Res.string.aboutButton),
                )
            }
        }
    )
}
