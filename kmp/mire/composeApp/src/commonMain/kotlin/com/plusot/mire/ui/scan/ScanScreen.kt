package com.plusot.mire.ui.scan

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.BottomCenter
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.model.rememberScreenModel
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import com.juul.kable.PlatformAdvertisement
import com.plusot.mire.bluetooth.rememberSystemControl
import com.plusot.mire.bluetooth.requirements.rememberBluetoothRequirementsFactory
import com.plusot.mire.icons.LocationDisabled
import com.plusot.mire.permissions.BindEffect
import com.plusot.mire.permissions.rememberPermissionsControllerFactory
import com.plusot.mire.ui.components.ActionRequired
import com.plusot.mire.ui.components.BluetoothDisabled
import com.plusot.mire.ui.scan.DeviceLocator.State.NotYetScanned
import com.plusot.mire.ui.scan.DeviceLocator.State.Scanning
import mire.composeapp.generated.resources.Res
import mire.composeapp.generated.resources.bluetoothPermissionsDeniedButtonText
import mire.composeapp.generated.resources.bluetoothPermissionsDeniedContentDescription
import mire.composeapp.generated.resources.bluetoothPermissionsDeniedDescription
import mire.composeapp.generated.resources.clear
import mire.composeapp.generated.resources.locationServicesDisabledButtonText
import mire.composeapp.generated.resources.locationServicesDisabledContentDescription
import mire.composeapp.generated.resources.locationServicesDisabledDescription
import mire.composeapp.generated.resources.refresh
import mire.composeapp.generated.resources.scanTitle
import org.jetbrains.compose.resources.stringResource

class ScanScreen() : Screen {

    @Composable
    override fun Content() {
        val screenModel = rememberScreenModel()
        onLifecycleResumed(screenModel::onResumed)
        handleNavigation(screenModel)

        val viewState by screenModel.viewState.collectAsState()
        val showConnectPermissionAlertDialog by screenModel.showConnectPermissionAlertDialog.collectAsState()
        val snackbarText by screenModel.snackbarText.collectAsState()

        Column(
            modifier = Modifier.fillMaxSize(),
        ) {
            Box(Modifier.weight(1f)) {
//                ProvideTextStyle(
//                    TextStyle(color = contentColorFor(backgroundColor = MaterialTheme.colors.background))
//                ) {
                    val systemControl = rememberSystemControl()
                    ScanPane(
                        viewState,
                        onScanClick = screenModel::scan,
                        onShowAppSettingsClick = screenModel::openAppSettings,
                        onShowLocationSettingsClick = systemControl::showLocationSettings,
                        onEnableBluetoothClick = systemControl::requestToTurnBluetoothOn,
                        onAdvertisementClicked = screenModel::onAdvertisementClicked,
                    )
//                }

                snackbarText?.let { text ->
                    Snackbar(text)
                }

                if (showConnectPermissionAlertDialog) {
                    ConnectPermissionsAlertDialog(
                        onOpenAppSettings = screenModel::openAppSettings,
                        onCancel = screenModel::dismissAlert,
                    )
                }

                Column(
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .padding(16.dp),
                    horizontalAlignment = Alignment.End,
                    verticalArrangement = Arrangement.spacedBy(16.dp)
                ) {
                    if (viewState == ViewState.Scan || (viewState as? ViewState.Devices)?.scanState != Scanning) {
                        FloatingActionButton(onClick = screenModel::scan) {
                            Icon(Icons.Filled.Refresh, contentDescription = stringResource(Res.string.refresh))
                        }
                    }

                    FloatingActionButton(onClick = screenModel::clear) {
                        Icon(Icons.Filled.Delete, contentDescription = stringResource(Res.string.clear))
                    }
                }
            }
        }
    }

    @Composable
    private fun rememberScreenModel(): ScanScreenModel {
        val permissionsFactory = rememberPermissionsControllerFactory()
        val bluetoothRequirementsFactory = rememberBluetoothRequirementsFactory()
        val screenModel = rememberScreenModel {
            val permissionsController = permissionsFactory.createPermissionsController()
            val bluetoothRequirements = bluetoothRequirementsFactory.create()
            ScanScreenModel(permissionsController, bluetoothRequirements)
        }
        BindEffect(screenModel.permissionsController)
        return screenModel
    }
}

@Composable
private fun handleNavigation(screenModel: ScanScreenModel) {
    val navigator = LocalNavigator.currentOrThrow
    LaunchedEffect(screenModel) {
        screenModel.navigation.collect(navigator::push)
    }
}

@Composable
private fun BoxScope.Snackbar(text: String) {
    Snackbar(
        Modifier
            .align(BottomCenter)
            .padding(10.dp)
    ) {
        Text(text, style = MaterialTheme.typography.bodyLarge)
    }
}

@Composable
private fun ConnectPermissionsAlertDialog(
    onOpenAppSettings: () -> Unit,
    onCancel: () -> Unit,
) {
    AlertDialog(
        title = { Text("Permission required") },
        text = { Text("Bluetooth connect permission needed to connect to device. Please grant permission via App settings.") },
        confirmButton = {
            TextButton(onClick = onOpenAppSettings) {
                Text("Open App Settings")
            }
        },
        dismissButton = {
            TextButton(onClick = onCancel) {
                Text("Cancel")
            }
        },
        onDismissRequest = onCancel,
    )
}

@Composable
private fun ScanPane(
    viewState: ViewState,
    onScanClick: () -> Unit,
    onShowAppSettingsClick: () -> Unit,
    onShowLocationSettingsClick: () -> Unit,
    onEnableBluetoothClick: () -> Unit,
    onAdvertisementClicked: (PlatformAdvertisement) -> Unit,
) {
    when (viewState) {
        is ViewState.Unsupported -> BluetoothNotSupported()
        is ViewState.Scan -> Scan(message = null, onScanClick)
        is ViewState.PermissionDenied -> BluetoothPermissionsDenied(onShowAppSettingsClick)
        is ViewState.LocationServicesDisabled -> LocationServicesDisabled(onShowLocationSettingsClick)
        is ViewState.BluetoothOff -> BluetoothDisabled(onEnableBluetoothClick)
        is ViewState.Devices -> AdvertisementsList(
            viewState.scanState,
            viewState.advertisements,
            onScanClick,
            onAdvertisementClicked
        )
    }
}

@Composable
private fun BluetoothNotSupported() {
    Column(
        Modifier
            .fillMaxSize()
            .padding(20.dp),
        horizontalAlignment = CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Text(text = "Bluetooth not supported.")
    }
}

@Composable
private fun Loading() {
    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun Scan(message: String?, onScanClick: () -> Unit) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        Column(horizontalAlignment = CenterHorizontally) {
            if (message != null) Text(message)
            Button(onScanClick) {
                Text(stringResource(Res.string.scanTitle))
            }
        }
    }
}

@Composable
private fun LocationServicesDisabled(enableAction: () -> Unit) {
    ActionRequired(
        icon = Icons.Filled.LocationDisabled,
        contentDescription = stringResource(Res.string.locationServicesDisabledContentDescription),
        description = stringResource(Res.string.locationServicesDisabledDescription),
        buttonText = stringResource(Res.string.locationServicesDisabledButtonText),
        onClick = enableAction,
    )
}

@Composable
private fun BluetoothPermissionsDenied(onShowAppSettingsClick: () -> Unit) {
    ActionRequired(
        icon = Icons.Filled.Warning,
        contentDescription = stringResource(Res.string.bluetoothPermissionsDeniedContentDescription),
        description = stringResource(Res.string.bluetoothPermissionsDeniedDescription),
        buttonText = stringResource(Res.string.bluetoothPermissionsDeniedButtonText),
        onClick = onShowAppSettingsClick,
    )
}

@Composable
private fun AdvertisementsList(
    scanState: DeviceLocator.State,
    advertisements: List<PlatformAdvertisement>,
    onScanClick: () -> Unit,
    onAdvertisementClick: (PlatformAdvertisement) -> Unit,
) {
    when {
        scanState == NotYetScanned -> Scan(message = null, onScanClick)

        // Scanning or Scanned w/ advertisements found.
        advertisements.isNotEmpty() -> LazyColumn {
            items(advertisements.size) { index ->
                val advertisement = advertisements[index]
                AdvertisementRow(advertisement) { onAdvertisementClick(advertisement) }
            }
        }

        // Scanning w/ no advertisements yet found.
        scanState == Scanning -> Loading()

        // Scanned w/ no advertisements found.
        else -> Scan("No devices found.", onScanClick)
    }
}

@Composable
private fun AdvertisementRow(advertisement: PlatformAdvertisement, onClick: () -> Unit) {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(20.dp)
            .clickable(onClick = onClick)
    ) {
        Column(Modifier.weight(1f)) {
            Text(
                fontSize = 22.sp,
                text = advertisement.name ?: "Unknown",
            )
            Text(advertisement.identifier.toString())
        }

        Text(
            modifier = Modifier.align(CenterVertically),
            text = "${advertisement.rssi} dBm",
        )
    }
}

