package com.plusot.mire.permissions

suspend fun PermissionsController.requestPermission(permission: Permission) = try {
    providePermission(permission)
    PermissionState.Granted
} catch (e: DeniedAlwaysException) {
    PermissionState.DeniedAlways
} catch (e: DeniedException) {
    PermissionState.Denied
} catch (e: Exception) {
    // RequestCanceledException
    null
}
