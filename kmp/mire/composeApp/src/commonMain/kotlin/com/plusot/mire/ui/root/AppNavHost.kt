package com.plusot.mire.ui.root

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.plusot.mire.ui.ScreenState
import com.plusot.mire.ui.about.AboutView
import com.plusot.mire.ui.graph.GraphView
import com.plusot.mire.ui.graph.LiveTimeChart
import com.plusot.mire.ui.home.HomeView
import com.plusot.mire.ui.scan.ScanScreen

@Composable
fun AppNavHost(
  navController: NavHostController,
  modifier: Modifier = Modifier,
) {
  NavHost(
    navController = navController,
    startDestination = ScreenState.Home.route,
    modifier = modifier,
  ) {
    composable(ScreenState.Home.route) {
      HomeView()
    }

    composable(ScreenState.Scan.route) {
      cafe.adriel.voyager.navigator.Navigator(ScanScreen())
    }

    composable(ScreenState.Graph.route) {
      //GraphView()
      LiveTimeChart(false)
    }

    composable(ScreenState.About.route) {
      AboutView(
        navController = navController
      )
    }
  }
}