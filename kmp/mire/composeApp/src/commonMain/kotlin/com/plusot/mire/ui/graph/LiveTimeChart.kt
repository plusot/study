package com.plusot.mire.ui.graph

import androidx.compose.animation.core.TweenSpec
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.unit.dp
import com.plusot.mire.data.formatTime
import io.github.koalaplot.core.ChartLayout
import io.github.koalaplot.core.Symbol
import io.github.koalaplot.core.legend.LegendLocation
import io.github.koalaplot.core.line.LinePlot
import io.github.koalaplot.core.style.KoalaPlotTheme
import io.github.koalaplot.core.style.LineStyle
import io.github.koalaplot.core.util.ExperimentalKoalaPlotApi
import io.github.koalaplot.core.util.VerticalRotation
import io.github.koalaplot.core.util.generateHueColorPalette
import io.github.koalaplot.core.util.rotateVertically
import io.github.koalaplot.core.xygraph.CategoryAxisModel
import io.github.koalaplot.core.xygraph.DefaultPoint
import io.github.koalaplot.core.xygraph.FloatLinearAxisModel
import io.github.koalaplot.core.xygraph.XYGraph
import io.github.koalaplot.core.xygraph.XYGraphScope
import io.github.koalaplot.core.xygraph.rememberAxisStyle
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.mutate
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toImmutableList
import kotlinx.collections.immutable.toPersistentList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random

//val liveTimeChartSampleView = object : SampleView {
//    override val name: String = "Live Time Chart"
//
//    override val thumbnail = @Composable {
//        ThumbnailTheme {
//            LiveTimeChart(true)
//        }
//    }
//
//    override val content: @Composable () -> Unit = @Composable {
//        LiveTimeChart(false)
//    }
//}

data class GraphData<X, Y : Comparable<Y>>(
    val xAxis: ImmutableList<X>,
//    val yAxis: ImmutableList<Y>,
    val points: Map<String, ImmutableList<DefaultPoint<X, Y>>>,
    val yRange: ClosedFloatingPointRange<Y>,
)

private const val HistorySize = 20
private const val UpdateDelay = 500L
private val ids = listOf("x", "y", "z")


private fun generateColorMap(ids: List<String>) = buildMap {
    val colors = generateHueColorPalette(ids.size)
    var i = 0
    ids.forEach {
        put(it, colors[i++])
    }
}

@OptIn(ExperimentalKoalaPlotApi::class)
@Composable
private fun XYGraphScope<String, Float>.chart(
    id: String,
    data: List<DefaultPoint<String, Float>>,
    thumbnail: Boolean
) {
    val colorMap = generateColorMap(ids);
    LinePlot(
        data, //info.value.points,
        animationSpec = TweenSpec(0),
        lineStyle = LineStyle(
            brush = SolidColor(colorMap[id] ?: Color.Black ),
            pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 20f), 4f),
            strokeWidth = 2.dp
        ),
        symbol = { point ->
            Symbol(
                shape = CircleShape,
                fillBrush = SolidColor(colorMap[id] ?: Color.Black),
                modifier = Modifier.then(
                    if (!thumbnail) {
                        Modifier.hoverableElement {
                            HoverSurface { Text(point.y.toString()) }
                        }
                    } else {
                        Modifier
                    }
                )
            )
        }
    )
}

@OptIn(ExperimentalKoalaPlotApi::class)
@Composable
fun LiveTimeChart(thumbnail: Boolean) {
    val info = remember {
        val x = Clock.System.now().toEpochMilliseconds()
        val y = 0f
        val map = buildMap<String, ImmutableList<DefaultPoint<String, Float>>> {
            ids.forEach { id ->
                put(
                    id,
                    persistentListOf(
                        DefaultPoint(
                            x.formatTime(), y
                        )
                    )
                )
            }
        }
        mutableStateOf(
            GraphData(
                persistentListOf(Instant.fromEpochMilliseconds(x).formatTime()),
//                persistentListOf(y),
                map,
                -1f..1f
            )
        )
    }

    LaunchedEffect(thumbnail) {
        withContext(Dispatchers.Main) {
            var count = 0
            while (isActive) {
                count++
                delay(UpdateDelay)
                val yNext = buildMap<String, Float> {
                    ids.forEach { id ->
                        put(id, (info.value.points[id]?.last()?.y ?: 0f) + 0.5f - Random.nextFloat())
                    }
                }

                val x = Clock.System.now().toEpochMilliseconds()

                info.value = info.value.copy(
                    info.value.xAxis.toPersistentList().mutate {
                        it.add(Instant.fromEpochMilliseconds(x).formatTime())
                    }.takeLast(HistorySize).toImmutableList(),
//                    info.value.yAxis.toPersistentList().mutate {
//                        it.add(yNext.toFloat())
//                    }.takeLast(HistorySize).toImmutableList(),
                    buildMap {
                        ids.forEach { id ->
                            info.value.points[id]?.toPersistentList()?.mutate { list ->
                                list.add(
                                    DefaultPoint(
                                        Instant.fromEpochMilliseconds(x).formatTime(),
                                        yNext[id] ?: 0f
                                    )
                                )
                            }?.takeLast(HistorySize)?.toImmutableList()?.let { list -> put(id, list) }
                        }
                    },
                    min(info.value.yRange.start, yNext.values.min())..max(info.value.yRange.endInclusive, yNext.values.max())
                )
            }
        }
    }

    ChartLayout(
        title = { ChartTitle("Live Time Chart") },
        legendLocation = LegendLocation.NONE,
    ) {
        XYGraph(
            xAxisModel = CategoryAxisModel(info.value.xAxis),
            yAxisModel = FloatLinearAxisModel(
                range = info.value.yRange,
                minimumMajorTickSpacing = 50.dp,
            ),
            yAxisLabels = {
                if (!thumbnail) {
                    AxisLabel(it.toString())
                }
            },
            xAxisLabels = {
                if (!thumbnail) {
                    AxisLabel(it, Modifier.padding(top = 2.dp))
                }
            },
            yAxisTitle = {
                AxisTitle("y-axis", Modifier
                    .rotateVertically(VerticalRotation.COUNTER_CLOCKWISE)
                    .padding(bottom = KoalaPlotTheme.sizes.gap))
            },
            xAxisTitle = { AxisTitle("x-axis") },
            xAxisStyle = rememberAxisStyle(labelRotation = 45),
        ) {
            ids.forEach { id ->
                info.value.points[id]?.let {
                    points -> chart(id, points, thumbnail)
                }
            }
//            LinePlot(
//                info.value.points,
//                symbol = { Symbol(fillBrush = SolidColor(Color.Black)) },
//                animationSpec = TweenSpec(0),
//                lineStyle = LineStyle(
//                    brush = SolidColor(Color.Red),
//                    pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 20f), 4f),
//                    strokeWidth = 2.dp
//                ),
//            )
        }
    }
}
