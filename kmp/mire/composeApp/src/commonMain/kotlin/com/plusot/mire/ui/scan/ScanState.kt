package com.plusot.mire.ui.scan

enum class ScanState { Scanning, Scanned }
