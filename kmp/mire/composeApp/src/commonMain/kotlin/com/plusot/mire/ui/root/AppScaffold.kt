package com.plusot.mire.ui.root

import MainTopAppBar
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.BottomSheetScaffold
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController

@Composable
fun AppScaffold() {
  val navController = rememberNavController()

  val bottomNavigationItems = listOf(
    BottomNavigationScreens.Home,
    BottomNavigationScreens.Scan,
    BottomNavigationScreens.Graph,
    BottomNavigationScreens.Info
  )
//  BottomSheetScaffold(
//    sheetContent =
//  ) {
  Scaffold (
    topBar = {
      MainTopAppBar(navController)
    },
    content = {
      AppNavHost(
        navController = navController,
        modifier = Modifier
          .fillMaxSize()
          .padding(it),
      )
    },
    bottomBar = {
      MainBottomBar(
        navController = navController,
        items = bottomNavigationItems
      )
    }
  )
//  }
}