package com.plusot.mire.ui.sensor

import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import com.juul.kable.State
import com.plusot.mire.bluetooth.peripheral
import com.plusot.mire.bluetooth.requirements.BluetoothRequirements
import com.plusot.mire.bluetooth.requirements.Deficiency.BluetoothOff
import com.plusot.mire.device.ble.NanoSense
import com.plusot.mire.ui.sensor.chart.Sample
import com.plusot.mire.util.SimpleLogger
import com.plusot.mire.util.coroutines.flow.withStartTime
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.scan
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

private val reconnectDelay = 1.seconds

class SensorScreenModel(
    bluetoothRequirements: BluetoothRequirements,
) : ScreenModel {

    private val nanoSense = peripheral?.let(::NanoSense) ?: error("Peripheral not set")

    @OptIn(ExperimentalCoroutinesApi::class)
    val state = bluetoothRequirements.deficiencies
        .map { BluetoothOff in it }
        .distinctUntilChanged()
        .flatMapLatest { isBluetoothOff ->
            if (isBluetoothOff) {
                flowOf(ViewState.BluetoothOff)
            } else {
                nanoSense.state.flatMapLatest { state ->
                    SimpleLogger.info { "!!! State is now $state"}
                    when (state) {
                        is State.Connecting -> flowOf(ViewState.Connecting)
                        is State.Connected -> combine(
                            nanoSense.battery,
                            nanoSense.rssi,
                            nanoSense.periodMillis,
                            ViewState::Connected,
                        )

                        is State.Disconnecting -> flowOf(ViewState.Disconnecting)
                        is State.Disconnected -> flowOf(ViewState.Disconnected)
                    }
                }
            }
        }
        .stateIn(screenModelScope, SharingStarted.WhileSubscribed(), ViewState.Connecting)

    val data = nanoSense.rgb
        .withStartTime()
        .scan(emptyList<Sample>()) { accumulator, (start, value) ->
            val t = start.elapsedNow().inWholeMilliseconds / 1_000f
            accumulator.takeLast(50) + Sample(t, value.x, value.y, value.z)
        }

    init {
        onDisconnected {
            SimpleLogger.info { "!!! Waiting $reconnectDelay to reconnect..." }
            delay(reconnectDelay)
            nanoSense.connect()
        }
        state.onEach {
            SimpleLogger.info { "!!! State: $it" }
        }.launchIn(screenModelScope)
    }

    fun setPeriod(period: Duration) {
        screenModelScope.launch {
            nanoSense.setPeriod(period)
        }
    }

    override fun onDispose() {
        // GlobalScope to allow disconnect process to continue after leaving screen.
        SimpleLogger.info { "!!! Disposing" }
        @OptIn(DelicateCoroutinesApi::class)
        GlobalScope.launch {
            nanoSense.disconnect()
        }
    }

    private fun onDisconnected(action: suspend (ViewState.Disconnected) -> Unit) {
        SimpleLogger.info { "!!! onDisconnected" }
        state
            .filterIsInstance<ViewState.Disconnected>()
            .onEach(action)
            .launchIn(screenModelScope)
    }
}
