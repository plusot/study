package com.plusot.mire

import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowPosition
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import org.koin.dsl.module

fun main() = application {
    val state = rememberWindowState(
        size = DpSize(600.dp, 900.dp),
        position = WindowPosition(200.dp, 200.dp)
    )
    Window(
        onCloseRequest = ::exitApplication,
        title = "Mire",
        state = state
    ) {
        App(module { /*single { BleModule() }*/ } )
    }
}