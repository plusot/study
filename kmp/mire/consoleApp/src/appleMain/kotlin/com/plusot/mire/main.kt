package com.plusot.mire

import kotlinx.coroutines.runBlocking

fun main() = runBlocking<Unit> { headlessApp() }
