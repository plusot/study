// To release gradle locks: find ~/.gradle -type f -name "*.lock" -delete

plugins {
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.compose)
    alias(libs.plugins.compose.compiler)
}

kotlin {
    jvmToolchain(11)

    macosX64 {
        binaries {
            executable {
                baseName = "mire"
                entryPoint = "com.plusot.mire.main"
            }
        }
    }
    macosArm64 {
        binaries {
            executable {
                baseName = "mire"
                entryPoint = "com.plusot.mire.main"
            }
        }
    }

    sourceSets {
        all {
            languageSettings.optIn("kotlin.ExperimentalStdlibApi")
        }

        commonMain.dependencies {
            implementation(compose.runtime)
            implementation(libs.kotlinx.coroutines.core)
            implementation(libs.kable)
            implementation(projects.shared)
        }
    }
}
