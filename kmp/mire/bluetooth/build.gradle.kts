plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.compose)
    alias(libs.plugins.compose.compiler)
    alias(libs.plugins.kotlin.multiplatform)
}

kotlin {
    explicitApi()
    jvmToolchain(libs.versions.jvm.get().toInt())

    androidTarget()
    iosArm64()
    // js().browser()
    macosX64()
    macosArm64()

    jvm()

    sourceSets {
        commonMain.dependencies {
            api(libs.kotlinx.coroutines.core)
            api(projects.permissions)
        }

        androidMain.dependencies {
            implementation(libs.androidx.activity.compose)
            implementation(libs.tuulbox.coroutines)
        }
    }
}

android {
    namespace = "com.plusot.mire.bluetooth"
    compileSdk = libs.versions.android.compileSdk.get().toInt()
    defaultConfig.minSdk = libs.versions.android.minSdk.get().toInt()
    buildFeatures.compose = true
}
