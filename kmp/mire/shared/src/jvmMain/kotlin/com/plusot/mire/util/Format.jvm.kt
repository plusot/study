package com.plusot.mire.util

import java.util.Locale


actual fun formatString(
    format: String,
    vararg args: Any?
): String {
    SimpleLogger.info(format)
    return String.format(Locale.getDefault(), format, args)
}