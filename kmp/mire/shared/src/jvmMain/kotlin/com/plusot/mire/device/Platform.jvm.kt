package com.plusot.mire.device

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.jdbc.sqlite.JdbcSqliteDriver
import com.plusot.mire.MireDb
import com.plusot.mire.util.SimpleLogger
import com.russhwolf.settings.PreferencesSettings
import com.russhwolf.settings.Settings
import org.koin.dsl.module
import java.awt.Toolkit
import java.util.prefs.Preferences

class JVMPlatform: Platform {
    override val osName = (System.getProperty("os.name") ?: "Desktop") + ", Java ${System.getProperty("java.version")}"

    override val osVersion = System.getProperty("os.version") ?: "---"

    override val deviceModel = "Desktop"

    override val cpuType = System.getProperty("os.arch") ?: "---"

    override val screen = ScreenInfo()

    override fun logSystemInfo() {
        print(deviceInfo)
    }
}

actual class ScreenInfo actual constructor() {
    private val toolkit = Toolkit.getDefaultToolkit()

    actual val width = toolkit.screenSize.width
    actual val height = toolkit.screenSize.height
    actual val density: Int? = null
}

actual fun getPlatform(): Platform = JVMPlatform()
actual val platformModule = module {
    single {
        Preferences.userRoot()
    }
    single<Settings> {
        PreferencesSettings(get())
    }
    single<SqlDriver> {
        val driver = JdbcSqliteDriver("jdbc:sqlite:MireDb.db")
        try {
            MireDb.Schema.create(driver)
        } catch (e: Exception) {
            SimpleLogger.error("Could not create schema", e)
            try {
                MireDb.Schema.migrate(driver, 1, 2)
                SimpleLogger.info("Schema migrated")
            } catch (e: Exception) {
                SimpleLogger.error("Could not migrate schema", e)
            }
        }
        driver
    }
}
