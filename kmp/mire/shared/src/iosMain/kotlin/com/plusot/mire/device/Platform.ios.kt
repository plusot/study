package com.plusot.mire.device

import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import org.koin.dsl.module
import platform.CoreGraphics.CGRectGetHeight
import platform.CoreGraphics.CGRectGetWidth
import platform.Foundation.NSLog
import platform.Foundation.NSString
import platform.Foundation.NSUTF8StringEncoding
import platform.Foundation.stringWithCString
import platform.UIKit.UIDevice
import platform.UIKit.UIScreen
import platform.UIKit.UIUserInterfaceIdiomPad
import platform.UIKit.UIUserInterfaceIdiomPhone
import platform.posix.uname
import platform.posix.utsname
import kotlin.experimental.ExperimentalNativeApi
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.native.NativeSqliteDriver
import com.plusot.mire.MireDb

class IOSPlatform: Platform {
    //override val osName: String = UIDevice.currentDevice.systemName() + " " + UIDevice.currentDevice.systemVersion

    @OptIn(ExperimentalNativeApi::class)
    override val osName = when (UIDevice.currentDevice.userInterfaceIdiom) {
        UIUserInterfaceIdiomPhone -> UIDevice.currentDevice.systemName()
        UIUserInterfaceIdiomPad -> "iPadOS"
        else -> kotlin.native.Platform.osFamily.name
    }

    override val osVersion = UIDevice.currentDevice.systemVersion

    @OptIn(ExperimentalForeignApi::class)
    override val deviceModel: String
        get() {
            memScoped {
                val systemInfo: utsname = alloc()
                uname(systemInfo.ptr)
                return NSString.stringWithCString(
                    systemInfo.machine,
                    encoding = NSUTF8StringEncoding
                ) ?: "---"
            }
        }

    @OptIn(ExperimentalNativeApi::class)
    override val cpuType = kotlin.native.Platform.cpuArchitecture.name

    override val screen = ScreenInfo()

    override fun logSystemInfo() {
        NSLog(deviceInfo)
    }

}

@OptIn(ExperimentalForeignApi::class)
actual class ScreenInfo actual constructor() {
    actual val width = CGRectGetWidth(UIScreen.mainScreen.nativeBounds).toInt()
    actual val height = CGRectGetHeight(UIScreen.mainScreen.nativeBounds).toInt()
    actual val density: Int? = UIScreen.mainScreen.scale.toInt()
}

actual fun getPlatform(): Platform = IOSPlatform()
actual val platformModule = module {
    single<SqlDriver> {
        NativeSqliteDriver(MireDb.Schema, "MireDb")
    }
}

