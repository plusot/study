package com.plusot.mire.util

import platform.Foundation.NSString
import platform.Foundation.stringWithFormat
import kotlin.collections.toTypedArray
import platform.Foundation.NSNull

actual fun formatString(
    format: String,
    vararg args: Any?
): String {
    //return NSString.stringWithFormat(format, *arrayOf(args))
    val convertedArgs = args.map { it?.toString() as NSString? ?: NSNull() }
    return NSString.stringWithFormat(format, convertedArgs)
}