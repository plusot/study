package com.plusot.mire.data.model

import platform.Foundation.NSUUID

actual class UUID {
    private val value = NSUUID()
    actual override fun toString() = value.UUIDString
}