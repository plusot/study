package com.plusot.mire.data.model

import kotlinx.serialization.Serializable

@Serializable
data class Measurement(
    val time: Long,
    val device: String,
    val value: Double,
    val name: String,
    val isStored: Boolean = false,
    val unit: String
)

//@Parcelize
@Serializable
data class SerializableMeasurement(
    val time: Long,
    val device: String,
    val value: Double,
    val name: String,
    val isStored: Boolean = false,
    val unit: ParameterUnit
)
