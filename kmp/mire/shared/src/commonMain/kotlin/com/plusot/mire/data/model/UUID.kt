package com.plusot.mire.data.model

expect class UUID() {
    override fun toString(): String
}