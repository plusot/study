package com.plusot.mire.data

import app.cash.sqldelight.db.SqlDriver
import com.plusot.mire.data.model.Measurement
import com.plusot.mire.MireDb
import com.plusot.mire.data.model.ParameterUnit
import com.plusot.mire.db.MeasurementsTable

class DbHelper (
    sqlDriver: SqlDriver ) {
    private val dbRef: MireDb = MireDb(sqlDriver)

    fun fetchAllItems(): List<MeasurementsTable> = dbRef
        .measurementsTableQueries
        .selectAll()
        .executeAsList()

    fun fetchByTime(time: Long): List<MeasurementsTable> = dbRef
        .measurementsTableQueries
        .selectByTime(time)
        .executeAsList()

    fun insertMeasurement(time: Long, device: String, value: Double, name: String) {
        dbRef.measurementsTableQueries.insertMeasurement(time, device, value, name)
    }

    fun updateIsStored(id: Int, isStored: Boolean) {
        dbRef.measurementsTableQueries.updateIsStored(isStored.toLong(), id.toLong())
    }
}

fun MeasurementsTable.isStored(): Boolean = this.isStored != 0L
fun MeasurementsTable.map(): Measurement = Measurement(this.time, this.device, this.param_value, this.param_name, this.isStored(), this.param_unit ?: ParameterUnit.UNKNOWN.name)

internal fun Boolean.toLong(): Long = if (this) 1L else 0L