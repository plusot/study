package com.plusot.mire.data.model

import MeasurementSerializer
import kotlinx.serialization.Serializable

@Serializable(with = MeasurementSerializer::class)
enum class ParameterUnit(val value: String) {
    SQUARE_METERS("m²"),
    NONE(""),
    UNKNOWN("-")
}

