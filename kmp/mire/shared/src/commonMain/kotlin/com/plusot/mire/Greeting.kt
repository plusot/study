package com.plusot.mire

import com.plusot.mire.data.TimeHelperImpl
import com.plusot.mire.device.getPlatform

class Greeting {
    private val platform = getPlatform()
    private val timeHelper = TimeHelperImpl()

    fun greet(): String {
        return "${platform.osName} at ${timeHelper.currentTimeString()}!"
    }
}