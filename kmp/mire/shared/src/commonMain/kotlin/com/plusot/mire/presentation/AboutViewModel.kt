package com.plusot.mire.presentation

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.plusot.mire.device.Platform
import com.plusot.mire.data.formatDateTime
import com.plusot.mire.data.formatFullDateTime
import com.plusot.mire.network.MireApi
import com.russhwolf.settings.Settings
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlin.math.max
import kotlin.math.min

class AboutViewModel(platform: Platform, settings: Settings, mireApi: MireApi) : BaseViewModel() {
    private var serverGreeting: String = "Server Greeting"
    val items = mutableStateOf(makeRowItems(platform))
    val firstOpened: String

    init {
        val timestampKey = "FIRST_OPENED_TIMESTAMP"

        val savedValue = settings.getLongOrNull(timestampKey)

        firstOpened = if (savedValue == null) {
            val time = Clock.System.now().toEpochMilliseconds()
            settings.putLong(timestampKey, time)
            time.formatDateTime()
        } else {
            "${savedValue.formatDateTime()}\n${savedValue.formatFullDateTime()}"
        }
        viewModelScope.launch {
            serverGreeting = mireApi.getServerGreeting()
            items.value = makeRowItems(platform)
        }
    }

    private fun makeRowItems(platform: Platform): List<RowItem> {
        val rowItems = mutableListOf(
            RowItem("Operating System", "${platform.osName} ${platform.osVersion}"),
            RowItem("Device", platform.deviceModel),
            RowItem("CPU", platform.cpuType),
            RowItem("Server", serverGreeting),
        )

        val max = max(platform.screen.width, platform.screen.height)
        val min = min(platform.screen.width, platform.screen.height)

        var displayInfo = "${max}×${min}"
        platform.screen.density?.let {
            displayInfo += " ${it}x"
        }

        rowItems.add(
            RowItem("Display", displayInfo)
        )

        return rowItems
    }

    data class RowItem(
        val title: String,
        val subtitle: String,
    )
}