package com.plusot.mire.presentation

import com.plusot.mire.data.MeasurementRepository
import com.plusot.mire.data.model.Measurement

class HomeViewModel(private val repository: MeasurementRepository)  : BaseViewModel() {

    private val measurements: List<Measurement>
        get() = repository.measurements

    var onMeasurementsUpdated: ((List<Measurement>) -> Unit)? = null
        set(value) {
            field = value
            onMeasurementsUpdated?.invoke(measurements)
        }

    fun addMeasurement(time: Long,
                       device: String = "app",
                       value: Double = 0.0,
                       name: String = "unknown"
    ) {
        repository.addMeasurement(time, device, value, name)
        onMeasurementsUpdated?.invoke(measurements)
    }

    fun markMeasurement(time: Long, isStored: Boolean) {
        repository.markMeasurement(time = time, isStored = isStored)
        onMeasurementsUpdated?.invoke(measurements)
    }
}