package com.plusot.mire.data

import com.plusot.mire.data.model.TimeHelper
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

class TimeHelperImpl: TimeHelper {
    override fun currentTime(): Long {
        val currentMoment: Instant = Clock.System.now()
        return currentMoment.toEpochMilliseconds()
    }

    override fun currentTimeString(): String {
        val currentMoment: Instant = Clock.System.now()
        val dateTime: LocalDateTime = currentMoment.toLocalDateTime(TimeZone.currentSystemDefault())
        return formatTime(dateTime)
    }

    override fun formatTime(dateTime: LocalDateTime): String {
        val stringBuilder = StringBuilder()
        val minute = dateTime.minute
        val second = dateTime.second
        val hour = dateTime.hour
        stringBuilder.append(hour.toString())
        stringBuilder.append(":")
        if (minute < 10) {
            stringBuilder.append('0')
        }
        stringBuilder.append(minute.toString())
        stringBuilder.append(":")
        if (second < 10) {
            stringBuilder.append('0')
        }
        stringBuilder.append(second.toString())
        return stringBuilder.toString()
    }

    override fun formatTime(time: Long): String {
        val instant = Instant.fromEpochMilliseconds(time)
        val dateTime: LocalDateTime = instant.toLocalDateTime(TimeZone.currentSystemDefault())
        return formatTime(dateTime)
    }

    override fun formatTime(instant: Instant): String {
        val dateTime: LocalDateTime = instant.toLocalDateTime(TimeZone.currentSystemDefault())
        return formatTime(dateTime)
    }

    override fun formatDateTime(dateTime: LocalDateTime): String {
        val stringBuilder = StringBuilder()
        val day = dateTime.dayOfMonth
        val month = dateTime.monthNumber
        val year = dateTime.year
        stringBuilder.append(year.toString())
        stringBuilder.append("-")
        if (month < 10) {
            stringBuilder.append('0')
        }
        stringBuilder.append(month.toString())
        stringBuilder.append("-")
        if (day < 10) {
            stringBuilder.append('0')
        }
        stringBuilder.append(day.toString())
        stringBuilder.append(" ")
        stringBuilder.append(formatTime(dateTime))
        return stringBuilder.toString()
    }

    override fun formatDateTime(time: Long): String {
        val instant = Instant.fromEpochMilliseconds(time)
        val dateTime: LocalDateTime = instant.toLocalDateTime(TimeZone.currentSystemDefault())
        return formatDateTime(dateTime)
    }

    override fun formatFullDateTime(dateTime: LocalDateTime): String {
        val stringBuilder = StringBuilder()
        val day = dateTime.dayOfMonth
        val year = dateTime.year
        stringBuilder.append(dateTime.dayOfWeek.toString().lowercase().replaceFirstChar { it.uppercase() } )
        stringBuilder.append(", ")
        stringBuilder.append(dateTime.month.toString().lowercase().replaceFirstChar { it.uppercase() })
        stringBuilder.append(" ")
        stringBuilder.append(day.toString())
        stringBuilder.append(", ")
        stringBuilder.append(year.toString())

        stringBuilder.append(" ")
        stringBuilder.append(formatTime(dateTime))
        return stringBuilder.toString()
    }

    override fun formatFullDateTime(time: Long): String {
        val instant = Instant.fromEpochMilliseconds(time)
        val dateTime: LocalDateTime = instant.toLocalDateTime(TimeZone.currentSystemDefault())
        return formatFullDateTime(dateTime)
    }

}

fun Long.formatDateTime(): String = TimeHelperImpl().formatDateTime(this)
fun Long.formatFullDateTime(): String = TimeHelperImpl().formatFullDateTime(this)
fun Long.formatTime(): String = TimeHelperImpl().formatTime(this)
fun Instant.formatTime(): String = TimeHelperImpl().formatTime(this)