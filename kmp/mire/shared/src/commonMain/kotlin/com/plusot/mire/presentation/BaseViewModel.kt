package com.plusot.mire.presentation

import androidx.lifecycle.ViewModel

abstract class BaseViewModel() : ViewModel()