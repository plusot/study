package com.plusot.mire.util

import kotlin.math.abs
import kotlin.math.log10
import kotlin.math.max
import kotlin.math.round

expect fun formatString(format: String, vararg args: Any?): String

fun Double.format(decimals: Int): String {
    if (decimals < 0) return format()
    val test = this.toString()
    val separator =  if (test.contains(",")) "," else "."
    val parts = test.split(separator)
    val end = (parts.last() + "0".repeat(20)).substring(0, decimals)

    return if (end.isEmpty()) parts.first() else parts.first() + separator + end
}

fun Double.format(): String {
    val power = log10(abs(this))
    val decimals = if (power > 2.5)
        0
    else
        max(0, 3 - round(power).toInt())

    val test = this.toString()
    val separator =  if (test.contains(",")) "," else "."
    val parts = test.split(separator)
    SimpleLogger.info("format: $power, $decimals, $test, $separator, ${parts.last()}")

    val end = (parts.last() + "0".repeat(20)).substring(0, decimals)

    return if (end.isEmpty()) parts.first() else parts.first() + separator + end
}