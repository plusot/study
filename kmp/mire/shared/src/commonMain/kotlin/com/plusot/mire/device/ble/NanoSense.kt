package com.plusot.mire.device.ble

import com.benasher44.uuid.Uuid
import com.benasher44.uuid.uuidFrom
import com.juul.kable.Bluetooth
import com.juul.kable.ExperimentalApi
import com.juul.kable.Filter
import com.juul.kable.Peripheral
import com.juul.kable.Scanner
import com.juul.kable.characteristicOf
import com.juul.kable.logs.Logging.Level
import com.plusot.mire.util.SimpleLogger
import com.plusot.mire.util.Vector3f
import com.plusot.mire.util.times
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.io.IOException
import kotlin.coroutines.coroutineContext
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

private val nanoSenseServiceUuid = nanoSenseUuid("EA00") //""aa80")
private val nanoSenseColorCharacteristicUuid = nanoSenseUuid("EA01") //""aa81")
private val nanoSenseWeatherCharacteristicUuid = nanoSenseUuid("2902")
private val clientCharacteristicConfigUuid = Bluetooth.BaseUuid + 0x2902

private val colorCharacteristic = characteristicOf(
    service = nanoSenseServiceUuid,
    characteristic = nanoSenseColorCharacteristicUuid,
)

private val weatherCharacteristic = characteristicOf(
    service = nanoSenseServiceUuid,
    characteristic = nanoSenseWeatherCharacteristicUuid,
)

class NanoSense(private val peripheral: Peripheral)  {
    private val rssiInterval = 5.seconds

    companion object {
        val nanoSenseUuid = uuidFrom("19D0EA00-E8F2-537E-4F6C-D104768A1214".lowercase()) //0000aa80-0000-1000-8000-00805f9b34fb")
        val PeriodRange = 100.milliseconds..2550.milliseconds

        val services = listOf(
            nanoSenseServiceUuid,
            nanoSenseColorCharacteristicUuid,
            nanoSenseWeatherCharacteristicUuid,
            clientCharacteristicConfigUuid,
        )

        val scanner by lazy {
            Scanner {
                logging {
                    level = Level.Data // Events
                }
                filters {
                    match {
                        name = Filter.Name.Exact("NanoSense") //= listOf(nanoSenseUuid)
                    }
                }
            }
        }
    }

    val state = peripheral.state
    private val _rssi = MutableStateFlow<Int?>(null)
    val rssi = _rssi.asStateFlow()

    // TODO: remove these:
    val battery = flowOf(0)
    private val _periodMillis = MutableStateFlow<Duration?>(null)
    val periodMillis = _periodMillis.filterNotNull()
    suspend fun setPeriod(period: Duration) {
        writeGyroPeriod(period)
        _periodMillis.value = period
    }
    // end remove

    val rgb: Flow<Vector3f> = peripheral
        .observe(colorCharacteristic)
        .map(::Vector3f)
        .map { it * 1.0f }

    suspend fun connect() {
        SimpleLogger.info { "Connecting" }

        try {
            peripheral.connect().launch { monitorRssi() }
            _periodMillis.value = readGyroPeriod()
            enableGyro()
            SimpleLogger.info { "!!! Connected" }
        } catch (e: IOException) {
            SimpleLogger.warn(e) { "!!! Connection attempt failed" }
            peripheral.disconnect()
        }
    }

    suspend fun disconnect() {
        peripheral.disconnect()
    }

    private suspend fun monitorRssi() {
        try {
            while (coroutineContext.isActive) {
                @OptIn(ExperimentalApi::class)
                _rssi.value = peripheral.rssi()

                SimpleLogger.debug { "RSSI: ${_rssi.value}" }
                delay(rssiInterval)
            }
        } catch (e: UnsupportedOperationException) {
            // As of Chrome 128, RSSI is not yet supported (even with
            // `chrome://flags/#enable-experimental-web-platform-features` flag enabled).
            SimpleLogger.warn(e) { "RSSI is not supported" }
        }
    }

    private suspend fun writeGyroPeriod(period: Duration) {
        require(period in PeriodRange) { "Period must be in the range $PeriodRange, was $period." }

        val value = period.inWholeMilliseconds / 10
        val data = byteArrayOf(value.toByte())

        SimpleLogger.verbose { "Writing gyro period" }
        //peripheral.write(movementPeriodCharacteristic, data, WithResponse)
        SimpleLogger.info { "Writing gyro period complete" }
    }

    suspend fun readGyroPeriod(): Duration {
        //val value = peripheral.read(movementPeriodCharacteristic)
        SimpleLogger.info { "movement → readPeriod → value = UNKNOWN" } //${value.toHexString(UpperCase)}" }
        //return value[0].toInt() and 0xFF * 10
        return 0.milliseconds
    }

    private suspend fun enableGyro() {
        SimpleLogger.info { "Enabling gyro" }
        //peripheral.write(movementConfigCharacteristic, byteArrayOf(0x7F, 0x0), WithResponse)
        SimpleLogger.info { "Gyro enabled" }
    }



}

private fun nanoSenseUuid(short16BitUuid: String): Uuid =
    uuidFrom("19D0${short16BitUuid}-E8F2-537E-4F6C-D104768A1214".lowercase())

private fun characteristicOf(service: Uuid, characteristic: Uuid) =
    characteristicOf(service.toString(), characteristic.toString())
