package com.plusot.mire.network

import com.plusot.mire.util.SimpleLogger
import io.ktor.client.plugins.logging.Logger

object HttpClientLogger : Logger {

    override fun log(message: String) {
        SimpleLogger.info(message)
    }
}