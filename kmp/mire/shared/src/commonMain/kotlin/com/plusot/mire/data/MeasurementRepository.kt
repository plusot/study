package com.plusot.mire.data

import com.plusot.mire.data.model.Measurement
import com.plusot.mire.db.MeasurementsTable

class MeasurementRepository(
    private val dbHelper: DbHelper
) {
    private val _measurements: List<Measurement> //= mutableListOf()
        get() = dbHelper.fetchAllItems().map(MeasurementsTable::map)

    val measurements: List<Measurement>
        get() = _measurements

    fun addMeasurement(time: Long,
                       device: String,
                       value: Double,
                       name: String) {
//        val newMeasurement = Measurement(
//            time, device, value, name
//        )
//        _measurements.add(newMeasurement)
        dbHelper.insertMeasurement(
            time, device, value, name
        )
    }

    fun markMeasurement(time: Long, isStored: Boolean) {
        val measurements = dbHelper.fetchByTime(time)
        measurements.forEach {
            dbHelper.updateIsStored(it.id.toInt(), isStored)
        }


//        val index = _measurements.indexOfFirst { it.time == time }
//        if (index != -1) {
//            _measurements[index] = _measurements[index].copy(isStored = isStored)
//        }
    }
}