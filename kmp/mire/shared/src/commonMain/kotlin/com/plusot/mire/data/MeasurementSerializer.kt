import com.plusot.mire.data.model.ParameterUnit
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object MeasurementSerializer : KSerializer<ParameterUnit> {

    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("PARAMETER_UNIT", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: ParameterUnit) {
        encoder.encodeString(value.value.lowercase())
    }

    override fun deserialize(decoder: Decoder): ParameterUnit {
        return try {
            val key = decoder.decodeString()
            findByKey(key)
        } catch (e: IllegalArgumentException) {
            ParameterUnit.UNKNOWN
        }
    }
}

private fun findByKey(key: String, default: ParameterUnit = ParameterUnit.UNKNOWN): ParameterUnit {
    return ParameterUnit.entries.find { it.value.lowercase() == key.lowercase() } ?: default
}