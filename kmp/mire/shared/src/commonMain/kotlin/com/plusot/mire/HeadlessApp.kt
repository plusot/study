package com.plusot.mire

import com.juul.kable.Peripheral
import com.juul.kable.State.Disconnected
import com.juul.kable.logs.Logging.Level.Data
import com.plusot.mire.device.ble.NanoSense
import com.plusot.mire.util.LogLevel
import com.plusot.mire.util.SimpleLogger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.io.IOException
import kotlin.time.Duration.Companion.seconds

suspend fun CoroutineScope.headlessApp() {
    SimpleLogger.tag = "MireTag"
    SimpleLogger.logLevel = LogLevel.DEBUG

    SimpleLogger.info { "Searching for NanoSense..." }
    val advertisement = NanoSense.scanner.advertisements.first()
    SimpleLogger.info { "Found $advertisement" }

    val nanoSense = Peripheral(advertisement) {
        logging {
            level = Data
        }
    }.let(::NanoSense)

    nanoSense.rgb.onEach { rotation ->
        SimpleLogger.info { rotation.toString() }
    }.launchIn(this)

    SimpleLogger.info { "Configuring auto connector" }
    nanoSense.state.onEach { state ->
        SimpleLogger.info { "Received state: $state" }
        if (state is Disconnected) {
            try {
                SimpleLogger.verbose { "Attempting connection" }
                nanoSense.connect()
            } catch (e: IOException) {
                SimpleLogger.error(e) { "Connect failed." }
                throw e
            }
            SimpleLogger.verbose { "Waiting to reconnect" }
            delay(2.seconds) // Throttle reconnects so we don't hammer the system if connection immediately drops.
        }
    }.launchIn(this).apply {
        invokeOnCompletion { cause ->
            SimpleLogger.warn(cause) { "Auto connector complete" }
        }
    }
}
