package com.plusot.mire.data.model

import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime

interface TimeHelper {
    fun currentTime(): Long
    fun currentTimeString(): String
    fun formatDateTime(dateTime: LocalDateTime): String
    fun formatDateTime(time: Long): String
    fun formatFullDateTime(dateTime: LocalDateTime): String
    fun formatFullDateTime(time: Long): String
    fun formatTime(dateTime: LocalDateTime): String
    fun formatTime(time: Long): String
    fun formatTime(instant: Instant): String
}