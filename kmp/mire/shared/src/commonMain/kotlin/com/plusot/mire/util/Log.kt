package com.plusot.mire.util

interface Logger {
    fun error(message: String, cause: Throwable? = null)
    fun error(cause: Throwable? = null, message: () -> String)
    fun warn(message: String, cause: Throwable? = null)
    fun warn(cause: Throwable? = null, message: () -> String)
    fun info(message: String, cause: Throwable? = null)
    fun info(cause: Throwable? = null, message: () -> String)
    fun verbose(cause: Throwable? = null, message: () -> String)
    fun debug(message: String, cause: Throwable? = null)
    fun debug(cause: Throwable? = null, message: () -> String)

}

enum class LogLevel {
    ERROR,
    WARN,
    INFO,
    VERBOSE,
    DEBUG,
}

object SimpleLogger: Logger {
    var tag: String? = null
    var logLevel = LogLevel.INFO

    private val _tag
        get() = "$tag " ?: ""

    override fun error(message: String, cause: Throwable?) {
        println("${_tag}e: $message ${cause ?: ""}")
    }

    override fun error(cause: Throwable?, message: () -> String) {
        if (logLevel >= LogLevel.ERROR) { println("${_tag}e: ${message()} ${cause ?: ""}") }
    }

    override fun warn(message: String, cause: Throwable?) {
        if (logLevel >= LogLevel.WARN) { println("${_tag}w: $message ${cause ?: ""}") }
    }

    override fun warn(cause: Throwable?, message: () -> String) {
        if (logLevel >= LogLevel.WARN) { println("${_tag}e: ${message()} ${cause ?: ""}") }
    }

    override fun info(message: String, cause: Throwable?) {
        if (logLevel >= LogLevel.INFO) { println("${_tag}i: $message ${cause ?: ""}") }
    }

    override fun info(cause: Throwable?, message: () -> String) {
        if (logLevel >= LogLevel.INFO) { println("${_tag}i: ${message()} ${cause ?: ""}") }
    }

    override fun verbose(cause: Throwable?, message: () -> String) {
        if (logLevel >= LogLevel.VERBOSE) { println("${_tag}v: ${message()} ${cause ?: ""}") }
    }

    override fun debug(message: String, cause: Throwable?) {
        if (logLevel >= LogLevel.DEBUG) { println("${_tag}d: $message ${cause ?: ""}") }
    }

    override fun debug(cause: Throwable?, message: () -> String) {
        if (logLevel >= LogLevel.DEBUG) { println("${_tag}v: ${message()} ${cause ?: ""}") }
    }
}