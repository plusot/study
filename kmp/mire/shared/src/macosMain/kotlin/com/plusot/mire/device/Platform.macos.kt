package com.plusot.mire.device

import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import org.koin.dsl.module
import platform.Foundation.NSLog
import platform.Foundation.NSString
import platform.Foundation.NSUTF8StringEncoding
import platform.Foundation.stringWithCString
import platform.posix.uname
import platform.posix.utsname
import kotlin.experimental.ExperimentalNativeApi

class MacosPlatform: Platform {

    @OptIn(ExperimentalNativeApi::class)
    override val osName = kotlin.native.Platform.osFamily.name

    override val osVersion = "MacOS" //UIDevice.currentDevice.systemVersion

    @OptIn(ExperimentalForeignApi::class)
    override val deviceModel: String
        get() {
            memScoped {
                val systemInfo: utsname = alloc()
                uname(systemInfo.ptr)
                return NSString.stringWithCString(
                    systemInfo.machine,
                    encoding = NSUTF8StringEncoding
                ) ?: "---"
            }
        }

    @OptIn(ExperimentalNativeApi::class)
    override val cpuType = kotlin.native.Platform.cpuArchitecture.name

    override val screen = ScreenInfo()

    override fun logSystemInfo() {
        //SimpleLogger.info("OS Name: MacOS")
        NSLog("OS Name: MacOS")
    }

}

actual class ScreenInfo actual constructor() {
    actual val width: Int = 0
    actual val height: Int = 0
    actual val density: Int? = null
}

actual fun getPlatform(): Platform = MacosPlatform()
actual val platformModule = module {}
