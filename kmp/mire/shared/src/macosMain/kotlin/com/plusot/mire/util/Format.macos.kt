package com.plusot.mire.util

import platform.Foundation.NSNull
import platform.Foundation.NSString
import platform.Foundation.stringWithFormat

actual fun formatString(
    format: String,
    vararg args: Any?
): String {
    //return NSString.stringWithFormat(format, *arrayOf(args))
    val convertedArgs = args.map { it?.toString() as NSString? ?: NSNull() }
    return NSString.stringWithFormat(format, convertedArgs)
}