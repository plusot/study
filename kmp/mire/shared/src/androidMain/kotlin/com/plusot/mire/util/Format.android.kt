package com.plusot.mire.util

import java.util.Locale

actual fun formatString(
    format: String,
    vararg args: Any?
): String {
    return java.lang.String.format(Locale.getDefault(), format, *args)
}