package com.plusot.mire.device

import android.content.res.Resources
import android.os.Build
import com.russhwolf.settings.Settings
import com.russhwolf.settings.SharedPreferencesSettings
import org.koin.dsl.module
import kotlin.math.round
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import com.plusot.mire.MireDb
import com.plusot.mire.util.SimpleLogger

class AndroidPlatform : Platform {
    override val osName = "Android"

    @androidx.annotation.ChecksSdkIntAtLeast(extension = 0)
    override val osVersion = "${Build.VERSION.SDK_INT}"

    override val deviceModel = "${Build.MANUFACTURER} ${Build.MODEL}"

    override val cpuType = Build.SUPPORTED_ABIS.firstOrNull() ?: "---"

    override val screen = ScreenInfo()

    override fun logSystemInfo() {
        SimpleLogger.debug { "Platform $deviceInfo"}
    }
}

actual class ScreenInfo actual constructor() {
    private val metrics = Resources.getSystem().displayMetrics

    actual val width = metrics.widthPixels
    actual val height = metrics.heightPixels
    actual val density: Int? = round(metrics.density).toInt()
}

actual fun getPlatform(): Platform = AndroidPlatform()
actual val platformModule = module {
    single<Settings> {
        SharedPreferencesSettings(delegate = get())
    }
    single<SqlDriver> {
        AndroidSqliteDriver(schema = MireDb.Schema, context = get(), name = "MireDb")
    }
}
