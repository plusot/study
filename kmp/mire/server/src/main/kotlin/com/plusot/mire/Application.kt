package com.plusot.mire

import com.plusot.mire.network.SERVER_PORT
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun main() {
    embeddedServer(Netty, port = SERVER_PORT, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    routing {
        get("/") {
            call.respondText("Mire server on: ${Greeting().greet()}")
        }
        get("/greeting") {
            call.respondText("Mire server on: ${Greeting().greet()}")
        }
    }
}