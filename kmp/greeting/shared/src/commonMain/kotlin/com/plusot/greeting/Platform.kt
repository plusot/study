package com.plusot.greeting

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform