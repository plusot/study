package com.plusot.greeting

import RocketComponent
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlin.random.Random
import kotlin.time.Duration.Companion.seconds


class Greeting {
    private val platform = getPlatform()
    private val rocketComponent = RocketComponent()

    fun greet(): Flow<String> = flow {
        val firstWord = if (Random.nextBoolean()) "Hi!" else "Hello!"

        emit("${firstWord}, ${platform.name}!")
        delay(1.seconds)

        emit("Created by Me Myself and I")
        delay(1.seconds)
        emit(daysPhrase())
        emit(rocketComponent.launchPhrase())
    }
}