package com.plusot.greeting

import android.os.Build

class AndroidPlatform : Platform {
    override val name: String = "Android ${Build.VERSION.SDK_INT} ${Build.DEVICE} ${Build.MODEL}"
}

actual fun getPlatform(): Platform = AndroidPlatform()