package com.plusot.greeting

import platform.UIKit.UIDevice

class IOSPlatform: Platform {
    override val name: String = "${UIDevice.currentDevice.systemName()}  ${UIDevice.currentDevice.systemVersion} on ${UIDevice.currentDevice.model}"
}

actual fun getPlatform(): Platform = IOSPlatform()