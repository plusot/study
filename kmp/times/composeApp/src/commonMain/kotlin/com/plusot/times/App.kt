package com.plusot.times

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.datetime.*
import org.jetbrains.compose.resources.DrawableResource
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.ui.tooling.preview.Preview
import times.composeapp.generated.resources.*

data class Country(val name: String, val zone: TimeZone, val image: DrawableResource)

fun currentTimeAt(location: String, zone: TimeZone): String {
    fun LocalTime.formatted() = "$hour:$minute:$second"

    val time = Clock.System.now()
    val localTime = time.toLocalDateTime(zone).time

    return "The time in $location is ${localTime.formatted()}"
}

fun countries() = listOf(
    Country("Australia", TimeZone.of("Australia/Sydney"), Res.drawable.au),
    Country("Egypt", TimeZone.of("Africa/Cairo"), Res.drawable.eg),
    Country("France", TimeZone.of("Europe/Paris"), Res.drawable.fr),
    Country("Indonesia", TimeZone.of("Asia/Jakarta"), Res.drawable.id), 
    Country("Japan", TimeZone.of("Asia/Tokyo"), Res.drawable.jp),
    Country("Mexico", TimeZone.of("America/Mexico_City"), Res.drawable.mx),
    Country("Netherlands", TimeZone.of("Europe/Amsterdam"), Res.drawable.nl),
    )

@Composable
@Preview
fun App(countries: List<Country> = countries()) {
    MaterialTheme {
        var showContent by remember { mutableStateOf(true) }
        var showCountries by remember { mutableStateOf(false) }
//        var location by remember { mutableStateOf("Europe/Paris") }
        var timeAtLocation by remember { mutableStateOf("No location selected") }
        val greeting = remember { Greeting().greet() }
        

        Column(modifier = Modifier.padding(20.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            Text(timeAtLocation,
                 style = TextStyle(fontSize = 20.sp),
                 textAlign = TextAlign.Center,
                 modifier = Modifier.fillMaxWidth().align(Alignment.CenterHorizontally)
             )
            Row(modifier = Modifier.padding(start = 20.dp, top = 10.dp)) {
                DropdownMenu(
                    expanded = showCountries,
                    onDismissRequest = { showCountries = false }
                ) {
                    countries().forEach { (name, zone, image) ->
                        DropdownMenuItem(
                            onClick = {
                                timeAtLocation = currentTimeAt(name, zone)
                                showCountries = false
                            }
                        ) {
                            Image(
                                painterResource(image),
                                modifier = Modifier.size(50.dp).padding(end = 10.dp),
                                contentDescription = "$name flag"
                            )
                            Text(name)
                        }
                    }
                }
            }
            Button(modifier = Modifier.padding(start = 20.dp, top = 10.dp),
                   onClick = { showCountries = !showCountries }) {
                   Text("Select Location")
               }
//            TextField(value = location,
//                      modifier = Modifier.padding(top = 10.dp),
//                      onValueChange = { location = it })
//            Button(modifier = Modifier.padding(top = 10.dp),
//                   onClick = { timeAtLocation = currentTimeAt(location) ?: "Invalid location" }) {
//                Text("Show Time At Location")
//            }
            AnimatedVisibility(showContent) {
                Column(Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
                    Image(painterResource(Res.drawable.compose_multiplatform), null)
                    Text("Compose: $greeting", textAlign = TextAlign.Center)
                    Text("UUID: ${randomUUID()}", textAlign = TextAlign.Center)
                }
            }
            
        }
    }
}
