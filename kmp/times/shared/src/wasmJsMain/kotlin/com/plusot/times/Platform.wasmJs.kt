package com.plusot.times

import com.benasher44.uuid.uuid4

class WasmPlatform: Platform {
    override val name: String = "Web with Kotlin/Wasm"
}

actual fun getPlatform(): Platform = WasmPlatform()
actual fun randomUUID(): String = uuid4().toString()