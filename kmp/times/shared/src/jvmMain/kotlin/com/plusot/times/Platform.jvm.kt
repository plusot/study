package com.plusot.times

import java.util.*

class JVMPlatform: Platform {
    override val name: String = "Java ${System.getProperty("java.version")}"
}

actual fun getPlatform(): Platform = JVMPlatform()
actual fun randomUUID(): String = UUID.randomUUID().toString()